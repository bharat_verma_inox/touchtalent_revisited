package com.app.touchtalent;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.utils.TtlConst;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;

public class UploadHomeActivity extends Activity {

	private final int VIDEO_REQUEST = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_home);
		String type = getIntent().getType();
		AppLog.i("type " + type);
		MyPref.init(this);
		if (type != null) {
			String user_token = MyPref.getMyTtlToken();
			if (user_token != null && user_token.length() > 0) {

				if (type.contains("image")) {
					Intent intent = new Intent(this, UploadImageActivity.class);
					intent.putExtra("fromintent", true);
					Uri imageUri = (Uri) getIntent().getExtras().get(Intent.EXTRA_STREAM);
					intent.putExtra("imageuri", imageUri);

					// getContentResolver().get

					startActivity(intent);
					finish();
				} else if (type.contains("text")) {
					Intent intent = new Intent(this, UploadActivity.class);
					intent.putExtra("fromintent", true);
					String content = getIntent().getExtras().getString(Intent.EXTRA_TEXT);
					intent.putExtra("textcontent", content);
					// intent.putExtra("subject", content);
					intent.putExtra(TtlConst.KEY_UPLOAD_TYPE, TtlConst.UPLOAD_TYPE_TEXT);
					startActivity(intent);
					finish();
				}

			} else {
				Toast.makeText(this, "you are currently not logged in. Please login and try again", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(this, RegLoginActivity.class);
				startActivity(intent);
				finish();
			}

		}

	}

	public void onImageClick(View v) {
		startActivity(new Intent(this, UploadImageActivity.class));
	}

	public void OnVideoClick(View v) {
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType("video/*");
		startActivityForResult(intent, VIDEO_REQUEST);
	}

	public void onTextClick(View v) {
		Intent in = new Intent(this, UploadActivity.class);
		in.putExtra(TtlConst.KEY_UPLOAD_TYPE, TtlConst.UPLOAD_TYPE_TEXT);
		startActivity(in);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == VIDEO_REQUEST && resultCode == RESULT_OK) {
			Uri selectedImage = data.getData();
			Intent in = new Intent(this, UploadActivity.class);
			in.putExtra(TtlConst.KEY_UPLOAD_TYPE, TtlConst.UPLOAD_TYPE_VIDEO);
			in.setData(selectedImage);
			startActivity(in);
		}
	}

	public void onBackClick(View v) {
		this.finish();
	}

	@Override
	public void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, MyUtilities.FLURRY_KEY);
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);
	}
}
