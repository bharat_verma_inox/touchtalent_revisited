package com.app.touchtalent;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.touchtalent.http.ApiCallResponse;
import com.app.touchtalent.http.ApiSource;
import com.app.touchtalent.http.CatagoriesExe;
import com.app.touchtalent.http.LoginExe;
import com.app.touchtalent.http.OnReposnceReceived;
import com.app.touchtalent.http.RequestCreater;
import com.app.touchtalent.parser.BaseData;
import com.app.touchtalent.parser.Catagories;
import com.app.touchtalent.parser.DataManager;
import com.app.touchtalent.parser.UserInfo;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.utils.TTLUtils;
import com.app.touchtalent.utils.TtlConst;
import com.app.touchtalent.utils.TtlProgressDialog;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusClient.OnAccessRevokedListener;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.Name;
import com.google.gson.Gson;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebook.OnFriendsRequestListener;
import com.sromku.simple.fb.SimpleFacebook.OnLoginListener;
import com.sromku.simple.fb.SimpleFacebook.OnProfileRequestListener;
import com.sromku.simple.fb.entities.Profile;

public class RegLoginActivity extends FragmentActivity implements OnReposnceReceived, OnConnectionFailedListener,
		ConnectionCallbacks, OnClickListener, ApiCallResponse {

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	// BHarat verma g+ ...........
	private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
	private ProgressDialog mConnectionProgressDialog;
	private PlusClient mPlusClient;
	private ConnectionResult mConnectionResult;
	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	String SENDER_ID = TtlConst.GCM_SENDER_ID;

	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "GCM Demo";

	TextView mDisplay;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	String regid;

	private final int CODE_CATAGORY = 121;
	private final int CODE_LOGIN = 321;

	private final String TERMS = " <font color=#FFFFFF>By joining I agree to Touchtalent`s </font><font color=#00FFFF>Terms of Service</font>";

	private Button mBtnLogin;
	private Button mBtnRegister;

	private EditText mFirstNameText = null;
	private EditText mLastNameText = null;
	private EditText mEmailText = null;
	private EditText mPassowrdText = null;

	private View mLoginPanel = null;
	private View mRegisterPanel = null;
	private TextView mTermsText = null;

	private int mLoginType = 0;
	private TtlProgressDialog mProgressDialogFacebook = null;

	private SimpleFacebook mSimpleFacebook;

	// private PlusClientFragment mSignInFragment;
	private boolean isGmailClicked = false;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		context = getApplicationContext();
		MyPref.init(this);
		AppLog.i("gcm id in regActivity " + MyPref.getGcmRegistrationId());
		mBtnLogin = (Button) findViewById(R.id.act_login_tab);
		mBtnRegister = (Button) findViewById(R.id.act_login_register_tab);

		mFirstNameText = (EditText) findViewById(R.id.act_login_fname);
		mLastNameText = (EditText) findViewById(R.id.act_login_lname);
		mEmailText = (EditText) findViewById(R.id.act_login_email);
		mPassowrdText = (EditText) findViewById(R.id.act_login_password);

		mLoginPanel = findViewById(R.id.act_login_panel);
		mRegisterPanel = findViewById(R.id.act_register_panel);

		boolean isLogin = getIntent().getBooleanExtra("isLogin", true);
		manageTabView(isLogin);

		mTermsText = (TextView) findViewById(R.id.act_login_terms);
		mTermsText.setMovementMethod(LinkMovementMethod.getInstance());

		SpannableStringBuilder ssb = new SpannableStringBuilder(Html.fromHtml(TERMS));
		ssb.setSpan(new ClickableSpan() {

			@Override
			public void onClick(View widget) {

			}
		}, 36, 52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		mTermsText.setText(ssb);
		mProgressDialogFacebook = new TtlProgressDialog(this);

		mSimpleFacebook = SimpleFacebook.getInstance(this);

		// Bharat Verma g+ code
		// Bharat Verma g+ login code starts here...........
		mPlusClient = new PlusClient.Builder(this, this, this)
				.setActions("http://schemas.google.com/AddActivity", "http://schemas.google.com/BuyActivity")
				.setScopes(Scopes.PLUS_LOGIN).build();

		mConnectionProgressDialog = new ProgressDialog(this);
		mConnectionProgressDialog.setMessage("Signing in...");
		findViewById(R.id.sign_in_button).setOnClickListener(this);

		// ........................
		String u_token = MyPref.getMyTtlToken();
		if (u_token != null && u_token.length() != 0) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
		} else {
			/*
			 * Intent intent = new Intent(this, SplashActivity.class);
			 * startActivity(intent); finish();
			 */
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		// Check device for Play Services APK.

	}

	@Override
	public void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, MyUtilities.FLURRY_KEY);
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
		mPlusClient.connect();
	}

	@Override
	public void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);
		if (mPlusClient.isConnected()) {
			mPlusClient.clearDefaultAccount();

			mPlusClient.revokeAccessAndDisconnect(new OnAccessRevokedListener() {
				@Override
				public void onAccessRevoked(ConnectionResult status) {
					// mPlusClient is now disconnected and access has
					// been revoked.
					// Trigger app logic to comply with the developer
					// policies
				}
			});
			mPlusClient.disconnect();
			// ego

		}
	}

	public void onTabClicked(View v) {
		mBtnLogin.setSelected(v.getId() == R.id.act_login_tab);
		mBtnRegister.setSelected(v.getId() == R.id.act_login_register_tab);
		manageTabView(v.getId() == R.id.act_login_tab);

	}

	private void manageTabView(boolean isLogin) {
		mLoginPanel.setVisibility(isLogin ? View.VISIBLE : View.GONE);
		mRegisterPanel.setVisibility(isLogin ? View.GONE : View.VISIBLE);
		findViewById(R.id.name_layout).setVisibility(isLogin ? View.GONE : View.VISIBLE);
		if (!isLogin)
			mFirstNameText.requestFocus();
		mBtnLogin.setSelected(isLogin);
		mBtnRegister.setSelected(!isLogin);
	}

	public void onForgotPasswordClick(View v) {
		// Intent in = new Intent(RegLoginActivity.this, WebViewActivity.class);
		// in.putExtra(TtlConst.KEY_URL, TtlConst.URL_FORGOT_PSED);
		// startActivity(in);
		Toast.makeText(this, "THIS IS TO BE IMPLEMENTED", Toast.LENGTH_LONG).show();
	}

	public void onLoginBtnClicked(View v) {
		mLoginType = DataManager.TYPE_EMAIL;
		String email = mEmailText.getText().toString();
		String password = mPassowrdText.getText().toString();
		String registrationId = MyPref.getGcmRegistrationId();
		if (!TTLUtils.validateEmail(email)) {
			mEmailText.setError(getString(R.string.error_invalidemail));
		} else if (password.length() == 0) {
			mPassowrdText.setError(getString(R.string.error_invalidpassword));
		} else {
			mProgressDialogFacebook.show();
			new LoginExe(this, CODE_LOGIN).execute(RequestCreater.Login(email, password, registrationId));
		}
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */

	public void onRegisterBtnclicked(View v) {
		String fname = mFirstNameText.getText().toString();
		String lname = mLastNameText.getText().toString();
		String email = mEmailText.getText().toString();
		String password = mPassowrdText.getText().toString();
		String registrationId = MyPref.getGcmRegistrationId();
		if (!TTLUtils.validateEmail(email)) {
			mEmailText.setError(getString(R.string.error_invalidemail));
		} else if (password.length() == 0) {
			mPassowrdText.setError(getString(R.string.error_invalidpassword));
		} else if (fname.length() == 0 && lname.length() == 0) {
			mFirstNameText.setError(getString(R.string.error_invalidname));
			mLastNameText.setError(getString(R.string.error_invalidname));
		} else {
			mProgressDialogFacebook.show();
			new LoginExe(this, CODE_LOGIN).execute(RequestCreater.SignUp(email, password, fname, lname, registrationId));
		}
	}

	private void onLoginSuccess(Catagories cat) {
		Intent in = new Intent(this, MainActivity.class);
		in.putExtra(TtlConst.KEY_DATA_CATOGORY, cat);
		startActivity(in);
		finish();
	}

	@Override
	public void onRequestComplete(BaseData obj, int code) {
		if (code == CODE_LOGIN) {
			UserInfo info = (UserInfo) obj;
			AppLog.i("token my", info.user.u_token);
			DataManager.getMnger(this).saveUser(info.user, mLoginType);
			mProgressDialogFacebook.updateTextandshow(R.string.dialog_loggingin);
			new CatagoriesExe(this, info.user.u_token, CODE_CATAGORY).execute(RequestCreater.getCatagories());
		} else if (code == CODE_CATAGORY) {
			onLoginSuccess((Catagories) obj);
			mProgressDialogFacebook.dismiss();
		}
	}

	@Override
	public void onRequestFailed(String message, int code) {
		mProgressDialogFacebook.dismiss();
		TTLUtils.showToast(this, message);
	}

	/**
	 * Login with facebook works here
	 */

	public void onLoginWithFbClick(View v) {
		mLoginType = DataManager.TYPE_FB;
		mSimpleFacebook.login(mOnLoginListener);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == TtlConst.REQUEST_CODE && resultCode == TtlConst.RESULT_LOGOUT) {
			/*
			 * if (mLoginType == DataManager.TYPE_GPLUS) {
			 * mSignInFragment.signOut(); } else { this.finish(); }
			 */
		} else if (requestCode == TtlConst.REQUEST_CODE && resultCode == TtlConst.RESULT_SUCCESS) {
			setResult(TtlConst.RESULT_SUCCESS);
			this.finish();
		} else if (requestCode == REQUEST_CODE_RESOLVE_ERR && resultCode == RESULT_OK) {
			mConnectionResult = null;
			mPlusClient.connect();
		} else if (requestCode == ApiSource.MY_ACTIVITYS_AUTH_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				ApiSource.getInstance().getAccessTokenForGPlus(this, mPlusClient.getAccountName(), "", this);
			}
		}

		else {
			mSimpleFacebook.onActivityResult(this, requestCode, resultCode, data);
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	// Login listener
	private OnLoginListener mOnLoginListener = new OnLoginListener() {

		@Override
		public void onFail(String reason) {
			AppLog.i("MYAPPS", "onFail to login");
		}

		@Override
		public void onException(Throwable throwable) {
			AppLog.i("MYAPPS", "onException to login " + throwable);
		}

		@Override
		public void onThinking() {
			AppLog.i("MYAPPS", "onThinking to login");
		}

		@Override
		public void onLogin() {
			AppLog.i("MYAPPS", "onLogin to login");
			mSimpleFacebook.getProfile(onProfileRequestListener);
		}

		@Override
		public void onNotAcceptingPermissions() {
			AppLog.i("MYAPPS", "onNotAcceptingPermissions to login");
		}
	};

	/**
	 * Fetch the facebook user Profile
	 */
	// listener for profile request
	final OnProfileRequestListener onProfileRequestListener = new SimpleFacebook.OnProfileRequestListener() {

		@Override
		public void onFail(String reason) {
			mProgressDialogFacebook.dismiss();
			AppLog.i("MYAPPS", reason);
		}

		@Override
		public void onException(Throwable throwable) {
			mProgressDialogFacebook.dismiss();
			AppLog.i("MYAPPS", "Bad thing happened");
		}

		@Override
		public void onThinking() {
			mProgressDialogFacebook.show();
			// show progress bar or something to the user while fetching profile
		}

		@Override
		public void onComplete(Profile profile) {
			String email = profile.getEmail();
			if (email.equalsIgnoreCase("null")) {
				email = profile.getUsername() + "@facebook.com";
			}
			String registrationId = MyPref.getGcmRegistrationId();
			MyPref.setMyFbToken(mSimpleFacebook.getAccessToken());
			new LoginExe(RegLoginActivity.this, CODE_LOGIN).execute(RequestCreater.FBLogin(email, profile.getId(),
					mSimpleFacebook.getAccessToken(), registrationId));
			AppLog.i("fbfriends", "frinds recieving " + mSimpleFacebook.getAccessToken());
			mSimpleFacebook.getFriends(new OnFriendsRequestListener() {

				@Override
				public void onFail(String reason) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onException(Throwable throwable) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onThinking() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onComplete(List<Profile> friends) {
					// TODO Auto-generated method stub
					if (MyPref.getFriendHastMap() == null || MyPref.getFriendHastMap().length() == 0) {
						// AppLog.i("fbfriends", "frinds recieved " +
						// friends.size());
						HashMap<Integer, String> friendList = new HashMap<Integer, String>();
						int count = 0;
						for (Profile profile : friends) {
							friendList.put(count, profile.getId());
							// AppLog.i(" name " + profile.getName() + " id " +
							// profile.getId());
							count++;
						}
						Gson gson = new Gson();
						String json = gson.toJson(friendList, HashMap.class);
						// AppLog.i("fbfriends", "frinds " + json);
						MyPref.setFriendHashMap(json);
					}
				}
			});
		}
	};

	/**
	 * Google Plus Login
	 */

	public void onSignedIn(PlusClient plusClient, String token) {
		// We can now obtain the signed-in user's profile information. if
		Person currentPerson = plusClient.getCurrentPerson();

	}

	@Override
	public void onClick(View view) {
		/*
		 * isGmailClicked = true; mLoginType = DataManager.TYPE_GPLUS;
		 * mSignInFragment.signIn(TtlConst.REQUEST_CODE_PLUS_CLIENT_FRAGMENT);
		 */
		if (view.getId() == R.id.sign_in_button && !mPlusClient.isConnected()) {
			if (mConnectionResult == null) {
				mConnectionProgressDialog.show();
			} else {
				try {
					mConnectionResult.startResolutionForResult(this, REQUEST_CODE_RESOLVE_ERR);
				} catch (SendIntentException e) {
					// Try connecting again.
					mConnectionResult = null;
					mPlusClient.connect();
				}
			}

		}
	}

	/*
	 * @Override public void onSignedOut() { mSimpleFacebook.clean();
	 * this.finish(); }
	 */

	@Override
	protected void onDestroy() {
		mSimpleFacebook.clean();
		super.onDestroy();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		AppLog.i("gplus", " G+ connected ");
		mConnectionProgressDialog.dismiss();
		String accountName = mPlusClient.getAccountName();

		String scope = "oauth2:server:client_id:" + TtlConst.GOOGLE_CLIENT_ID + ":api_scope:" + Scopes.PLUS_LOGIN;
		ApiSource.getInstance().getAccessTokenForGPlus(this, mPlusClient.getAccountName(), scope, this);

	}

	@Override
	public void onDisconnected() {
		AppLog.i("gplus", " G+ Disconnected ");

	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (mConnectionProgressDialog.isShowing()) {
			AppLog.i("gplus", "onConnectionFailed entered " + result.getErrorCode());
			if (result.hasResolution()) {
				try {
					AppLog.i("gplus", "hasResolution ");
					result.startResolutionForResult(this, REQUEST_CODE_RESOLVE_ERR);
				} catch (SendIntentException e) {
					AppLog.i("gplus", "hasResolution SendIntent catch block ");
					e.printStackTrace();
					mPlusClient.connect();
				}
			}
		}

		// Save the intent so that we can start an activity when the user clicks
		// the sign-in button.
		mConnectionResult = result;
	}

	@Override
	public void onResponseRecieved(boolean status, int errorCode, Object extra) {
		AppLog.i("gplus", "onResponseRecieved " + extra);
		if (true) {
			String accessToken = null;
			if (extra instanceof String) {
				accessToken = (String) extra;
			}
			Person person = mPlusClient.getCurrentPerson();
			int gender = person.getGender();
			String genderManipulated = "";
			if (gender == 0) {
				genderManipulated = "Male";
			} else if (gender == 1) {
				genderManipulated = "Female";
			} else if (gender == 2) {
				genderManipulated = "Other";
			}
			AppLog.i("gplus", genderManipulated + "##" + MyPref.getGcmRegistrationId());

			Name name = person.getName();
			new LoginExe(this, CODE_LOGIN).execute(RequestCreater.GPlusLogin(mPlusClient.getAccountName(), person.getId(),
					accessToken, name.getGivenName(), name.getFamilyName(), person.getImage().getUrl(), genderManipulated,
					MyPref.getGcmRegistrationId()));

		}
	}

}
