package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.touchtalent.R;
import com.app.touchtalent.database.Category;

public class MenuAdapter extends BaseAdapter {

	private LayoutInflater mInflater = null;
	public ArrayList<Category> mCategory = new ArrayList<Category>();
	private boolean isFromDialog = false;
	private int mPosition = -1;

	public MenuAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
	}

	public MenuAdapter(Context context, boolean ifrom) {
		this(context);
		isFromDialog = ifrom;
	}

	public void updateSelected(int pos) {
		mPosition = pos;
		this.notifyDataSetChanged();
	}

	public int getCount() {
		return mCategory.size();
	}

	public void addAllandShow(ArrayList<Category> arrays) {
		mCategory.addAll(arrays);
		notifyDataSetChanged();
	}

	public void removeAndcollapse() {
		mCategory.clear();
		notifyDataSetChanged();
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		Category catorgory = mCategory.get(position);
		if (convertView == null
				|| (convertView != null && convertView.getTag() == null)) {
			convertView = mInflater.inflate(R.layout.cell_menu_more, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.text);
			holder.im = (ImageView) convertView.findViewById(R.id.down_arrow);
			holder.im.setVisibility(isFromDialog ? View.GONE : View.INVISIBLE);
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
					pixToDp(48, parent.getResources()));
			convertView.setLayoutParams(params);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (isFromDialog) {
			convertView
					.setBackgroundResource((mPosition == position) ? R.drawable.menu_focused
							: R.drawable.menu_background_bg);
		}
		convertView.setTag(holder);
		holder.text.setText(catorgory.getCat_name());
		return convertView;
	}

	static class ViewHolder {
		TextView text;
		ImageView im = null;
	}

	public int pixToDp(int pix, Resources res) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				pix, res.getDisplayMetrics());
	}

	@Override
	public Object getItem(int position) {
		return mCategory.get(position);
	}
}
