package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.R;
import com.app.touchtalent.http.gson.MessageGson;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.VolleySingleton;

public class ConversationAdapter extends BaseAdapter {

	private ArrayList<MessageGson> dataList;
	private Context context;

	public ConversationAdapter(Context context, ArrayList<MessageGson> dataList) {
		this.dataList = dataList;
		this.context = context;

	}

	@Override
	public int getCount() {
		return (dataList == null) ? 0 : dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static class ViewHolder {
		TextView tv_message, tv_time;
		NetworkImageView imageView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.layout_singlemessage, null);

			holder.tv_time = (TextView) convertView
					.findViewById(R.id.tv_mailbox_time);
			holder.tv_message = (TextView) convertView
					.findViewById(R.id.tv_mailbox_name);
			holder.imageView = (NetworkImageView) convertView
					.findViewById(R.id.image_mailbox_dp);
			holder.tv_time.setVisibility(View.GONE);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		MessageGson messageGson = dataList.get(position);
		holder.tv_message.setText(Html.fromHtml(messageGson.message_detail));
		// holder.tv_time.setText(relativetime);

		holder.imageView.setImageUrl(
				MyUtilities.getUserProfileImageUrl(messageGson.sender_uid, ""),
				VolleySingleton.getInstance(context).getImageLoader());
		return convertView;

	}

	public void setDataList(ArrayList<MessageGson> dataList) {
		if (dataList != null) {
			this.dataList = dataList;
			notifyDataSetChanged();
		}
	}

}
