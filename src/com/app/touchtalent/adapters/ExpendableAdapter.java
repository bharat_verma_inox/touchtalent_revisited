package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.touchtalent.Group;
import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.database.Category;

public class ExpendableAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<Group> groups;

	public ExpendableAdapter(Context context, ArrayList<Group> groups) {
		this.context = context;
		this.groups = groups;

	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return groups.get(groupPosition).children.get(childPosition);
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = ((Activity) context).getLayoutInflater().inflate(
					R.layout.layout_child, null);
		}
		final Category child = (Category) getChild(groupPosition, childPosition);
		TextView text = (TextView) convertView.findViewById(R.id.tv_child);
		text.setText(child.getCat_name());
		convertView.setTag(child);
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				((MainActivity) context).showFragement(v.getTag(),
						MainActivity.HOME_FRAGMENT, true);

			}
		});
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPostion) {
		// TODO Auto-generated method stub
		return groups.get(groupPostion).children.size();

	}

	@Override
	public Object getGroup(int arg0) {
		return groups.get(arg0);
	}

	@Override
	public int getGroupCount() {
		return (groups == null) ? 0 : groups.size();
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpended,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = ((Activity) context).getLayoutInflater().inflate(
					R.layout.layout_group, null);
		}

		Group group = (Group) getGroup(groupPosition);
		ImageView imageView = (ImageView) convertView
				.findViewById(R.id.image_group);
		if (group.children.size() > 0) {
			imageView.setVisibility(View.VISIBLE);
		} else {
			imageView.setVisibility(View.INVISIBLE);

		}
		TextView text = (TextView) convertView.findViewById(R.id.tv_group);
		text.setText(group.name);

		if (isExpended) {
			imageView.setBackgroundResource(R.drawable.ic_arrow_open);

		} else {
			imageView.setBackgroundResource(R.drawable.ic_arrow);
		}
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {

		// TODO Auto-generated method stub
		return true;
	}

	public void setGroups(ArrayList<Group> groups) {
		if (groups != null && groups.size() > 0) {
			this.groups = groups;
			notifyDataSetChanged();
		}
	}
}
