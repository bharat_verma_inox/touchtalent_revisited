package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.touchtalent.R;
import com.app.touchtalent.database.Category;

public class CategoryAdapter extends BaseAdapter {

	private ArrayList<Category> dataList;
	private Context context;

	public CategoryAdapter(ArrayList<Category> dataList, Context context) {
		this.dataList = dataList;
		this.context = context;

	}

	@Override
	public int getCount() {
		return (dataList == null) ? 0 : dataList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.layout_dropdown, null);
		}
		TextView tv = (TextView) convertView.findViewById(R.id.tv_spiner_item);
		tv.setText(dataList.get(position).getCat_name());
		tv.setTag((dataList.get(position).getCat_id()));
		return convertView;
	}

	public void setDataList(ArrayList<Category> dataList) {
		if (dataList != null && dataList.size() > 0) {
			this.dataList = dataList;
		}
	}

}
