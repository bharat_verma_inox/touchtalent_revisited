package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.database.Exibition;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;
import com.app.touchtalent.volley.VolleySingleton;

public class ExhibitionFeedAdapter extends MyEndlessListAdapter<Exibition> implements OnClickListener, DataReciever {

	public static String LOVED_IT = "4";
	public static String YOU_ROCK = "2";
	public static String AWESOME = "3";
	public static String SALUTE = "5";
	public static String HATS_OFF = "6";
	private Context context;

	public ExhibitionFeedAdapter(Context context, ArrayList<Exibition> dataList) {
		super();

		this.context = context;
		if (dataList != null) {
			this.data = dataList;
		} else {
			this.data = new ArrayList<Exibition>();
		}
	}

	private static class ViewHolder {
		public RelativeLayout rl_broadcast;
		public TextView tv_category;
		TextView tv_title, tv_auther, tv_views, tv_comments;
		ImageButton btn_broadcast, btn_follow, btn_love, btn_comment, btn_awesome, btn_hatsoff, btn_salute, btn_twitter,
				btn_pintrest, btn_in, btn_gplus, btn_facebook, btn_rock;
		ImageView btn_play;
		NetworkImageView imageView;
		TextView tv_content, toast_awesome, toast_hatsoff, toast_salute, toast_love, toast_rock;
	}

	@Override
	public void onClick(View v) {
		Exibition tag = null;
		switch (v.getId()) {
		case R.id.btn_feedrow_awesome:
			if (MyUtilities.isInternetConnected(context)) {
				((ImageButton) v).setImageResource(R.drawable.ic_awesome_active);
				postOpinion(v, AWESOME);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_love:
			if (MyUtilities.isInternetConnected(context)) {
				((ImageButton) v).setImageResource(R.drawable.ic_like_active);
				postOpinion(v, LOVED_IT);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_comment:
			if (MyUtilities.isInternetConnected(context)) {
				tag = (Exibition) v.getTag();
				((MainActivity) context).showFragement("" + tag.getPost_id(), MainActivity.COMMENT_FRAGMENT, true);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_rock:
			if (MyUtilities.isInternetConnected(context)) {
				((ImageButton) v).setImageResource(R.drawable.ic_yo_active);
				postOpinion(v, YOU_ROCK);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_hatsoff:
			if (MyUtilities.isInternetConnected(context)) {
				((ImageButton) v).setImageResource(R.drawable.ic_hat_active);
				postOpinion(v, HATS_OFF);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_salute:
			if (MyUtilities.isInternetConnected(context)) {
				((ImageButton) v).setImageResource(R.drawable.ic_salute_active);
				postOpinion(v, SALUTE);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_facebook:
			if (MyUtilities.isInternetConnected(context)) {
				tag = (Exibition) v.getTag();
				MyUtilities.shareToFacebook(
						MyUtilities.getPostUrl(tag.getPost_slug(), "" + "" + tag.getPost_id(), tag.getCategory_slug(), false),
						context);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_twitter:
			if (MyUtilities.isInternetConnected(context)) {
				tag = (Exibition) v.getTag();
				MyUtilities.shareToTwitter(tag.getPost_title(),
						MyUtilities.getPostUrl(tag.getPost_slug(), "" + "" + tag.getPost_id(), tag.getCategory_slug(), false),
						"", context);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_pinterest:
			if (MyUtilities.isInternetConnected(context)) {
				String imageUrl = "";
				tag = (Exibition) v.getTag();
				if (Integer.valueOf(tag.getPost_mediatype()) == 0) {
					imageUrl = MyUtilities.getImageUrl(tag.getPost_slug(), "" + tag.getPost_id(), false);
				} else {
					imageUrl = "static.touchtalent.com/fb_dp.png ";
				}
				MyUtilities.shareToPinterest(tag.getPost_title(),
						MyUtilities.getPostUrl(tag.getPost_slug(), "" + "" + tag.getPost_id(), tag.getCategory_slug(), false),
						imageUrl, context);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_linkedin:
			if (MyUtilities.isInternetConnected(context)) {
				tag = (Exibition) v.getTag();
				MyUtilities.shareToLinkedIn(
						MyUtilities.getPostUrl(tag.getPost_slug(), "" + "" + tag.getPost_id(), tag.getCategory_slug(), false),
						context);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_feedrow_gplus:
			if (MyUtilities.isInternetConnected(context)) {
				tag = (Exibition) v.getTag();
				MyUtilities
						.shareToGooglePlus(tag.getPost_title(),
								MyUtilities.getPostUrl(tag.getPost_slug(), "" + tag.getPost_id(), tag.getCategory_slug(), false),
								context);
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_rowfeed_broadcast:
			if (MyUtilities.isInternetConnected(context)) {
				ViewHolder holder = (ViewHolder) v.getTag();
				if (holder.rl_broadcast.getVisibility() == View.VISIBLE) {

					holder.rl_broadcast.setVisibility(View.INVISIBLE);
					((ImageButton) v).setImageResource(R.drawable.btn_broadcast_inactive);
				} else {
					((ImageButton) v).setImageResource(R.drawable.btn_close_inactive);
					holder.rl_broadcast.setVisibility(View.VISIBLE);
				}
			} else {
				MyUtilities.showNetworkToast(context);
			}
			break;
		case R.id.btn_rowfeed_follow:
			if (MyUtilities.isInternetConnected(context)) {
				ApiController.getInstance(context).markFavorite(this, "" + ((Exibition) v.getTag()).getPost_slug());
			} else {
				MyUtilities.showNetworkToast(context);
			}

			break;
		case R.id.image_main_photo:
			Exibition feedGson = (Exibition) v.getTag();
			((MainActivity) context).showFragement(feedGson, MainActivity.POSTPAGE_FRAGMENT, true);

			break;
		case R.id.tv_main_textcontent:
			Exibition feedGson1 = (Exibition) v.getTag();
			((MainActivity) context).showFragement(feedGson1, MainActivity.POSTPAGE_FRAGMENT, true);
			break;

		default:
			break;
		}

	}

	private void postOpinion(View view, String type) {
		TagHolder tag = (TagHolder) view.getTag();

		ApiController.getInstance(context).postOpinion(this, type, "1", "" + tag.getFeed().getPost_id(), "0");

		MyUtilities.showAnimation(context, tag.view_to_animate);
	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		// TODO Auto-generated method stub
	}

	@Override
	protected View doGetProgressView() {
		View v;
		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = vi.inflate(R.layout.item_feeds_loading, null);

		return v;
	}

	@Override
	protected View doGetView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.layout_row_feed, null);
			holder.btn_awesome = (ImageButton) convertView.findViewById(R.id.btn_feedrow_awesome);
			holder.btn_twitter = (ImageButton) convertView.findViewById(R.id.btn_feedrow_twitter);
			holder.btn_facebook = (ImageButton) convertView.findViewById(R.id.btn_feedrow_facebook);
			holder.btn_gplus = (ImageButton) convertView.findViewById(R.id.btn_feedrow_gplus);
			holder.btn_pintrest = (ImageButton) convertView.findViewById(R.id.btn_feedrow_pinterest);
			holder.btn_broadcast = (ImageButton) convertView.findViewById(R.id.btn_rowfeed_broadcast);
			holder.btn_follow = (ImageButton) convertView.findViewById(R.id.btn_rowfeed_follow);
			holder.btn_love = (ImageButton) convertView.findViewById(R.id.btn_feedrow_love);
			holder.btn_hatsoff = (ImageButton) convertView.findViewById(R.id.btn_feedrow_hatsoff);
			holder.btn_salute = (ImageButton) convertView.findViewById(R.id.btn_feedrow_salute);
			holder.btn_comment = (ImageButton) convertView.findViewById(R.id.btn_feedrow_comment);
			holder.btn_rock = (ImageButton) convertView.findViewById(R.id.btn_feedrow_rock);
			holder.imageView = (NetworkImageView) convertView.findViewById(R.id.image_main_photo);
			holder.tv_auther = (TextView) convertView.findViewById(R.id.tv_rowfeed_artist);
			holder.tv_comments = (TextView) convertView.findViewById(R.id.tv_rowfeed_comments);
			holder.tv_category = (TextView) convertView.findViewById(R.id.tv_rowfeed_category);
			holder.tv_views = (TextView) convertView.findViewById(R.id.tv_rowfeed_view);
			holder.tv_title = (TextView) convertView.findViewById(R.id.tv_rowfeed_title);
			convertView.setTag(holder);
			holder.btn_in = (ImageButton) convertView.findViewById(R.id.btn_feedrow_linkedin);
			holder.tv_content = (TextView) convertView.findViewById(R.id.tv_main_textcontent);
			holder.imageView.setDefaultImageResId(R.drawable.ic_image_loading);
			holder.imageView.setErrorImageResId(R.drawable.ic_image_loading);

			holder.btn_broadcast.setOnClickListener(this);
			holder.btn_follow.setOnClickListener(this);
			holder.btn_twitter.setOnClickListener(this);
			holder.btn_gplus.setOnClickListener(this);
			holder.btn_in.setOnClickListener(this);
			holder.btn_pintrest.setOnClickListener(this);
			holder.btn_facebook.setOnClickListener(this);
			holder.btn_comment.setOnClickListener(this);
			holder.btn_awesome.setOnClickListener(this);
			holder.btn_hatsoff.setOnClickListener(this);
			holder.btn_love.setOnClickListener(this);
			holder.btn_rock.setOnClickListener(this);
			holder.btn_salute.setOnClickListener(this);
			holder.imageView.setOnClickListener(this);
			holder.rl_broadcast = (RelativeLayout) convertView.findViewById(R.id.rl_feedrow_broadcastbuttons);
			holder.btn_play = (ImageView) convertView.findViewById(R.id.image_main_play);
			holder.toast_awesome = (TextView) convertView.findViewById(R.id.btn_feedrow_awesome1);
			holder.toast_love = (TextView) convertView.findViewById(R.id.btn_feedrow_love1);
			holder.toast_hatsoff = (TextView) convertView.findViewById(R.id.btn_feedrow_hatsoff1);
			holder.toast_salute = (TextView) convertView.findViewById(R.id.btn_feedrow_salute1);
			holder.toast_rock = (TextView) convertView.findViewById(R.id.btn_feedrow_rock1);
			holder.btn_broadcast.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.btn_broadcast.setImageResource(R.drawable.btn_broadcast_inactive);

		Exibition feedGson = getItem(position);
		if (Integer.valueOf(feedGson.getPost_mediatype()) == 0) {
			holder.imageView.setVisibility(View.VISIBLE);
			holder.tv_content.setVisibility(View.INVISIBLE);
			holder.imageView.setImageUrl(
					"http://full.creative.touchtalent.com/" + feedGson.getPost_slug() + "-" + feedGson.getPost_id() + ".jpg",
					VolleySingleton.getInstance(context).getImageLoader());
			holder.btn_play.setVisibility(View.INVISIBLE);
		} else if (Integer.valueOf(feedGson.getPost_mediatype()) == 1) {
			holder.imageView.setVisibility(View.VISIBLE);
			holder.tv_content.setVisibility(View.INVISIBLE);

			holder.btn_play.setVisibility(View.VISIBLE);
			String post_description = feedGson.getPost_description();
			if (post_description.contains("youtube")) {
				String url = "http://img.youtube.com/vi/" + feedGson.getPost_vlink() + "/0.jpg";

				holder.imageView.setImageUrl(url, VolleySingleton.getInstance(context).getImageLoader());
			} else {
				String url = "http://small.art.touchtalent.com/" + feedGson.getPost_slug() + "-small-" + feedGson.getPost_id()
						+ ".jpg";
				holder.imageView.setImageUrl(url, VolleySingleton.getInstance(context).getImageLoader());

			}

		} else if (Integer.valueOf(feedGson.getPost_mediatype()) == 2) {
			holder.imageView.setVisibility(View.INVISIBLE);
			holder.tv_content.setVisibility(View.VISIBLE);
			holder.tv_content.setText(Html.fromHtml(feedGson.getPost_description()));
			holder.btn_play.setVisibility(View.INVISIBLE);
			holder.tv_content.setOnClickListener(this);
			holder.tv_content.setTag(feedGson);

		}
		holder.tv_auther.setText(feedGson.getUser_fname() + " " + feedGson.getUser_lname());
		if ("true".equals(feedGson.getIs_favourite())) {
			holder.btn_follow.setImageResource(R.drawable.btn_favourite_active);
		} else {
			holder.btn_follow.setImageResource(R.drawable.btn_favourite_inactive);

		}
		holder.rl_broadcast.setVisibility(View.INVISIBLE);
		holder.tv_title.setText(feedGson.getPost_title());
		holder.tv_views.setText(feedGson.getPost_views() + "");
		holder.tv_comments.setText(feedGson.getNum_comments() + "");
		holder.tv_category.setText(feedGson.getCategory_name());

		holder.btn_awesome.setTag(new TagHolder(feedGson, holder.toast_awesome));
		holder.btn_follow.setTag(feedGson);
		holder.btn_love.setTag(new TagHolder(feedGson, holder.toast_love));
		holder.btn_rock.setTag(new TagHolder(feedGson, holder.toast_rock));
		holder.btn_comment.setTag(feedGson);
		holder.btn_salute.setTag(new TagHolder(feedGson, holder.toast_salute));
		holder.btn_hatsoff.setTag(new TagHolder(feedGson, holder.toast_hatsoff));
		holder.imageView.setTag(feedGson);
		holder.btn_in.setTag(feedGson);
		holder.btn_facebook.setTag(feedGson);
		holder.btn_twitter.setTag(feedGson);
		holder.btn_pintrest.setTag(feedGson);
		holder.btn_gplus.setTag(feedGson);

		return convertView;

	}

	public static class TagHolder {
		Exibition feedGson;
		View view_to_animate;

		TagHolder(Exibition feedGson, View view) {
			this.feedGson = feedGson;
			this.view_to_animate = view;
		}

		public Exibition getFeed() {
			return feedGson;
		}
	}

}
