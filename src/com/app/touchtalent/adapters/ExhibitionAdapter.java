package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.http.gson.Exhibitions;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.VolleySingleton;

public class ExhibitionAdapter extends BaseAdapter implements OnClickListener {
	private ArrayList<Exhibitions> dataList;
	private Context context;

	public ExhibitionAdapter(ArrayList<Exhibitions> dataList, Context context) {
		super();
		this.dataList = dataList;
		this.context = context;
	}

	@Override
	public int getCount() {
		return (dataList == null) ? 0 : dataList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static class ViewHolder {
		TextView tv_desciption, tv_time, tv_name;
		NetworkImageView imageView;
		ImageButton btn_visit;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_exhibition, null);
			holder.tv_time = (TextView) convertView
					.findViewById(R.id.tv_exhibitionrow_date);
			holder.tv_name = (TextView) convertView
					.findViewById(R.id.tv_exhibitionrow_name);
			holder.imageView = (NetworkImageView) convertView
					.findViewById(R.id.image_exhibitionrow_banner);
			holder.tv_desciption = (TextView) convertView
					.findViewById(R.id.tv_exhibitionrow_description);
			convertView.setTag(holder);
			holder.btn_visit = (ImageButton) convertView
					.findViewById(R.id.btn_exhibitionrow_visit);
			holder.imageView.setDefaultImageResId(R.drawable.ic_image_loading);
			holder.imageView.setErrorImageResId(R.drawable.ic_image_loading);
			holder.btn_visit.setOnClickListener(this);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tv_name.setText(dataList.get(position).ex_name);
		holder.tv_desciption.setText(dataList.get(position).ex_description);
		String start_date = dataList.get(position).ex_start;
		String end_date = dataList.get(position).ex_end;
		start_date = start_date.substring(0, start_date.lastIndexOf(" "));
		end_date = end_date.substring(0, end_date.lastIndexOf(" "));
		holder.btn_visit.setTag(dataList.get(position));
		holder.tv_time.setText("Exhibit : From " + start_date + " - "
				+ end_date);
		holder.imageView.setImageUrl(MyUtilities.getExhibitionImageUrl(
				dataList.get(position).ex_id, 1),
				VolleySingleton.getInstance(context).getImageLoader());
		return convertView;
	}

	public void setDataList(ArrayList<Exhibitions> dataList) {
		if (dataList != null && dataList.size() > 0) {
			this.dataList = dataList;
			notifyDataSetChanged();
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_exhibitionrow_visit) {
			((MainActivity) context).showFragement(v.getTag(),
					MainActivity.EXHIBITIONFEED_FRAGMENT, true);
		}

	}
}
