package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.R;
import com.app.touchtalent.http.gson.Viewer;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.VolleySingleton;

public class ViewerAdapter extends BaseAdapter implements OnClickListener {

	private Context context;
	private ArrayList<Viewer> dataList;

	public ViewerAdapter(Context context, ArrayList<Viewer> dataList) {

		this.context = context;
		this.dataList = dataList;
	}

	@Override
	public int getCount() {
		return (dataList == null) ? 0 : dataList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static class ViewHolder {
		TextView tv_name, tv_message, tv_time;
		NetworkImageView imageView;
		ImageButton btn_follow;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_inboxmessage, null);

			holder.tv_time = (TextView) convertView.findViewById(R.id.tv_mailbox_time);
			holder.tv_name = (TextView) convertView.findViewById(R.id.tv_mailbox_name);
			holder.imageView = (NetworkImageView) convertView.findViewById(R.id.image_mailbox_dp);
			holder.btn_follow = (ImageButton) convertView.findViewById(R.id.btn_mailbox_follow);

			holder.btn_follow.setOnClickListener(this);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Viewer messageGson = dataList.get(position);

		holder.tv_name.setText(messageGson.u_fname + " " + messageGson.u_lname);
		holder.tv_name.setTag(messageGson.viewer_uid);
		holder.tv_time.setText(messageGson.profile_place);
		if (messageGson.is_following != null && messageGson.is_following.contains("1")) {
			holder.btn_follow.setBackgroundResource(R.drawable.btn_follow_following);

		} else if (messageGson.is_following != null && messageGson.is_following.contains("0")) {
			holder.btn_follow.setBackgroundResource(R.drawable.btn_follow_follow);

		}
		holder.btn_follow.setTag(messageGson);
		holder.imageView.setImageUrl(MyUtilities.getUserProfileImageUrl(messageGson.viewer_uid, ""),
				VolleySingleton.getInstance(context).getImageLoader());
		return convertView;

	}

	public void setDataList(ArrayList<Viewer> dataList) {
		if (dataList != null) {
			this.dataList = dataList;
			notifyDataSetChanged();
		}
	}

	public String getMessageId(int position) {
		return dataList.get(position).viewer_uid;
	}

	@Override
	public void onClick(View v) {

		Viewer data = (Viewer) v.getTag();
		// String[] datavalues = data.split("#");

		if (data.is_following.contains("1")) {
			// unfollowing
			ApiController.getInstance(context).unfollowUser(null, data.viewer_uid);
			data.is_following = "0";
			v.setBackgroundResource(R.drawable.btn_follow_follow);
			// v.setTag(datavalues[0] + "#0");
		} else if (data.is_following.contains("0")) {

			ApiController.getInstance(context).followUser(null, data.viewer_uid);
			data.is_following = "1";
			v.setBackgroundResource(R.drawable.btn_follow_following);
			// v.setTag(datavalues[0] + "#1");
		}
		notifyDataSetChanged();

	}
}
