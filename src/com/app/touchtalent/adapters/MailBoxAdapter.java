package com.app.touchtalent.adapters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.R;
import com.app.touchtalent.http.gson.MessageThreadGson;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.VolleySingleton;

public class MailBoxAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<MessageThreadGson> dataList;

	public MailBoxAdapter(Context context, ArrayList<MessageThreadGson> dataList) {

		this.context = context;
		this.dataList = dataList;
	}

	@Override
	public int getCount() {
		return (dataList == null) ? 0 : dataList.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	public static class ViewHolder {
		TextView tv_name, tv_message, tv_time;
		NetworkImageView imageView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_inboxmessage, null);

			holder.tv_time = (TextView) convertView.findViewById(R.id.tv_mailbox_time);
			holder.tv_name = (TextView) convertView.findViewById(R.id.tv_mailbox_name);
			holder.imageView = (NetworkImageView) convertView.findViewById(R.id.image_mailbox_dp);
			convertView.setTag(holder);
			holder.imageView.setDefaultImageResId(R.drawable.ic_image_loading);
			holder.imageView.setErrorImageResId(R.drawable.ic_image_loading);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		MessageThreadGson messageGson = dataList.get(position);
		Date date = null;
		TimeZone zone = TimeZone.getTimeZone(Locale.getDefault().getCountry());
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(zone);
			date = df.parse(messageGson.last_msg);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long millis = date.getTime();
		String relativetime = DateUtils.getRelativeTimeSpanString(millis).toString();

		holder.tv_name.setText(messageGson.u_fname + " " + messageGson.u_lname);
		holder.tv_name.setTag(messageGson.u_id);
		holder.tv_time.setText(relativetime);

		holder.imageView.setImageUrl(MyUtilities.getUserProfileImageUrl(messageGson.u_id, ""),
				VolleySingleton.getInstance(context).getImageLoader());
		return convertView;

	}

	public void setDataList(ArrayList<MessageThreadGson> dataList) {
		if (dataList != null) {
			this.dataList = dataList;
			notifyDataSetChanged();
		}
	}

	public String getMessageId(int position) {
		return dataList.get(position).u_id;
	}

	public MessageThreadGson getMessageObject(int position) {
		return dataList.get(position);
	}
}
