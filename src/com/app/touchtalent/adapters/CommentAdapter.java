package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.R;
import com.app.touchtalent.http.gson.CommentGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.volley.VolleySingleton;

public class CommentAdapter extends BaseAdapter {

	private ArrayList<CommentGson> dataList;
	private Context context;

	public CommentAdapter(Context context, ArrayList<CommentGson> dataList) {
		this.dataList = dataList;
		this.context = context;

	}

	@Override
	public int getCount() {
		int size = (dataList == null) ? 0 : dataList.size();
		AppLog.i("data get count " + size);
		return size;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	private static class ViewHolder {
		public NetworkImageView imageView;
		public TextView tv_name, tv_comment;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.row_comment, null);
			holder = new ViewHolder();
			holder.imageView = (NetworkImageView) convertView.findViewById(R.id.image_comment_photo);
			holder.tv_comment = (TextView) convertView.findViewById(R.id.tv_comment_commenttext);
			holder.tv_name = (TextView) convertView.findViewById(R.id.tv_comment_username);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		CommentGson comment = dataList.get(position);
		holder.imageView.setImageUrl(comment.img, VolleySingleton.getInstance(context).getImageLoader());
		if (comment.comment_smiley == 1) {
			holder.tv_comment.setText(Html.fromHtml(comment.comment_text));
		} else {
			holder.tv_comment.setText(getPredefinedComment(Integer.valueOf(comment.comment_smiley)));
		}
		holder.tv_name.setText(comment.u_fname + " " + comment.u_lname + " says");
		// TODO Auto-generated method stub
		return convertView;
	}

	public String getPredefinedComment(int type) {
		String comment = "";
		switch (type) {
		case 2:
			comment = "\"you rock\"";
			break;
		case 3:
			comment = "\"awesome\"";
			break;
		case 4:
			comment = "\"loved it\"";
			break;
		case 5:
			comment = "\"salute\"";
			break;
		case 6:
			comment = "\"hats off\"";
			break;

		default:
			break;

		}
		return comment;
	}

	public void setDataList(ArrayList<CommentGson> dataList) {
		if (dataList != null) {
			AppLog.i("data present here ");
			this.dataList = dataList;
			notifyDataSetChanged();
		}
	}

	public void clearData() {
		dataList.clear();
		notifyDataSetChanged();
	}
}
