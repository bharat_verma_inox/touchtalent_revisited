package com.app.touchtalent.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.touchtalent.R;
import com.app.touchtalent.parser.Portfolio.PortFolioData;

public class PortfolioAdapter extends BaseAdapter{

	private LayoutInflater mInflater = null;
	public ArrayList<PortFolioData> mPortFolio = new ArrayList<PortFolioData>(); 
	private int mPosition = -1;

	public PortfolioAdapter(Context context,ArrayList<PortFolioData> datas ) {
		mInflater = LayoutInflater.from(context);
		mPortFolio.addAll(datas);
	}

	public void updateSelected(int pos){
		mPosition = pos;
		this.notifyDataSetChanged();
	}

	public int getCount() {
		return mPortFolio.size();
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		PortFolioData data = mPortFolio.get(position);
		if (convertView == null ||(convertView!=null && convertView.getTag() == null)) {
			convertView = mInflater.inflate(R.layout.cell_menu_more, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.text);
			holder.im = (ImageView)convertView.findViewById(R.id.down_arrow);
			holder.im.setVisibility(View.GONE);
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,pixToDp(48, parent.getResources()));
			convertView.setLayoutParams(params);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		convertView.setBackgroundResource((mPosition ==position)?R.drawable.menu_focused:R.drawable.menu_background_bg);
		convertView.setTag(holder);
		holder.text.setText(data.folder_name);
		return convertView;
	}

	static class ViewHolder {
		TextView text;
		ImageView im = null;
	}

	public int pixToDp(int pix,Resources res){
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pix, res.getDisplayMetrics());
	}


	@Override
	public Object getItem(int position) {
		return mPortFolio.get(position);
	}
}
