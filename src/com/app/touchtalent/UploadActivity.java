package com.app.touchtalent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.app.touchtalent.database.Category;
import com.app.touchtalent.filters.activity.ActivityGallery;
import com.app.touchtalent.http.OnReposnceReceived;
import com.app.touchtalent.http.PortfolioExe;
import com.app.touchtalent.http.RequestCreater;
import com.app.touchtalent.http.UploadTextExe;
import com.app.touchtalent.imageprocessor.ImageCallBack;
import com.app.touchtalent.imageprocessor.ImageProcessor;
import com.app.touchtalent.imageprocessor.ImageWriterExe;
import com.app.touchtalent.imageprocessor.OnImageSavedListner;
import com.app.touchtalent.parser.BaseData;
import com.app.touchtalent.parser.DataManager;
import com.app.touchtalent.parser.Portfolio;
import com.app.touchtalent.parser.Portfolio.PortFolioData;
import com.app.touchtalent.parser.UploadData;
import com.app.touchtalent.parser.User;
import com.app.touchtalent.service.UploadService;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.utils.TTLUtils;
import com.app.touchtalent.utils.TtlConst;
import com.app.touchtalent.utils.TtlProgressDialog;
import com.app.touchtalent.widget.CatogoryDialog;
import com.app.touchtalent.widget.CatogoryDialog.onCatagorySelector;
import com.app.touchtalent.widget.PortFolioDialog;
import com.app.touchtalent.widget.PortFolioDialog.onPortfolioSelector;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;

public class UploadActivity extends Activity implements OnFocusChangeListener, OnReposnceReceived, onCatagorySelector,
		OnClickListener, onPortfolioSelector, ImageCallBack, OnImageSavedListner {

	private EditText mUntitiledName = null;
	private EditText mCatagory = null;
	private EditText mTags = null;
	private EditText mTextentry = null;
	private EditText mPortFolio = null;
	private ProgressBar mImageProgress = null;

	private ImageButton mSaveChanges = null;
	private ImageView mContent = null;

	private Bitmap mUploadImage = null;
	private Bitmap mBackup = null;

	private int mUploadType = 0;
	private TtlProgressDialog mProgressDialog = null;
	private TtlProgressDialog mUplodProgress = null;

	private CatogoryDialog mCatogoryDialog = null;
	private PortFolioDialog mFolioDialog = null;

	private int GET_PORTFOLO = 12;

	private String mFolderId = "";
	private String mCatogoryId = "";
	private AlertDialog alertDialog = null;
	private User mUser = null;

	private int WANT_2_CELL = 0;
	private int SOLD_OUT = -2;
	private int NOT_SELLING = -1;

	private String mAbsaluteImagePath = null;
	private Uri mImageUri = null;
	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload);
		mUntitiledName = (EditText) findViewById(R.id.untitle_work);
		mCatagory = (EditText) findViewById(R.id.select_cat);
		mTags = (EditText) findViewById(R.id.tags);
		mContent = (ImageView) findViewById(R.id.upload_content);
		mTextentry = (EditText) findViewById(R.id.textEntry);
		mPortFolio = (EditText) findViewById(R.id.select_portfolio);
		mImageProgress = (ProgressBar) findViewById(R.id.upload_image_progress);
		mSaveChanges = (ImageButton) findViewById(R.id.upload_saveBtn);

		mUntitiledName.setOnFocusChangeListener(this);
		mCatagory.setOnFocusChangeListener(this);
		mCatagory.setOnClickListener(this);
		mTags.setOnFocusChangeListener(this);
		mPortFolio.setOnFocusChangeListener(this);
		mPortFolio.setOnClickListener(this);

		mUploadType = getIntent().getIntExtra(TtlConst.KEY_UPLOAD_TYPE, 0);
		mTextentry.setVisibility(mUploadType == TtlConst.UPLOAD_TYPE_TEXT ? View.VISIBLE : View.GONE);
		mContent.setVisibility(mUploadType == TtlConst.UPLOAD_TYPE_TEXT ? View.GONE : View.VISIBLE);
		mImageProgress.setVisibility(mUploadType == TtlConst.UPLOAD_TYPE_TEXT ? View.GONE : View.VISIBLE);

		View editor = findViewById(R.id.image_editor_area);
		editor.setVisibility((mUploadType == TtlConst.UPLOAD_TYPE_IMAGE || mUploadType == TtlConst.UPLOAD_TYPE_CAM) ? View.VISIBLE
				: View.GONE);

		ArrayList<com.app.touchtalent.database.Category> catagories = DataManager.getMnger(this).getCatagories();
		mCatogoryDialog = new CatogoryDialog(this, catagories, this);

		mProgressDialog = new TtlProgressDialog(this);
		mUplodProgress = new TtlProgressDialog(this);
		mUplodProgress.setButton(DialogInterface.BUTTON1, "Run in Background", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent in = new Intent(UploadActivity.this, MainActivity.class);
				in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(in);
			}
		});

		Intent intent = getIntent();
		boolean booleanExtra = intent.getBooleanExtra("fromintent", false);
		if (booleanExtra) {
			String textcontent = intent.getStringExtra("textcontent");
			if (textcontent != null) {
				mTextentry.setText(textcontent);
			}

		}

		mProgressDialog.show();
		mUser = DataManager.getMnger(this).getUser();
		new PortfolioExe(this, mUser.u_token, GET_PORTFOLO).execute(RequestCreater.getPortFolio(mUser.u_id));
		initImageView();
	}

	public void onCancelClick(View v) {
		this.finish();

	}

	private void initImageView() {
		if (mUploadType == TtlConst.UPLOAD_TYPE_IMAGE || mUploadType == TtlConst.UPLOAD_TYPE_CAM) {
			mImageUri = getIntent().getData();
			if (mImageUri != null) {
				if (mUploadType == TtlConst.UPLOAD_TYPE_IMAGE) {
//					mImageUri.get
					AppLog.i("real path " + ImageProcessor.getRealPathFromURI(mImageUri, this));
					mAbsaluteImagePath = ImageProcessor.getRealPathFromURI(mImageUri, this);
				} else if (mUploadType == TtlConst.UPLOAD_TYPE_CAM) {
					mAbsaluteImagePath = mImageUri.getPath();
				}
				if (mAbsaluteImagePath != null) {
					new ImageProcessor(this, this, mImageProgress, false).execute(mAbsaluteImagePath);
				}
			}
		} else if (mUploadType == TtlConst.UPLOAD_TYPE_VIDEO) {
			mImageUri = getIntent().getData();
			if (mImageUri != null) {
				mAbsaluteImagePath = ImageProcessor.getRealPathFromURI(mImageUri, this);
				if (mAbsaluteImagePath != null) {
					new ImageProcessor(this, this, mImageProgress, true).execute(mAbsaluteImagePath);
				}
			}
		}
	}

	public void onUploadclick(View v) {
		if (mUploadType == TtlConst.UPLOAD_TYPE_TEXT) {
			upLoadText();
		} else if (mUploadType == TtlConst.UPLOAD_TYPE_IMAGE || mUploadType == TtlConst.UPLOAD_TYPE_CAM) {
			uploadImage();
		} else if (mUploadType == TtlConst.UPLOAD_TYPE_VIDEO) {
			uploadVideo();
		}
	}

	@Override
	public void onFocusChange(View arg0, boolean arg1) {
		EditText Et = (EditText) arg0;
		if (!arg1) {
			Et.setBackgroundResource(Et.getText().toString().length() == 0 ? R.drawable.edittext_selector
					: R.drawable.edittext_gray_selected);
		}
	}

	private void upLoadText() {
		if (mTextentry.getText().toString().length() == 0) {
			mTextentry.setError(getString(R.string.upload_error));
		} else if (validateForm()) {
			mProgressDialog.show();
			new UploadTextExe(this, mUser.u_token, 0).execute(RequestCreater.UploadText(mUser.u_id, mCatogoryId, mFolderId,
					mUntitiledName.getText().toString(), mTextentry.getText().toString(), mTags.getText().toString(),
					getSellOption()));
		}
	}

	private void uploadImage() {
		if (validateForm()) {
			mUplodProgress.show();
			LocalBroadcastManager.getInstance(this).registerReceiver((receiver), new IntentFilter(TtlConst.BROADCAST));
			Intent in = new Intent(this, UploadService.class);
			in.putExtra(UploadService.KEY_CAT, mCatogoryId);
			in.putExtra(UploadService.KEY_FOLDER, mFolderId);
			in.putExtra(UploadService.KEY_TITLE, mUntitiledName.getText().toString());
			in.putExtra(UploadService.KEY_TAG, mTags.getText().toString());
			in.putExtra(UploadService.KEY_IMAGE_PATH, mAbsaluteImagePath);
			in.putExtra(UploadService.KEY_SELL_OPTION, getSellOption());
			in.putExtra(UploadService.KEY_ISIMAGE, true);
			startService(in);
		}
	}

	private void uploadVideo() {
		if (validateForm()) {
			mUplodProgress.show();
			LocalBroadcastManager.getInstance(this).registerReceiver((receiver), new IntentFilter(TtlConst.BROADCAST));
			Intent in = new Intent(this, UploadService.class);
			in.putExtra(UploadService.KEY_CAT, mCatogoryId);
			in.putExtra(UploadService.KEY_FOLDER, mFolderId);
			in.putExtra(UploadService.KEY_TITLE, mUntitiledName.getText().toString());
			in.putExtra(UploadService.KEY_TAG, mTags.getText().toString());
			in.putExtra(UploadService.KEY_IMAGE_PATH, mAbsaluteImagePath);
			in.putExtra(UploadService.KEY_SELL_OPTION, getSellOption());
			in.putExtra(UploadService.KEY_ISIMAGE, false);
			startService(in);
		}
	}

	@Override
	public void onRequestComplete(BaseData obj, int code) {
		mProgressDialog.dismiss();
		if (code == GET_PORTFOLO) {
			Portfolio data = (Portfolio) obj;
			mFolioDialog = new PortFolioDialog(this, data.portfolios, this);
		} else {
			TTLUtils.showToast(this, "Text uploaded Successfully");
			UploadData uploadData = (UploadData) obj;
			goToHome(uploadData.pid);
		}
	}

	@Override
	public void onRequestFailed(String message, int code) {
		mProgressDialog.dismiss();
		TTLUtils.showToast(this, message);
	}

	@Override
	public void onSelect(Category category) {
		mCatagory.setText(category.getCat_name());
		mCatogoryId = category.getCat_id() + "";
		mCatagory.setError(null);
		mCatagory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.downarrow, 0);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.select_cat:
			mCatogoryDialog.show();
			break;
		case R.id.select_portfolio:
			if (mFolioDialog != null)
				mFolioDialog.show();
			break;
		default:
			break;
		}
	}

	private String getSellOption() {
		int position = NOT_SELLING;
		// if (mSell_No.isChecked()) {
		position = NOT_SELLING;
		// }

		// if (mSell_Yes.isChecked()) {
		// position = WANT_2_CELL;
		// }
		//
		// if (mSell_Sold.isChecked()) {
		// position = SOLD_OUT;
		// }

		return "" + position;
	}

	@Override
	public void onSelect(PortFolioData portfolio) {
		mFolderId = portfolio.folder_id;
		mPortFolio.setText(portfolio.folder_name);
		mPortFolio.setError(null);
		mPortFolio.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.downarrow, 0);
	}

	private void showAlertDilaog() {
		if (alertDialog == null) {
			AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
			builder1.setMessage(R.string.upload_update_useraccount_msg);
			builder1.setCancelable(true);
			builder1.setPositiveButton(R.string.upload_pro_dialog_ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			alertDialog = builder1.create();
		}
		alertDialog.show();
	}

	private void goToHome(String Id) {
		AppLog.i("go to home called " + Id);
		intent = new Intent(this, com.app.touchtalent.MainActivity.class);
		intent.putExtra("objectid", Id);
		intent.putExtra("objecttype", "post");
		intent.putExtra("isnotification", true);
		// Intent in = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);

	}

	public void onCropImageClick(View v) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");
		List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
		int size = list.size();
		if (size != 0) {
			intent.setData(mImageUri);
			intent.putExtra("return-data", true);
			intent.putExtra("outputX", 400);
			intent.putExtra("outputY", 400);
			Intent i = new Intent(intent);
			ResolveInfo res = list.get(0);
			i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
			startActivityForResult(i, TtlConst.CROP_FROM_CAM);
		}
	}

	public void onRotateRightClick(View v) {
		roatteBitmap(90);
		mSaveChanges.setVisibility(View.VISIBLE);
	}

	private void roatteBitmap(float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		mUploadImage = Bitmap.createBitmap(mUploadImage, 0, 0, mUploadImage.getWidth(), mUploadImage.getHeight(), matrix, true);
		mContent.setImageBitmap(mUploadImage);
	}

	public void onRotateleftClick(View v) {
		roatteBitmap(-90);
		mSaveChanges.setVisibility(View.VISIBLE);
	}

	public void onEffectsClicks(View v) {
		Intent in = new Intent(this, ActivityGallery.class);
		in.setData(mImageUri);
		in.putExtra(TtlConst.KEY_UPLOAD_DATA, mAbsaluteImagePath);
		startActivityForResult(in, TtlConst.FILTER_IMAGE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
		if (TtlConst.CROP_FROM_CAM == requestCode) {
			if (imageReturnedIntent != null) {
				Bundle extras = imageReturnedIntent.getExtras();
				if (extras != null) {
					mBackup = mUploadImage;
					mUploadImage = extras.getParcelable("data");
					mContent.setImageBitmap(mUploadImage);
					new ImageWriterExe(this, this).execute(mUploadImage);
				}
			}
		} else if (requestCode == TtlConst.FILTER_IMAGE && resultCode == RESULT_OK) {
			if (imageReturnedIntent != null) {
				mBackup = mUploadImage;
				Uri selectedImage = imageReturnedIntent.getData();
				appplyEffects(selectedImage);
			}
		}
	}

	private boolean validateForm() {
		if (mUntitiledName.getText().toString().length() == 0) {
			mUntitiledName.setError(getString(R.string.upload_error));
			return false;
		} else if (mCatagory.getText().toString().length() == 0) {
			mCatagory.setError(getString(R.string.upload_error));
			return false;
		} else if (mPortFolio.getText().toString().length() == 0) {
			mPortFolio.setError(getString(R.string.upload_error));
			return false; 
		}
//		} else if (mTags.getText().toString().length() == 0) {
//			mTags.setError(getString(R.string.upload_error));
//			return false;
//		}
		return true;
	}

	@Override
	public void onImageCreated(Bitmap image) {
		if (image != null && mContent != null) {
			mUploadImage = image;
			mContent.setImageBitmap(mUploadImage);
			if (mBackup != null && !mBackup.isRecycled()) {
				mBackup.recycle();
				mBackup = null;
			}
		} else {
			TTLUtils.showToast(this, R.string.upload_error_save);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mUploadImage != null && !mUploadImage.isRecycled()) {
			mUploadImage.recycle();
			mUploadImage = null;
			java.lang.System.gc();
		}
	}

	public void onSaveChangeClick(View v) {
		new ImageWriterExe(this, this).execute(mUploadImage);
	}

	@Override
	public void onimageSaved(final File file) {
		if (file == null) {
			TTLUtils.showToast(this, getString(R.string.upload_error_save));
			mSaveChanges.setVisibility(View.VISIBLE);
		} else {
			mImageUri = Uri.fromFile(file);
			mAbsaluteImagePath = file.getAbsolutePath();
			mSaveChanges.setVisibility(View.GONE);
			if (mBackup != null && !mBackup.isRecycled()) {
				mBackup.recycle();
				mBackup = null;
			}
		}
	}

	private void appplyEffects(Uri uri) {
		mImageUri = uri;
		if (mImageUri != null) {
			mAbsaluteImagePath = ImageProcessor.getRealPathFromURI(mImageUri, this);
			if (mAbsaluteImagePath != null) {
				new ImageProcessor(this, this, mImageProgress, false).execute(mAbsaluteImagePath);
			}
		}
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			boolean isProgress = intent.getBooleanExtra(UploadService.KEY_PROGRESS, false);
			if (isProgress) {
				int delta = intent.getIntExtra(UploadService.KEY_PROGRESS_VALUE, 0);
				mUplodProgress.setMessage("Uploading... " + delta + "%");
			} else {
				AppLog.e("MYAPPS", "On Recieve : completed");
				mUplodProgress.dismiss();
				UploadData data = (UploadData) intent.getSerializableExtra(TtlConst.KEY_UPLOAD_DATA);
				if (data.status.equalsIgnoreCase("ERROR")) {
					TTLUtils.showToast(context, data.message);
				} else {
					TTLUtils.showToast(context, R.string.upload_success);
					goToHome(data.pid);
				}
			}
		}
	};

	@Override
	public void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, MyUtilities.FLURRY_KEY);
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);

		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
	}

	public void addCategories() {
		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList = new ArrayList<String>();
		arrayList.add("Everything");
		arrayList.add("3D Art");
		arrayList.add("Animation");
		arrayList.add("Calligraphy");
		arrayList.add("Comics");
		arrayList.add("Crafts");
		arrayList.add("Design");
		arrayList.add("Digital Art");
		arrayList.add("Fashion");
		arrayList.add("Graffiti");
		arrayList.add("Literature");
		arrayList.add("Music");
		arrayList.add("Painting");
		arrayList.add("Performing Arts");
		arrayList.add("Photography");
		arrayList.add("Poetery");
		arrayList.add("Sculpting");
		arrayList.add("Singing");
		arrayList.add("Video Editing");

	}

}
