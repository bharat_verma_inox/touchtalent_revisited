package com.app.touchtalent;

import java.util.ArrayList;

import com.app.touchtalent.database.Category;

public class Group {

	public ArrayList<Category> children;
	public String name;

	public Group(String name, ArrayList<Category> chilList) {
		this.name = name;
		this.children = chilList;

	}
}
