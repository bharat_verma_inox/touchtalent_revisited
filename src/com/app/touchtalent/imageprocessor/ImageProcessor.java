package com.app.touchtalent.imageprocessor;

import java.io.FileNotFoundException;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ProgressBar;

public class ImageProcessor extends AsyncTask<String, Void, Bitmap> {

	private Context mContext = null;
	private ImageCallBack mImageCallBack = null;
	private ProgressBar mProgressBar = null;
	boolean isVideoThumpNail = false;

	public ImageProcessor(Context context, ImageCallBack callabck, ProgressBar bar, boolean isVideoThump) {
		mContext = context;
		mImageCallBack = callabck;
		mProgressBar = bar;
		isVideoThumpNail = isVideoThump;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (mProgressBar != null)
			mProgressBar.setVisibility(View.VISIBLE);
	}

	@Override
	protected Bitmap doInBackground(String... params) {
		try {
			System.gc();
			if (!isVideoThumpNail)
				return decodeUri(params[0]);
			else {
				return ThumbnailUtils.createVideoThumbnail(params[0], MediaStore.Video.Thumbnails.MINI_KIND);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
		if (mImageCallBack != null) {
			mImageCallBack.onImageCreated(result);
		}
		if (mProgressBar != null)
			mProgressBar.setVisibility(View.INVISIBLE);
	}

	public Bitmap decodeUri(String path) throws FileNotFoundException {
		if (path != null) {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, o);
			final int REQUIRED_SIZE = dipToPixels(400);
			int scale = calculateInSampleSize(o, REQUIRED_SIZE, REQUIRED_SIZE);
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeFile(path, o2);
		}
		return null;
	}

	public int calculateInSampleSize(BitmapFactory.Options o, int reqWidth, int reqHeight) {
		// Raw height and width of image
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < reqWidth || height_tmp / 2 < reqHeight) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}
		return scale;
	}

	public static String getRealPathFromURI(Uri contentUri, Context context) {
		if (contentUri.toString().startsWith("content:/")) {
			String[] proj = { MediaStore.Images.Media.DATA };
			CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
			if (loader != null) {
				Cursor cursor = loader.loadInBackground();
				if (cursor != null) {
					int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					return cursor.getString(column_index);
				}
			}

			return null;
		} else if (contentUri.toString().startsWith("file:")) {
			return contentUri.toString().substring("file://".length() - 1);
		}
		else return null;
	}

	public int dipToPixels(float dipValue) {
		DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
	}

}
