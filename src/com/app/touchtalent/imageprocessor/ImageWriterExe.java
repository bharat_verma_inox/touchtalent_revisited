package com.app.touchtalent.imageprocessor;

import java.io.File;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.app.touchtalent.R;
import com.app.touchtalent.utils.TTLUtils;

public class ImageWriterExe extends AsyncTask<Bitmap, Void, File> {

	private OnImageSavedListner mImageCallBack = null;
	private ProgressDialog mProgressBar = null;

	public ImageWriterExe(Context context, OnImageSavedListner callabck) {
		mImageCallBack = callabck;
		mProgressBar = new ProgressDialog(context);
		mProgressBar.setMessage(context.getResources().getString(
				R.string.ttl_progress_loading));
		mProgressBar.setCancelable(false);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (mProgressBar != null)
			mProgressBar.show();
	}

	@Override
	protected File doInBackground(Bitmap... params) {
		try {
			System.gc();
			File file = TTLUtils.writeFile(params[0]);
			if (null == file)
				return null;
			System.gc();
			return file;
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(File result) {
		super.onPostExecute(result);
		if (mImageCallBack != null) {
			mImageCallBack.onimageSaved(result);
		}
		if (mProgressBar != null)
			mProgressBar.dismiss();
	}

}
