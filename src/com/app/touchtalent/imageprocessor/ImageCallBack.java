package com.app.touchtalent.imageprocessor;

import android.graphics.Bitmap;

public interface ImageCallBack {

	public void onImageCreated(Bitmap image);
}
