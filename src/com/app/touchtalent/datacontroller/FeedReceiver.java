package com.app.touchtalent.datacontroller;

import com.app.touchtalent.datacontroller.DataController.PrefValues;

public interface FeedReceiver {

	public void onFeedReceive(int callcode, boolean staus, PrefValues values);
}
