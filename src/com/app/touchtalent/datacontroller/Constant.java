package com.app.touchtalent.datacontroller;

public class Constant {

	public static int USER_ID;
	public static byte HOME_FEEDS = 000;
	public static final int FILTER_MOST_POPULAR = 500;
	public static final int FILTER_MOST_TRANDING = 300;
	public static final int FILTER_MOST_RECENT = 25;

	public static byte TYPE_OLD = 114;
	public static byte TYPE_NEW = 115;
	public static final int CODE_HOME_FEEDS = 2000;
	public static final int CODE_HOME_FEEDS_RECENT = 2025;
	public static final int CODE_HOME_FEEDS_POPULAR = 2500;
	public static final int CODE_HOME_FEEDS_TRENDING = 2300;
	public static final int CODE_CATEGORY_FEEDS_RECENT = 3025;
	public static final int CODE_CATEGORY_FEEDS_POPULAR = 3500;
	public static final int CODE_CATEGORY_FEEDS_TRENDING = 3300;
	public static final int CODE_CATEGORY_FEEDS = 3000;
	public static byte CODE_USER_FEEDS = 118;
	public static byte CODE_EXIHIBITION_FEEDS = 119;
	public static byte CODE_EXIBITION = 120;

	public static String getFilterSting(int filter) {
		if (filter == FILTER_MOST_POPULAR)
			return "mostpopular";
		else if (filter == FILTER_MOST_RECENT)
			return "recent";
		else
			return "trending";
	}

	public static int getFilterId(String filterName) {
		if (filterName.equals("mostpopular"))
			return FILTER_MOST_POPULAR;
		if (filterName.equals("recent"))
			return FILTER_MOST_RECENT;
		else
			return FILTER_MOST_TRANDING;
	}

}
