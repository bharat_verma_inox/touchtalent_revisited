package com.app.touchtalent.datacontroller;

import java.util.List;

import com.app.touchtalent.database.Feed;

public interface UserFeedListener {

	public void onUserFeedRecieved(boolean success, int callCode, List<Feed> feedList, int page);
}
