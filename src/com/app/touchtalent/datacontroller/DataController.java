package com.app.touchtalent.datacontroller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;

import com.app.touchtalent.database.Category;
import com.app.touchtalent.database.Exibition;
import com.app.touchtalent.database.Feed;
import com.app.touchtalent.http.gson.CategoryGson;
import com.app.touchtalent.http.gson.FeedDataGson;
import com.app.touchtalent.http.gson.FeedGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.DatabaseManager;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;

public class DataController implements DataReciever {

	private FeedReceiver feedReceiver;
	private CategoryReceiver categoryReceiver;
	private Context context;
	private int REQUEST_COUNT = 0;
	private ExhibitionReceiver exhibitionReceiver;
	private UserFeedListener userfeedListener;
	private int page;

	public DataController(Context context, FeedReceiver feedReceiver) {
		this.context = context;
		this.feedReceiver = feedReceiver;
	}

	public List<Feed> getFeedsByCategory(int cat_id, int filter, int skip) {

		List<Feed> categoryFeeds = null;
		categoryFeeds = DatabaseManager.getCategoryFeeds(cat_id, filter, skip);

		if (categoryFeeds == null) {
			HashMap<String, String> paramHashMap = getParamHashMap(cat_id + "", Constant.getFilterSting(filter), null, null,
					null, null, null, null, filter + "", null);
			getFeedsfromWeb(paramHashMap, Constant.CODE_CATEGORY_FEEDS + filter);

		} else if (categoryFeeds.size() == 0) {
			String displayed = DatabaseManager.getDisplayedCategory(cat_id, filter);
			HashMap<String, String> paramHashMap = null;
			if (filter == Constant.FILTER_MOST_RECENT) {
				paramHashMap = getParamHashMap(cat_id + "", Constant.getFilterSting(filter), null, null, null, displayed,
						MyPref.getRecentPostId() + "", null, filter + "", null);

			} else if (filter == Constant.FILTER_MOST_POPULAR) {
				paramHashMap = getParamHashMap(cat_id + "", Constant.getFilterSting(filter), null, null,
						MyPref.getPopularLastScore() + "", displayed, MyPref.getPopularLastPostId() + "", null, filter + "", null);
			} else if (filter == Constant.FILTER_MOST_TRANDING) {
				paramHashMap = getParamHashMap(cat_id + "", Constant.getFilterSting(filter), null, null,
						MyPref.getTrendingLastScore() + "", displayed, MyPref.getTrendingLastPostId() + "", null, filter + "",
						null);
			}
			getFeedsfromWeb(paramHashMap, Constant.CODE_CATEGORY_FEEDS + filter);

		}
		return categoryFeeds;
	}

	public List<Feed> getHomeFeeds(int filter, int skip) {
		List<Feed> homeFeeds = null;
		homeFeeds = DatabaseManager.getHomeFeeds(filter, skip);

		if (homeFeeds == null) {
			AppLog.i("homefeeds null here " + filter);
			HashMap<String, String> paramHashMap = getParamHashMap(null, Constant.getFilterSting(filter), null, null, null, null,
					null, null, filter + "", null);
			getFeedsfromWeb(paramHashMap, Constant.CODE_HOME_FEEDS + filter);

		} else if (homeFeeds.size() == 0) {
			AppLog.i("homefeeds zero " + filter);
			String displayed = DatabaseManager.getDisplayedHome(filter);
			HashMap<String, String> paramHashMap = null;
			AppLog.i("displayed here " + displayed);
			if (filter == Constant.FILTER_MOST_RECENT) {
				AppLog.i("displayed here in fltr" + displayed);
				paramHashMap = getParamHashMap(null, Constant.getFilterSting(filter), null, null, null, displayed,
						MyPref.getRecentPostId() + "", null, filter + "", null);

			} else if (filter == Constant.FILTER_MOST_POPULAR) {
				paramHashMap = getParamHashMap(null, Constant.getFilterSting(filter), null, null, MyPref.getPopularLastScore()
						+ "", displayed, MyPref.getPopularLastPostId() + "", null, filter + "", null);
			} else if (filter == Constant.FILTER_MOST_TRANDING) {
				paramHashMap = getParamHashMap(null, Constant.getFilterSting(filter), null, null, MyPref.getTrendingLastScore()
						+ "", displayed, MyPref.getTrendingLastPostId() + "", null, filter + "", null);
			}
			getFeedsfromWeb(paramHashMap, Constant.CODE_HOME_FEEDS + filter);

		}
		return homeFeeds;

	}

	public void updateFeed(FeedGson feedGson) {
		List<FeedGson> list = new ArrayList<FeedGson>();
		list.add(feedGson);
		DatabaseManager.storeFeeds(list);
	}

	public void updateMarkFavourate(long post_id) {
		DatabaseManager.updatefavourate(post_id);
	}

	public List<Feed> getUserFeeds(long userid, int pagenumber, UserFeedListener listener) {

		HashMap<String, String> paramHashMap = null;
		paramHashMap = getParamHashMap(null, null, userid + "", null, null, null, null, "allworks", null, pagenumber + "");
		this.userfeedListener = listener;
		page = pagenumber;
		getFeedsfromWeb(paramHashMap, Constant.CODE_USER_FEEDS);
		return new ArrayList<Feed>();
	}

	public List<Category> getAllcategories(CategoryReceiver categoryReceiver) {
		this.categoryReceiver = categoryReceiver;
		List<Category> listCategory = DatabaseManager.getCategories();
		if (listCategory == null || listCategory.size() == 0) {
			AppLog.i(" Category Data FROM WEB");
			ApiController.getInstance(context).getCategories(this);
		} else
			AppLog.i(" Size of category List " + listCategory.size());

		return listCategory;
	}

	public List<Exibition> getExhibition(ExhibitionReceiver exhibitionReceiver, long exibitionid) {
		this.exhibitionReceiver = exhibitionReceiver;
		List<Exibition> listexExibitions = DatabaseManager.getExibition(exibitionid);
		if (REQUEST_COUNT <= 2 && (listexExibitions == null || listexExibitions.size() <= 10)) {
			String lastScore = DatabaseManager.getLastScoreExibition(exibitionid);
			String lastPsotID = DatabaseManager.getLastPostIdEXibition(exibitionid);
			String displayed = DatabaseManager.getDisplayedExibition(exibitionid);
			HashMap<String, String> params = getParamHashMap(null, null, null, "" + exibitionid, lastScore, displayed,
					lastPsotID, "exhibition", null, null);
			getFeedsfromWeb(params, Constant.CODE_EXIHIBITION_FEEDS);
		}
		return listexExibitions;
	}

	public void getFeedsfromWeb(HashMap<String, String> params, int callCode) {

		ApiController.getInstance(context).fetchFeedData(DataController.this, params, callCode);
	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		AppLog.i("db-debug", "onDataRecived " + callCode + " " + success);

		if (success) {

			if (Constant.CODE_HOME_FEEDS < callCode && callCode <= Constant.CODE_HOME_FEEDS_POPULAR) {
				// Corresponding to home feeds
				AppLog.i("db-debug", "saving perfectly");
				checkAndMessageResponse(object, callCode, success);

			} else if (Constant.CODE_CATEGORY_FEEDS < callCode && callCode <= Constant.CODE_CATEGORY_FEEDS_POPULAR) {
				checkAndMessageResponse(object, callCode, success);

			} else if (Constant.CODE_HOME_FEEDS_RECENT == callCode) {
				AppLog.i("db-debug", "code_home_feeds");
				checkAndMessageResponse(object, callCode, success);

			} else if (callCode == MyUtilities.CODE_CATEGORIES) {
				if (object != null) {
					CategoryGson categoryGson = (CategoryGson) object;
					if (categoryGson.categories != null) {
						DatabaseManager.storeCategory(categoryGson.categories);
					}
					categoryReceiver.onCategoryreceive(success);
				}
			} else if (callCode == Constant.CODE_USER_FEEDS) {
				if (object != null) {
					FeedDataGson feedDataGson = (FeedDataGson) object;
					ArrayList<FeedGson> feedList = feedDataGson.feed;
					if (feedList != null) {
						if (feedList.size() > 0) {

							List<Feed> userFeeds = convertToFeedClass(feedList);
							userfeedListener.onUserFeedRecieved(success, callCode, userFeeds, page);

						} else {
							userfeedListener.onUserFeedRecieved(false, callCode, new ArrayList<Feed>(), page);

						}
					}
				}

			} else if (callCode == Constant.CODE_EXIHIBITION_FEEDS) {
				if (object != null) {
					FeedDataGson feedDataGson = (FeedDataGson) object;
					if (feedDataGson.feed != null) {
						if (feedDataGson.feed.size() == 0) {
						} else {
							AppLog.i("ex_id", " Exhibition Feed json   size  " + feedDataGson.feed.size() + "uid "
									+ feedDataGson.uid);
							Long eid = Long.parseLong(feedDataGson.eid);
							boolean storeExibition = DatabaseManager.storeExibition(feedDataGson.feed, eid);
							if (!storeExibition)
								REQUEST_COUNT++;
						}
						exhibitionReceiver.onExhibitionReceive(success);
					}
				}
			}
		}

	}

	private void checkAndMessageResponse(Object object, int callCode, boolean success) {
		if (object != null) {
			FeedDataGson feedDataGson = (FeedDataGson) object;
			ArrayList<FeedGson> feedList = feedDataGson.feed;
			if (feedList != null) {
				int count = feedList.size();
				AppLog.i("db-debug", "OnDataReeceived home feeds size" + count);
				if (feedList.size() > 0) {
					// FeedGson feed = feedList.get(count - 1);
					// updatePreferences(callCode, feedList.get(0).p_score,
					// feed.p_score, feed.p_id);
					FeedGson feedWithHighestScore = getFeedWithHighestScore(feedList);
					FeedGson feedWithSmallestScore = getFeedWithSmallestScore(feedList);

					PrefValues values = new PrefValues(feedWithHighestScore.p_score, feedWithSmallestScore.p_score,
							feedWithSmallestScore.p_id);
					
					boolean storeFeeds = DatabaseManager.storeFeeds(feedList);
					if (!storeFeeds) {

						updatePreferences(values, callCode);
						REQUEST_COUNT++;
					}
					feedReceiver.onFeedReceive(callCode, success, values);
				} else {
					feedReceiver.onFeedReceive(404, false, null);
				}
			} else {
				feedReceiver.onFeedReceive(callCode, success, null);
			}
		}
	}

	public HashMap<String, String> getParamHashMap(String categoryId, String filter, String userid, String eid, String lastSocre,
			String displayed, String lastPostid, String params, String filtercode, String pagenumber) {
		HashMap<String, String> parameters = new HashMap<String, String>();
		if (categoryId != null)
			parameters.put("catid", "" + categoryId);
		if (userid != null)
			parameters.put("userId", userid);
		if (lastSocre != null)
			parameters.put("last_score", "" + lastSocre);
		if (displayed != null)
			parameters.put("displayed", displayed);
		if (lastPostid != null)
			parameters.put("lpid", "" + lastPostid);
		if (filter != null)
			parameters.put("filter", filter);
		if (eid != null)
			parameters.put("eid", eid);
		if (params != null)
			parameters.put("param", params);
		if (filtercode != null)
			parameters.put("slideval", filtercode);
		if (pagenumber != null)
			parameters.put("page", pagenumber);

		AppLog.i("db-debug", "diplayed Hash Map for request " + parameters.toString());
		return parameters;
	}

	public void updatePreferences(int callcode, Float firstscore, Float lastscore, long lpid) {
		callcode = callcode % 1000; // to filterout FILTErs
		AppLog.i("db-debug", "updatePref data recieved " + firstscore + " " + lastscore + " " + lpid);
		switch (callcode) {

		case Constant.FILTER_MOST_RECENT:
			AppLog.i("db-debug", "updating RECENT pref");
			MyPref.setRecentDetails(lastscore, lpid);
			break;
		case Constant.FILTER_MOST_POPULAR:
			AppLog.i("db-debug", "updating popular pref");
			MyPref.setPolpularDetails(lastscore, lpid);
			break;

		case Constant.FILTER_MOST_TRANDING:
			AppLog.i("db-debug", "updating trending pref");
			MyPref.setTrendingDetails(firstscore, lpid, lastscore);
			break;

		}

	}

	private List<Feed> convertToFeedClass(List<FeedGson> feedList) {
		Feed feed = null;
		ArrayList<Feed> userFeedList = new ArrayList<Feed>();
		for (FeedGson post : feedList) {
			feed = new Feed(Long.valueOf(post.p_id), post.p_slug, post.p_title, Long.valueOf(post.u_id), post.p_description, "",
					post.p_mediatype, Integer.valueOf(post.cat_id), Integer.valueOf(post.num_comments),
					Integer.valueOf(post.p_views), Float.valueOf("0.0"), post.p_vlink, post.if_favourite, post.if_follow,
					post.p_timestamp, post.u_fname, post.u_lname, post.u_slug, post.isfile, post.cat_name, post.cat_slug);
			userFeedList.add(feed);
		}
		return userFeedList;
	}

	public static class PrefValues {
		public PrefValues(float firstscore, float lastscore, long p_id) {
			this.firstscore = firstscore;
			this.lastscore = lastscore;
			this.lpid = p_id;
		}

		public long lpid;
		public float firstscore, lastscore;

	}

	public void updatePreferences(PrefValues values, int callcode) {
		updatePreferences(callcode, values.firstscore, values.lastscore, values.lpid);
	}

	public FeedGson getFeedWithHighestScore(ArrayList<FeedGson> list) {
		FeedGson temp = list.get(0);
		for (FeedGson gson : list) {
			if (temp.p_score < gson.p_score) {
				temp = gson;
			}

		}
		return temp;

	}

	public FeedGson getFeedWithSmallestScore(ArrayList<FeedGson> list) {
		FeedGson temp = list.get(0);
		for (FeedGson gson : list) {
			if (temp.p_score > gson.p_score) {
				temp = gson;
			}

		}
		return temp;

	}
}
