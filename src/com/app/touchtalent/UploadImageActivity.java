package com.app.touchtalent;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;

import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.utils.TTLUtils;
import com.app.touchtalent.utils.TtlConst;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;

public class UploadImageActivity extends Activity {

	private Uri mTempImageUri = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_image_home);
		Intent intent = getIntent();
		boolean fromintent = false;
		// fromintent = intent.getExtras().getBoolean("fromintent");
		AppLog.i("from bundle " + fromintent);
		fromintent = intent.getBooleanExtra("fromintent", false);
		AppLog.i("from intent " + fromintent);
		if (fromintent) {
			Uri uri = (Uri) intent.getExtras().get("imageuri");
			AppLog.i("Uri recieved "+ uri.toString());
			uploadAvatar(uri, TtlConst.UPLOAD_TYPE_IMAGE);
		}
	}

	public void onUploadFromLib(View v) {
		getPicFromGallary();
	}

	public void onTakePhoto(View v) {
		takePhoto();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
		switch (requestCode) {
		case TtlConst.PICK_FROM_GALLARY:
			if (resultCode == RESULT_OK) {
				Uri selectedImage = imageReturnedIntent.getData();
				uploadAvatar(selectedImage, TtlConst.UPLOAD_TYPE_IMAGE);
			}
			break;
		case TtlConst.PICK_FROM_CAM:
			if (resultCode == RESULT_OK) {
				uploadAvatar(mTempImageUri, TtlConst.UPLOAD_TYPE_CAM);
			}
			break;
		}
	}// end

	private void takePhoto() {
		try {
			mTempImageUri = TTLUtils.getTempUri();
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mTempImageUri);
			intent.putExtra("return-data", true);
			startActivityForResult(intent, TtlConst.PICK_FROM_CAM);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
	}// end

	private void getPicFromGallary() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		photoPickerIntent.setType("image/*");
		photoPickerIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
		startActivityForResult(photoPickerIntent, TtlConst.PICK_FROM_GALLARY);
	}

	private void uploadAvatar(Uri selectedImage, int type) {
		Intent in = new Intent(this, UploadActivity.class);
		in.putExtra(TtlConst.KEY_UPLOAD_TYPE, type);
		in.setData(selectedImage);
		startActivity(in);
	}

	public void onBackClick(View v) {
		this.finish();
	}

	@Override
	public void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, MyUtilities.FLURRY_KEY);
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);
	}

}