package com.app.touchtalent;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.app.touchtalent.utils.AppLog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class GsonRequest<T> extends Request<T> {
	private final Gson mGson;
	private final Class<T> mClazz;
	private final Listener<T> mListener;
	private HashMap<String, String> params, headers;

	public GsonRequest(int method, String url, Class<T> clazz,
			Listener<T> listener, ErrorListener errorListener,
			HashMap<String, String> params, HashMap<String, String> headers) {
		super(method, url, errorListener);
		Log.i("volley", "Constructor called 1");
		this.mClazz = clazz;
		this.mListener = listener;
		mGson = new Gson();
		this.params = params;
		this.headers = headers;
	}

	public GsonRequest(int method, String url, Class<T> clazz,
			Listener<T> listener, ErrorListener errorListener, Gson gson) {
		super(method, url, errorListener);
		Log.i("volley", "Constructor called 2");
		this.mClazz = clazz;
		this.mListener = listener;
		mGson = gson;
	}

	@Override
	protected void deliverResponse(T response) {
		mListener.onResponse(response);
	}

	@Override
	protected Response<T> parseNetworkResponse(NetworkResponse response) {
		AppLog.i("volley", "try entered");
		try {
			String json = new String(response.data,
					HttpHeaderParser.parseCharset(response.headers));
			AppLog.i("volley", "json to print "+json);
			
			return Response.success(mGson.fromJson(json, mClazz),
					HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			AppLog.i("volley","unsupported exception");
			return Response.error(new ParseError(e));
		} catch (JsonSyntaxException e) {
			AppLog.i("volley","JsonSyntex exception");
			return Response.error(new ParseError(e));
		}
	}

	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		// TODO Auto-generated method stub
		return params;
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {

		return headers;
	}
}