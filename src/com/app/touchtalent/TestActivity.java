package com.app.touchtalent;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.http.gson.Country;
import com.app.touchtalent.http.gson.UserInfoGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;
import com.app.touchtalent.volley.VolleySingleton;

public class TestActivity extends Activity implements DataReciever {

	private LinearLayout ll_scoreholder;
	private Typeface tf_avenir;
	private Typeface tf_score;
	private LinearLayout ll_countryholder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_fragment_wow);
		tf_avenir = Typeface.createFromAsset(getAssets(), "avenir-lt-95-black.ttf");
		tf_score = Typeface.createFromAsset(getAssets(), "helveticaneue-bold.ttf");
		TextView tv_headertext = (TextView) findViewById(R.id.tv_wow_headertext);
		TextView tv_wow = (TextView) findViewById(R.id.tv_wow_headertext_wow);
		tv_headertext.setTypeface(tf_avenir);
		tv_wow.setTypeface(tf_avenir);
		ll_scoreholder = (LinearLayout) findViewById(R.id.frame_wow_scoreholder);
		ll_countryholder = (LinearLayout) findViewById(R.id.ll_wow_countryholder);
		String headertext = "you are becoming popular<br>at Tochtalent";
		tv_headertext.setText(Html.fromHtml(headertext));
		String url = "", name = "";
		int index = 0;
		// addCountryView(url, name, index);
		ApiController.getInstance(this).getUserProfileById("1", this);
	}

	private void addCountryView(String url, String name, int index) {
		View view = LayoutInflater.from(this).inflate(R.layout.country_item, null);
		NetworkImageView imageView = (NetworkImageView) view.findViewById(R.id.image_country);
		TextView text_countryName = (TextView) view.findViewById(R.id.tv_country);
		text_countryName.setText(name);
		text_countryName.setTypeface(tf_avenir, Typeface.BOLD);
		imageView.setImageUrl(url, VolleySingleton.getInstance(this).getImageLoader());
		ll_countryholder.addView(view, index);
	}

	private void addScoreDigit(String digit) {
		LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		layoutParams.leftMargin = 5;
		layoutParams.rightMargin = 5;
		layoutParams.bottomMargin = 2;
		layoutParams.topMargin = 2;
		TextView textView = new TextView(this);
		textView.setBackgroundResource(R.drawable.bg_score);
		textView.setLayoutParams(layoutParams);
		textView.setText(digit);
		textView.setTextColor(Color.WHITE);
		textView.setTextSize(44);
		textView.setTypeface(tf_score);
		textView.setGravity(Gravity.CENTER);
		ll_scoreholder.addView(textView);
	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		// TODO Auto-generated method stub
		if (success) {
			UserInfoGson gson = (UserInfoGson) object;
			manageScoreView(gson.interactions);
			manageCountryViews(gson.countries);

		}

	}

	private void manageCountryViews(ArrayList<Country> countries) {

		if (countries != null && countries.size() > 0) {
			int index = 0;
			for (Country country : countries) {
				String url = getCountryImageUrl(country.country_name);
				AppLog.i("country url " + country.country_name + " " + url);
				addCountryView(url, country.country_name, index);
				index++;
			}
		}
	}

	private void manageScoreView(String[] interactions) {
		// TODO Auto-generated method stub
		if (interactions != null && interactions.length > 0) {
			for (String digit : interactions) {
				addScoreDigit(digit);
			}
		} else {
			for (int i = 0; i < 5; i++) {
				addScoreDigit("0");
			}
		}
	}

	private String getCountryImageUrl(String name) {
		return "http://www.touchtalent.com/images/flags/Flag%20of%20" + name + ".png";
	}
}
