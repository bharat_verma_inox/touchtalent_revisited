package com.app.touchtalent.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.http.WebServiceConst;
import com.app.touchtalent.http.multipart.IStreamListener;
import com.app.touchtalent.parser.DataManager;
import com.app.touchtalent.parser.UploadData;
import com.app.touchtalent.parser.User;
import com.app.touchtalent.requestcreater.RequestCreater;
import com.app.touchtalent.utils.TtlConst;
import com.google.gson.Gson;

public class UploadService extends IntentService implements IStreamListener {

	private static final int NOTIFY_ID_UPLOAD = 1337;
	protected String mErrorResponse = null;
	protected final static int STATUS = 200;

	public static final String KEY_CAT = "catogory";
	public static final String KEY_FOLDER = "folder";
	public static final String KEY_TITLE = "titile";
	public static final String KEY_TAG = "tags";
	public static final String KEY_IMAGE_PATH = "absolutepath";
	public static final String KEY_SELL_OPTION = "selloption";
	public static final String KEY_ISIMAGE = "isImage";

	public static final String KEY_PROGRESS = "progres_changed";
	public static final String KEY_PROGRESS_VALUE = "progres_value";

	private LocalBroadcastManager broadcaster = null;;

	private NotificationManager nm;
	private User mUser = null;

	public UploadService() {
		super("Touchtalent-uploader");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		broadcaster = LocalBroadcastManager.getInstance(this);
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	}

	private static String getAbsoluteUrl() {
		return WebServiceConst.URL_UPLOAD_IMAGE;
	}

	private static String getVideoAbsoluteUrl() {
		return WebServiceConst.URL_UPLOAD_VIDEO;
	}

	@Override
	protected void onHandleIntent(Intent in) {
		mUser = DataManager.getMnger(this).getUser();
		try {
			String catogory = in.getStringExtra(UploadService.KEY_CAT);
			String folderId = in.getStringExtra(UploadService.KEY_FOLDER);
			String Titile = in.getStringExtra(UploadService.KEY_TITLE);
			String Tags = in.getStringExtra(UploadService.KEY_TAG);
			String absolutePath = in
					.getStringExtra(UploadService.KEY_IMAGE_PATH);
			String sellOptions = in
					.getStringExtra(UploadService.KEY_SELL_OPTION);
			boolean isIamge = in.getBooleanExtra(UploadService.KEY_ISIMAGE,
					true);

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(isIamge ? getAbsoluteUrl()
					: getVideoAbsoluteUrl());
			MultipartEntity entity = null;
			if (isIamge) {
				entity = RequestCreater
						.getImageParams(mUser.u_id, catogory, folderId, Titile,
								Tags, absolutePath, sellOptions, this);
			} else {
				entity = RequestCreater
						.getVideoParams(mUser.u_id, catogory, folderId, Titile,
								Tags, absolutePath, sellOptions, this);
			}

			// setting headers
			post.getParams().setBooleanParameter(
					"http.protocol.expect-continue", false);
			post.addHeader("ttl_key", "xxxx");
			post.addHeader("ttl_token", mUser.u_token);

			post.setEntity(entity);
			HttpResponse response = client.execute(post);
			int code = response.getStatusLine().getStatusCode();

			if (STATUS == code) {
				HttpEntity resEntity = response.getEntity();
				InputStream inStream = resEntity.getContent();
				UploadData mResponse = parse(inStream);
				uploadComplete(mResponse);
				if (inStream != null) {
					inStream.close();
				}
				return;
			} else {
				mErrorResponse = response.getStatusLine().getReasonPhrase();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException ioEx) {
			ioEx.printStackTrace();
			mErrorResponse = "Sorry !!! " + ioEx.getMessage();
		} catch (SecurityException secEx) {
			secEx.printStackTrace();
			mErrorResponse = "Security Fail: " + secEx.getMessage();
		} catch (IllegalArgumentException argEx) {
			argEx.printStackTrace();
			mErrorResponse = " " + argEx.getMessage();
		} catch (Exception ex) {
			ex.printStackTrace();
			mErrorResponse = "An unkown error occured " + ex.getMessage();
		}
		UploadData data = new UploadData();
		data.status = "ERROR";
		data.message = mErrorResponse;
		uploadComplete(data);
	}

	private void uploadComplete(UploadData data) {
		Intent intent = new Intent(TtlConst.BROADCAST);
		intent.putExtra(TtlConst.KEY_UPLOAD_DATA, data);
		intent.putExtra(KEY_PROGRESS, false);
		broadcaster.sendBroadcast(intent);
	}

	@Override
	public void onDestroy() {
		nm.cancel(NOTIFY_ID_UPLOAD);
		super.onDestroy();
	}

	private Notification buildNotification(String msg, int progress) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this);
		builder.setWhen(System.currentTimeMillis());
		builder.setTicker(msg);
		builder.setContentTitle(getString(R.string.app_name));
		builder.setContentText(msg);
		builder.setSmallIcon(R.drawable.logo_touchtalent);
		builder.setOngoing(true);
		builder.setProgress(100, progress, false);
		Intent notificationIntent = new Intent(this, MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);
		builder.setContentIntent(contentIntent);
		return builder.build();
	}

	@Override
	public void counterChanged(int percentUploaded) {
		final String msg = "Uploading... " + percentUploaded + "%";
		Notification notification = buildNotification(msg, percentUploaded);
		nm.notify(NOTIFY_ID_UPLOAD, notification);
		Intent intent = new Intent(TtlConst.BROADCAST);
		intent.putExtra(KEY_PROGRESS_VALUE, percentUploaded);
		intent.putExtra(KEY_PROGRESS, true);
		broadcaster.sendBroadcast(intent);
	}

	public void sendResult(String message) {
		Intent intent = new Intent(TtlConst.BROADCAST);
		if (message != null)
			intent.putExtra(KEY_PROGRESS, message);
		broadcaster.sendBroadcast(intent);
	}

	public UploadData parse(InputStream in) {
		Gson gson = new Gson();
		Reader reader = new InputStreamReader(in);
		UploadData response = gson.fromJson(reader, UploadData.class);
		return response;
	}

}
