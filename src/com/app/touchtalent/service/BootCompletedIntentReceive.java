package com.app.touchtalent.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.touchtalent.utils.MyUtilities;

public class BootCompletedIntentReceive {
	public class BootCompletedIntentReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent1) {
			if ("android.intent.action.BOOT_COMPLETED".equals(intent1.getAction())) {
				MyUtilities.startInviteService(context);
			}

		}
	}
}
