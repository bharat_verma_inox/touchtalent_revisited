package com.app.touchtalent.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.utils.TtlConst;
import com.app.touchtalent.volley.ApiController;

public class MyService extends Service {

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO do something useful
		AppLog.i("app service  " + startId + MyPref.getInvitationSent() + " " + MyPref.getMyFbToken());
		MyPref.init(getApplicationContext());
		if (!MyPref.getInvitationSent() && MyPref.getMyFbToken() != null) {
			AppLog.i("calling method in serviceca");
			if (MyUtilities.isInternetConnected(getApplicationContext())) {
				AppLog.i("calling method internet connected");
				String fbid = MyUtilities.getNextFriendToInvite(getApplicationContext());
				AppLog.i("fb id=" + fbid);
				if (fbid != null) {
					ApiController.inviteSpecificFriend(fbid, TtlConst.TT_FB_APP_ID, MyPref.getMyFbToken());

				} else {
					AppLog.i("setting invitation sent true");
					MyPref.setInvitationSent(true);
					stopSelf();

				}

			}
		}
		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO for communication return IBinder implementation
		return null;
	}
}
