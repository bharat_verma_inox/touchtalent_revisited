/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.app.touchtalent;

import java.util.List;
import java.util.Random;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.TtlConst;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public GcmIntentService() {
		super("GcmIntentService");
	}

	public static final String TAG = "GCM Demo";

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle

			String sender, message, objectid = null, objecttype = null;
			sender = extras.getString("from");
			if (TtlConst.GCM_SENDER_ID.equals(sender)) {
				message = extras.getString("message");
				objectid = extras.getString("objectid");
				objecttype = extras.getString("objecttype");
				handleNotification(objectid, objecttype, message,
						getApplicationContext());

			}
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void createNotification(String message, Context context, String url) {
		Random rnd = new Random();
		int i = rnd.nextInt();
		NotificationManager notificationManager = (NotificationManager) context
				.getApplicationContext().getSystemService(
						Context.NOTIFICATION_SERVICE);

		@SuppressWarnings("deprecation")
		Notification notification = new Notification(R.drawable.ic_launcher,
				TtlConst.APP_NAME, System.currentTimeMillis());
		MyPref.init(context);

		Intent intent = null;
		if (!isApplicationRunning()) {
			intent = new Intent(context.getApplicationContext(),
					com.app.touchtalent.MainActivity.class);
			intent.putExtra("url", TtlConst.MENU_URL + "/#" + url);
			intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			MyPref.setAppStatus(false);

		} else {
			MyPref.setNotificationUrl(TtlConst.MENU_URL + "/#" + url);
			MyPref.setAppStatus(true);

			intent = new Intent(context.getApplicationContext(),
					com.app.touchtalent.MainActivity.class);
			intent.putExtra("url", TtlConst.MENU_URL + "/#" + url);
			intent.putExtra("source", "notification");
			intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		}
		PendingIntent pIntent = PendingIntent.getActivity(
				context.getApplicationContext(), i, intent,
				android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
		notification.setLatestEventInfo(context.getApplicationContext(),
				TtlConst.APP_NAME, message, pIntent);
		notification.flags = Notification.FLAG_ONLY_ALERT_ONCE
				| Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND
				| Notification.DEFAULT_VIBRATE;

		notificationManager.notify(i, notification);
	}

	private boolean isApplicationRunning() {
		ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

		ComponentName componentInfo = taskInfo.get(0).topActivity;
		if (componentInfo.getPackageName().equalsIgnoreCase(
				TtlConst.PACKAGE_NAME)) {
			return true;
		}
		return false;

	}

	public void handleNotification(String objectid, String objecttype,
			String message, Context context) {

		Random rnd = new Random();
		int i = rnd.nextInt();
		NotificationManager notificationManager = (NotificationManager) context
				.getApplicationContext().getSystemService(
						Context.NOTIFICATION_SERVICE);

		@SuppressWarnings("deprecation")
		Notification notification = new Notification(R.drawable.ic_launcher,
				TtlConst.APP_NAME, System.currentTimeMillis());
		MyPref.init(context);

		Intent intent = null;
		
		MyPref.setAppStatus(true);

		intent = new Intent(context.getApplicationContext(),
				com.app.touchtalent.MainActivity.class);
		intent.putExtra("objectid", objectid);
		intent.putExtra("objecttype", objecttype);
		intent.putExtra("message", message);
		intent.putExtra("isnotification", true);
		intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent pIntent = PendingIntent.getActivity(
				context.getApplicationContext(), i, intent,
				android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
		notification.setLatestEventInfo(context.getApplicationContext(),
				TtlConst.APP_NAME, message, pIntent);
		notification.flags = Notification.FLAG_ONLY_ALERT_ONCE
				| Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND
				| Notification.DEFAULT_VIBRATE;

		notificationManager.notify(i, notification);
	}
}
