package com.app.touchtalent;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

@SuppressLint("NewApi") public class WatchWidget extends AppWidgetProvider
{
  public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
  {
    int i = paramArrayOfInt.length;
      int k = paramArrayOfInt[0];
      
      Intent localIntent2 = new Intent(paramContext, MainActivity.class);
      PendingIntent localPendingIntent2 = PendingIntent.getActivity(paramContext, 0, localIntent2, k);
      RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), R.layout.layout_widget);
      localRemoteViews.setOnClickPendingIntent(R.id.widget_btn, localPendingIntent2);
      paramAppWidgetManager.updateAppWidget(k, localRemoteViews);

  }
}