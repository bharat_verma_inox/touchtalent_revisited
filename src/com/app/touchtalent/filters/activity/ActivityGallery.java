/*
 * Copyright (C) 2012 CyberAgent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.app.touchtalent.filters.activity;

import jp.co.cyberagent.android.gpuimage.GPUImage.OnPictureSavedListener;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.app.touchtalent.R;
import com.app.touchtalent.filters.GPUImageFilterTools;
import com.app.touchtalent.filters.GPUImageFilterTools.FilterAdjuster;
import com.app.touchtalent.filters.GPUImageFilterTools.OnGpuImageFilterChosenListener;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.utils.TtlConst;
import com.app.touchtalent.utils.TtlProgressDialog;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;

public class ActivityGallery extends Activity implements OnSeekBarChangeListener, OnClickListener, OnPictureSavedListener {

	private GPUImageFilter mFilter;
	private FilterAdjuster mFilterAdjuster;
	private GPUImageView mGPUImageView;
	private String mFilePath = null;
	private Uri mFileUri = null;
	TtlProgressDialog mDialog = null;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);
		((SeekBar) findViewById(R.id.seekBar)).setOnSeekBarChangeListener(this);
		findViewById(R.id.button_choose_filter).setOnClickListener(this);
		findViewById(R.id.button_save).setOnClickListener(this);

		mGPUImageView = (GPUImageView) findViewById(R.id.gpuimage);
		mFileUri = getIntent().getData();
		mFilePath = getIntent().getStringExtra(TtlConst.KEY_UPLOAD_DATA);
		handleImage();
		mDialog = new TtlProgressDialog(this);
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.button_choose_filter:
			GPUImageFilterTools.showDialog(this, new OnGpuImageFilterChosenListener() {

				@Override
				public void onGpuImageFilterChosenListener(final GPUImageFilter filter) {
					switchFilterTo(filter);
					mGPUImageView.requestRender();
				}

			});
			break;
		case R.id.button_save:
			saveImage();
			break;

		default:
			break;
		}

	}

	@Override
	public void onPictureSaved(final Uri uri) {
		mDialog.dismiss();
		Intent intent = new Intent();
		intent.setData(uri);
		this.setResult(RESULT_OK, intent);
		this.finish();
		// Toast.makeText(this, "Saved: " + uri.toString(),
		// Toast.LENGTH_SHORT).show();
	}

	private void saveImage() {
		mDialog.show();
		mGPUImageView.saveToPictures("GPUImage", mFilePath, this);
	}

	private void switchFilterTo(final GPUImageFilter filter) {
		if (mFilter == null || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
			mFilter = filter;
			mGPUImageView.setFilter(mFilter);
			mFilterAdjuster = new FilterAdjuster(mFilter);
		}
	}

	@Override
	public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
		if (mFilterAdjuster != null) {
			mFilterAdjuster.adjust(progress);
		}
		mGPUImageView.requestRender();
	}

	@Override
	public void onStartTrackingTouch(final SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(final SeekBar seekBar) {
	}

	private void handleImage() {
		mGPUImageView.setImage(mFileUri);
	}

	@Override
	public void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, MyUtilities.FLURRY_KEY);
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);
	}
}
