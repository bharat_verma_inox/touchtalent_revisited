package com.app.touchtalent.volley;

import java.io.File;
import java.nio.ByteBuffer;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.ImageLoader;

public class DiskBitmapCache extends DiskBasedCache implements
		ImageLoader.ImageCache {

	private Context context;

	public DiskBitmapCache(File rootDirectory, int maxCacheSizeInBytes,
			Context context) {
		super(rootDirectory, maxCacheSizeInBytes);
		this.context = context;
	}

	public DiskBitmapCache(File cacheDir, Context context) {
		super(cacheDir);
		this.context = context;
	}

	public Bitmap getBitmap(String url) {

		final String volleyFileName = getFilenameForKey(url);

		if (context.getCacheDir().listFiles() != null)
			for (File file : context.getCacheDir().listFiles()) {
				if (file.getName().equals(volleyFileName))
					return BitmapFactory.decodeFile(file.getName());
			}

		return null;
	}

	public void putBitmap(String url, Bitmap bitmap) {
		final Entry entry = new Entry();

		ByteBuffer buffer = ByteBuffer.allocate(sizeOf(bitmap));
		bitmap.copyPixelsToBuffer(buffer);
		entry.data = buffer.array();

		put(url, entry);
	}

	// Volley creates a filename for the url with the following function, so
	// we'll use the same function
	// for translating the url back to said filename
	private String getFilenameForKey(String key) {
		int firstHalfLength = key.length() / 2;
		String localFilename = String.valueOf(key.substring(0, firstHalfLength)
				.hashCode());
		localFilename += String.valueOf(key.substring(firstHalfLength)
				.hashCode());
		return localFilename;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	protected int sizeOf(Bitmap data) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			return data.getRowBytes() * data.getHeight();
		} else {
			return data.getByteCount();
		}
	}

}