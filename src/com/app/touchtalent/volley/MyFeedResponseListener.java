package com.app.touchtalent.volley;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.touchtalent.utils.AppLog;

public class MyFeedResponseListener<T> implements Response.Listener<T>,
		Response.ErrorListener {

	private DataReciever listener;
	int callCode;

	public MyFeedResponseListener(DataReciever receiver, int callCode) {
		// TODO Auto-generated constructor stub
		this.listener = receiver;
		this.callCode = callCode;

	}

	@Override
	public void onResponse(T response) {
		AppLog.i("onResponse " + response);
		if (listener != null) {
			listener.onDataReceived(response, callCode, true);
		}

	}

	@Override
	public void onErrorResponse(VolleyError error) {
		// TODO Auto-generated method stub
		AppLog.i("Error Volley " + callCode + "# " + error.toString());
		if (listener != null) {
			listener.onDataReceived(error, callCode, false);
		}

	}
}
