package com.app.touchtalent.volley;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.app.touchtalent.http.gson.ArtInfoGson;
import com.app.touchtalent.http.gson.CategoryGson;
import com.app.touchtalent.http.gson.ExhibitionGson;
import com.app.touchtalent.http.gson.FeedDataGson;
import com.app.touchtalent.http.gson.MessageFetchGson;
import com.app.touchtalent.http.gson.MessageThreadFetchGson;
import com.app.touchtalent.http.gson.SearchGson;
import com.app.touchtalent.http.gson.UserInfoGson;
import com.app.touchtalent.http.gson.ViewerGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;

public class ApiController {
	private static String BASE_URL = "http://www.touchtalent.com/api/v1";
	private static String FEED_URL = "/user/getfeed";
	private static String POST_OPINION = "/art/add_comment";
	private static String FAVORITE_POST = "/art/addto_favourite";
	private static String COMPLETE_ARTINFO = "/post/getinfo";
	private static String COMPLETE_USERINFO = "/user/getprofile";
	private static String MAILBOX = "/user/mailbox";
	private static String CONVERSATION = "/user/message";
	private static String MESSAGE_SEND = "/user/sendmessage";
	private static String GET_EXHIBITIONS = "/user/getexhibitions";
	private static String GET_CATEGORIES = "/art/getcategories";
	private static String GET_VIEWERS = "/user/profileviewers";
	private static String LOGOUT = "/user/logout";
	private static String FOLLOW_USER = "/user/follow";
	private static String UNFOLLOW_USER = "/user/unfollow";
	private static String SEARCH_USER = "/user/search";
	private Context context;
	private static ApiController instance;
	private static HashMap<String, String> headers = new HashMap<String, String>();
	static {
		headers.put("ttl_key", "xxxx");
	}

	private ApiController(Context context) {
		this.context = context;

	}

	public static ApiController getInstance(Context context) {
		if (instance == null) {
			instance = new ApiController(context);
		}
		return instance;

	}

	public void fetchFeedData(DataReciever listener, HashMap<String, String> params) {
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		MyFeedResponseListener<FeedDataGson> myListener = new MyFeedResponseListener<FeedDataGson>(listener,
				MyUtilities.CODE_FEED_FETCH);
		headers.put("ttl_token", MyPref.getMyTtlToken());

		GsonRequest<FeedDataGson> myReq = new GsonRequest<FeedDataGson>(Method.GET, BASE_URL + FEED_URL, FeedDataGson.class,
				myListener, myListener, params, headers);
		queue.add(myReq);

	}

	public void postOpinion(DataReciever listener, final String opinion, final String u_id, final String p_id,
			final String comment_text) {
		AppLog.i(" type");
		if (MyUtilities.isInternetConnected(context)) {
			headers.put("ttl_token", MyPref.getMyTtlToken());
			MyFeedResponseListener<String> myListener = new MyFeedResponseListener<String>(listener,
					MyUtilities.CODE_POST_COMMENT);
			StringRequest request = new StringRequest(Method.POST, BASE_URL + POST_OPINION, myListener, myListener) {
				@Override
				protected Map<String, String> getParams() throws AuthFailureError {
					HashMap<String, String> params = new HashMap<String, String>();
					// params.put("u_id", u_id);
					params.put("pid", p_id);
					params.put("opinion", opinion);
					params.put("comment_text ", comment_text);
					return params;
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					return headers;
				}
			};
			VolleySingleton.getInstance(context).getRequestQueue().add(request);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}
	}

	public void markFavorite(DataReciever listener, final String p_id) {
		if (MyUtilities.isInternetConnected(context)) {
			MyFeedResponseListener<String> myListener = new MyFeedResponseListener<String>(listener,
					MyUtilities.CODE_MARK_FAVORITE);
			StringRequest request = new StringRequest(Method.POST, BASE_URL + FAVORITE_POST, myListener, myListener) {

				@Override
				protected Map<String, String> getParams() throws AuthFailureError {
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("pid", p_id);
					return params;
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					// TODO Auto-generated method stub
					headers.put("ttl_token", MyPref.getMyTtlToken());
					return headers;
				}
			};
			VolleySingleton.getInstance(context).getRequestQueue().add(request);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}
	}

	public void getCompleteArtInfo(DataReciever listener, String p_id) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("p_id", p_id);
		if (MyUtilities.isInternetConnected(context)) {
			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<ArtInfoGson> myListener = new MyFeedResponseListener<ArtInfoGson>(listener,
					MyUtilities.CODE_ARTWORK);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<ArtInfoGson> myReq = new GsonRequest<ArtInfoGson>(Method.POST, BASE_URL + COMPLETE_ARTINFO,
					ArtInfoGson.class, myListener, myListener, params, headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}
	}

	public void getUserProfileById(String u_id, DataReciever listener) {
		if (MyUtilities.isInternetConnected(context)) {
			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<UserInfoGson> myListener = new MyFeedResponseListener<UserInfoGson>(listener,
					MyUtilities.CODE_USERINFO);
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("user_id", u_id);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<UserInfoGson> myReq = new GsonRequest<UserInfoGson>(Method.POST, BASE_URL + COMPLETE_USERINFO,
					UserInfoGson.class, myListener, myListener, params, headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}

	}

	public void getUserMailbox(DataReciever listener) {
		if (MyUtilities.isInternetConnected(context)) {
			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<MessageThreadFetchGson> myListener = new MyFeedResponseListener<MessageThreadFetchGson>(
					listener, MyUtilities.CODE_MAILBOX);
			HashMap<String, String> params = new HashMap<String, String>();
			// params.put("user_id", u_id);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<MessageThreadFetchGson> myReq = new GsonRequest<MessageThreadFetchGson>(Method.POST, BASE_URL + MAILBOX,
					MessageThreadFetchGson.class, myListener, myListener, params, headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}

	}

	public void getUserConversationById(String sender_id, DataReciever listener) {
		if (MyUtilities.isInternetConnected(context)) {
			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<MessageFetchGson> myListener = new MyFeedResponseListener<MessageFetchGson>(listener,
					MyUtilities.CODE_CONVERSTION);
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("sender", sender_id);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<MessageFetchGson> myReq = new GsonRequest<MessageFetchGson>(Method.POST, BASE_URL + CONVERSATION,
					MessageFetchGson.class, myListener, myListener, params, headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);

		}

	}

	public void sendMessage(final String reciever_id, final String message, DataReciever listener) {
		if (MyUtilities.isInternetConnected(context)) {
			headers.put("ttl_token", MyPref.getMyTtlToken());
			MyFeedResponseListener<String> myListener = new MyFeedResponseListener<String>(listener, MyUtilities.CODE_SENDMESSAGE);
			StringRequest request = new StringRequest(Method.POST, BASE_URL + MESSAGE_SEND, myListener, myListener) {
				@Override
				protected Map<String, String> getParams() throws AuthFailureError {
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("receiver", reciever_id);
					params.put("message", message);
					return params;
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					return headers;
				}
			};
			VolleySingleton.getInstance(context).getRequestQueue().add(request);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}

	}

	public void fetchFeedData(DataReciever listener, HashMap<String, String> params, int callcode) {
		if (MyUtilities.isInternetConnected(context)) {
			AppLog.i("logout", MyPref.getMyTtlToken() + " " + MyPref.getMyUserId());
			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<FeedDataGson> myListener = new MyFeedResponseListener<FeedDataGson>(listener, callcode);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<FeedDataGson> myReq = new GsonRequest<FeedDataGson>(Method.POST, BASE_URL + FEED_URL, FeedDataGson.class,
					myListener, myListener, params, headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}

	}

	public void getExhibitionList(DataReciever listener) {
		if (MyUtilities.isInternetConnected(context)) {
			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<ExhibitionGson> myListener = new MyFeedResponseListener<ExhibitionGson>(listener,
					MyUtilities.CODE_EXHIBITION);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<ExhibitionGson> myReq = new GsonRequest<ExhibitionGson>(Method.GET, BASE_URL + GET_EXHIBITIONS,
					ExhibitionGson.class, myListener, myListener, new HashMap<String, String>(), headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}

	}

	public void getCategories(DataReciever listener) {
		if (MyUtilities.isInternetConnected(context)) {
			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<CategoryGson> myListener = new MyFeedResponseListener<CategoryGson>(listener,
					MyUtilities.CODE_CATEGORIES);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<CategoryGson> myReq = new GsonRequest<CategoryGson>(Method.POST, BASE_URL + GET_CATEGORIES,
					CategoryGson.class, myListener, myListener, new HashMap<String, String>(), headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}

	}

	public void getViewerDetails(DataReciever listener) {
		if (MyUtilities.isInternetConnected(context)) {
			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<ViewerGson> myListener = new MyFeedResponseListener<ViewerGson>(listener,
					MyUtilities.CODE_VIEWERS);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<ViewerGson> myReq = new GsonRequest<ViewerGson>(Method.POST, BASE_URL + GET_VIEWERS, ViewerGson.class,
					myListener, myListener, new HashMap<String, String>(), headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}

	}

	public void logoutUser(DataReciever listener) {
		if (MyUtilities.isInternetConnected(context)) {
			AppLog.i("logout", MyPref.getMyTtlToken() + " " + MyPref.getMyUserId());
			headers.put("ttl_token", MyPref.getMyTtlToken());
			MyFeedResponseListener<String> myListener = new MyFeedResponseListener<String>(listener, MyUtilities.CODE_LOGOUT);
			StringRequest request = new StringRequest(Method.POST, BASE_URL + LOGOUT, myListener, myListener) {
				@Override
				protected Map<String, String> getParams() throws AuthFailureError {
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("u_id", MyPref.getMyUserId() + "");
					return params;
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					return headers;
				}
			};
			VolleySingleton.getInstance(context).getRequestQueue().add(request);

		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}
	}

	public void followUser(DataReciever listener, final String u_id) {
		if (MyUtilities.isInternetConnected(context)) {

			MyFeedResponseListener<String> myListener = new MyFeedResponseListener<String>(listener, MyUtilities.CODE_FOLLOW);
			StringRequest request = new StringRequest(Method.POST, BASE_URL + FOLLOW_USER, myListener, myListener) {

				@Override
				protected Map<String, String> getParams() throws AuthFailureError {
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("following", u_id);
					return params;
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					headers.put("ttl_token", MyPref.getMyTtlToken());
					return headers;
				}
			};
			VolleySingleton.getInstance(context).getRequestQueue().add(request);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}
	}

	public void unfollowUser(DataReciever listener, final String u_id) {

		if (MyUtilities.isInternetConnected(context)) {
			MyFeedResponseListener<String> myListener = new MyFeedResponseListener<String>(listener, MyUtilities.CODE_UNFOLLOW);
			StringRequest request = new StringRequest(Method.POST, BASE_URL + UNFOLLOW_USER, myListener, myListener) {

				@Override
				protected Map<String, String> getParams() throws AuthFailureError {
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("unfollowing", u_id);
					return params;
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					headers.put("ttl_token", MyPref.getMyTtlToken());
					return headers;
				}
			};
			VolleySingleton.getInstance(context).getRequestQueue().add(request);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}
	}

	public void searchUser(DataReciever listener, String name) {
		if (MyUtilities.isInternetConnected(context)) {

			RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
			MyFeedResponseListener<SearchGson> myListener = new MyFeedResponseListener<SearchGson>(listener,
					MyUtilities.CODE_SEARCH);
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("name", name);
			headers.put("ttl_token", MyPref.getMyTtlToken());
			GsonRequest<SearchGson> myReq = new GsonRequest<SearchGson>(Method.POST, BASE_URL + SEARCH_USER, SearchGson.class,
					myListener, myListener, params, headers);
			queue.add(myReq);
		} else {
			MyUtilities.showNetworkToast(context);
			listener.onDataReceived("Connection Error", 404, false);
		}

	}

	public static void inviteSpecificFriend(final String friendId, final String appId, final String accessToken) {
		new Thread(new Runnable() {
			public void run() {
				// AppLog.i("sending to " + friendId);
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost("https://m.facebook.com/dialog/app_requests/submit");

				JSONObject data = new JSONObject();
				try {
					data.put("badge_of_awesomeness", "1");
					data.put("social_karma", "1");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}

				JSONArray egoLoggingSignatures = new JSONArray();

				JSONObject m_tokenizer_items = new JSONObject();
				try {
					m_tokenizer_items.put(friendId, 1);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}

				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
					nameValuePairs.add(new BasicNameValuePair("lsd", "AVoJajn-"));
					nameValuePairs.add(new BasicNameValuePair("app_id", appId));
					nameValuePairs.add(new BasicNameValuePair("redirect_uri", "fbconnect://success"));
					nameValuePairs.add(new BasicNameValuePair("type", "user_agent"));
					nameValuePairs.add(new BasicNameValuePair("display", "touch"));
					nameValuePairs.add(new BasicNameValuePair("access_token", accessToken));
					nameValuePairs.add(new BasicNameValuePair("from_post", "1"));
					nameValuePairs
							.add(new BasicNameValuePair("message",
									"Hi, I am sharing my creativity on Touchtalent. This is the most popular app to discover amazing Art & Artists."));
					nameValuePairs.add(new BasicNameValuePair("data", data.toString()));
					nameValuePairs.add(new BasicNameValuePair("frictionless", "1"));
					nameValuePairs.add(new BasicNameValuePair("egoLoggingSignatures", egoLoggingSignatures.toString()));
					nameValuePairs.add(new BasicNameValuePair("m_tokenizer_items", m_tokenizer_items.toString()));
					nameValuePairs.add(new BasicNameValuePair("__CONFIRM__", "Send"));

					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					// Execute HTTP Post Request
					HttpResponse response = httpclient.execute(httppost);
					response.getStatusLine();
					HeaderIterator headerIterator = response.headerIterator();
					while (headerIterator.hasNext()) {
						Header nextHeader = headerIterator.nextHeader();
						AppLog.i("fb header " + nextHeader.getName() + " value " + nextHeader.getValue());
					}

				} catch (ClientProtocolException e) {
					e.printStackTrace();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
