package com.app.touchtalent.fragments;

public interface FragmentVisibilityListener {

	public void onFragmentVisible(Object data);

}
