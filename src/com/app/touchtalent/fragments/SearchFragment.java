package com.app.touchtalent.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.SearchAdapter;
import com.app.touchtalent.http.gson.PersonGson;
import com.app.touchtalent.http.gson.SearchGson;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;

public class SearchFragment extends Fragment implements OnItemClickListener,
		DataReciever {

	private ListView listView;
	private SearchAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.layout_fragment_inbox, null);
		listView = (ListView) view.findViewById(R.id.listview_messages);
		view.findViewById(R.id.btn_inbox_menu).setOnClickListener(
				(MainActivity) getActivity());
		((TextView) view.findViewById(R.id.tv_mailbox_headertext))
				.setText("Search");
		listView.setOnItemClickListener(this);
		adapter = new SearchAdapter(getActivity(), new ArrayList<PersonGson>());
		listView.setAdapter(adapter);
		Bundle arguments = getArguments();
		String name = null;
		if (arguments != null) {
			name = arguments.getString("data");
			if (name != null) {
				ApiController.getInstance(getActivity()).searchUser(this, name);
			}

		}
		return view;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {

		String uid = adapter.getUserId(position);
		((MainActivity) getActivity()).showFragement(uid,
				MainActivity.PROFILE_FRAGMENT, true);

	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		if (success) {
			if (callCode == MyUtilities.CODE_SEARCH) {

				SearchGson user = (SearchGson) object;
				ArrayList<PersonGson> persons = user.users;
				adapter.setDataList(persons);

			}
		}

	}

	public void reloadView(String name) {

		ApiController.getInstance(getActivity()).searchUser(this, name);
	}
}
