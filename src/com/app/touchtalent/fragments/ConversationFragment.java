package com.app.touchtalent.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.ConversationAdapter;
import com.app.touchtalent.http.gson.MessageFetchGson;
import com.app.touchtalent.http.gson.MessageThreadGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;
import com.google.gson.Gson;

public class ConversationFragment extends Fragment implements OnClickListener, DataReciever {

	private ListView listView;
	private EditText editText;
	private ImageButton btn_send;
	private ConversationAdapter adapter;
	private String sender_id;
	private boolean isFragmentVisible = false;
	private TextView tv_titlename;
	public static boolean isHireMessage = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_conversation, null);
		listView = (ListView) view.findViewById(R.id.listview_conversation);
		editText = (EditText) view.findViewById(R.id.et_conversation_message);
		btn_send = (ImageButton) view.findViewById(R.id.btn_conversation_send);
		tv_titlename = (TextView) view.findViewById(R.id.tv_conversation_title);
		view.findViewById(R.id.btn_conversation_menu).setOnClickListener((MainActivity) getActivity());
		btn_send.setOnClickListener(this);
		Bundle bundle = getArguments();
		if (bundle != null) {
			String exjson = (String) bundle.getString("data");
			if (exjson != null) {
				Gson gson = new Gson();
				AppLog.i("exjson found to be " + exjson);
				MessageThreadGson messageThread = (MessageThreadGson) gson.fromJson(exjson, MessageThreadGson.class);
				if (messageThread != null) {
					sender_id = messageThread.u_id;
					if (messageThread.u_fname != null) {
						tv_titlename.setText(messageThread.u_fname + " " + messageThread.u_lname);
						tv_titlename.setOnClickListener(this);
					}
				}
			}
		}
		

		ApiController.getInstance(getActivity()).getUserConversationById(sender_id, this);
		adapter = new ConversationAdapter(getActivity(), null);
		listView.setAdapter(adapter);
		AppLog.i("profile", "on Convert View" + isHireMessage);
		isFragmentVisible = true;
		return view;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_conversation_send) {
			ApiController.getInstance(getActivity()).sendMessage(sender_id, editText.getText().toString(), this);
			editText.clearFocus();
			editText.setText("");
		} else if (v.getId() == R.id.tv_conversation_title) {

			((MainActivity) getActivity()).showFragement(v.getTag(), MainActivity.PROFILE_FRAGMENT, true);

		}
	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		if (isFragmentVisible) {
			if (success) {
				if (callCode == MyUtilities.CODE_CONVERSTION) {

					final MessageFetchGson messageGson = (MessageFetchGson) object;
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							if (messageGson.person != null) {
								String name = messageGson.person.u_fname + " " + messageGson.person.u_lname;
								AppLog.i("conversation name " + name);

								tv_titlename.setText(name);
								tv_titlename.setTag(messageGson.person.u_id);
								tv_titlename.setOnClickListener(ConversationFragment.this);

							}
						}
					});
					adapter.setDataList(messageGson.conversation);
					if (adapter.getCount() > 0) {
						listView.smoothScrollToPosition(adapter.getCount() - 1);
					}
				} else if (callCode == MyUtilities.CODE_SENDMESSAGE) {
					AppLog.i("CODE IS CONVERSTION FETCH");
					ApiController.getInstance(getActivity()).getUserConversationById(sender_id, this);
				}
			} else {
				AppLog.i("error " + object + " " + callCode);
			}
		}
		if (isHireMessage) {
			AppLog.i("profile", "ishire true");
			editText.setText(MyUtilities.MESSAGE_HIRE);
			isHireMessage = false;
		} else {
			AppLog.i("profile", "ishire false");
			editText.setText("");

		}

	}

	@Override
	public void onDestroyView() {
		isFragmentVisible = false;
		super.onDestroyView();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	public void reloadFragment(String exjson) {
		if (exjson != null) {
			Gson gson = new Gson();
			AppLog.i("exjson found to be " + exjson);
			MessageThreadGson messageThread = (MessageThreadGson) gson.fromJson(exjson, MessageThreadGson.class);
			if (messageThread != null) {
				sender_id = messageThread.u_id;
				if (messageThread.u_fname != null) {
					tv_titlename.setText(messageThread.u_fname + " " + messageThread.u_lname);
					tv_titlename.setOnClickListener(this);
				}
			}
		}
	}
}
