package com.app.touchtalent.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.ViewerAdapter;
import com.app.touchtalent.http.gson.ViewerGson;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;

public class ViewerFragment extends Fragment implements OnItemClickListener, DataReciever {

	private ListView listView;
	private ViewerAdapter adapter;
	private boolean isFragmentVisible = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.layout_profile_followers, null);
		listView = (ListView) view.findViewById(R.id.listview_followers);
		view.findViewById(R.id.btn_followers_menu).setOnClickListener((MainActivity) getActivity());
		listView.setOnItemClickListener(this);
		adapter = new ViewerAdapter(getActivity(), null);
		listView.setAdapter(adapter);
		isFragmentVisible = true;
		ApiController.getInstance(getActivity()).getViewerDetails(this);
		return view;

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart() {
		super.onStart();

	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		if (isFragmentVisible) {
			if (success) {
				if (callCode == MyUtilities.CODE_VIEWERS) {
					ViewerGson gson = (ViewerGson) object;
					adapter.setDataList(gson.viewers);
				}
			}
		}

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		isFragmentVisible = false;
	}

	public void reloadFragment() {
		// TODO Auto-generated method stub
		ApiController.getInstance(getActivity()).getViewerDetails(this);

	}

}
