package com.app.touchtalent.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.ExhibitionAdapter;
import com.app.touchtalent.http.gson.ExhibitionGson;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;

public class ExhibitionFragment extends Fragment implements DataReciever {

	private ListView listView;
	private ExhibitionAdapter adapter;
	private boolean isFragmentVisible = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.layout_fragment_exhibition, null);
		listView = (ListView) view.findViewById(R.id.listview_exhibitions);
		adapter = new ExhibitionAdapter(null, getActivity());
		listView.setAdapter(adapter);
		view.findViewById(R.id.btn_exhibition_menu).setOnClickListener(
				(MainActivity) getActivity());
		isFragmentVisible = true;
		ApiController.getInstance(getActivity()).getExhibitionList(this);
		return view;
	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		// TODO Auto-generated method stub
		if (isFragmentVisible) {
			if (success) {
				if (callCode == MyUtilities.CODE_EXHIBITION) {

					ExhibitionGson gson = (ExhibitionGson) object;
					adapter.setDataList(gson.exhibitions);
				}
			}
		}

	}

	@Override
	public void onDestroyView() {
		isFragmentVisible = false;
		super.onDestroyView();
	}
}
