package com.app.touchtalent.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.ExhibitionFeedAdapter;
import com.app.touchtalent.database.Exibition;
import com.app.touchtalent.datacontroller.DataController;
import com.app.touchtalent.datacontroller.ExhibitionReceiver;
import com.app.touchtalent.http.gson.Exhibitions;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.VolleySingleton;
import com.google.gson.Gson;

public class ExhibitionFeedFragment extends Fragment implements ExhibitionReceiver, OnScrollListener {

	private ListView listView;
	private DataController dataController;
	private String ex_id;
	private ExhibitionFeedAdapter adapter;
	private boolean loading;
	private NetworkImageView imageView_header;
	private ProgressBar progressbar;
	private List<Exibition> exhibitionList;
	private Exhibitions exhibition;
	private TextView tv_title;
	private boolean isFragmentVisible;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.layout_fragment_exhibitionfeeds, null);
		listView = (ListView) view.findViewById(R.id.listview_exfeeds);
		tv_title = (TextView) view.findViewById(R.id.tv_exfeeds_username);
		view.findViewById(R.id.btn_exfeeds_menu).setOnClickListener((MainActivity) getActivity());
		progressbar = (ProgressBar) view.findViewById(R.id.item_loading_pb);
		dataController = new DataController(getActivity(), null);
		Bundle bundle = getArguments();

		if (bundle != null) {
			String exjson = (String) bundle.getString("data");
			if (exjson != null) {
				Gson gson = new Gson();
				exhibition = (Exhibitions) (gson.fromJson(exjson, Exhibitions.class));
				if (exhibition != null) {
					ex_id = exhibition.ex_id;
					if (exhibition.ex_name != null) {
						tv_title.setText(exhibition.ex_name);
					}
				}
			}
		}
		exhibitionList = dataController.getExhibition(this, Long.valueOf(ex_id));
		adapter = new ExhibitionFeedAdapter(getActivity(), new ArrayList<Exibition>(exhibitionList));

		View headerview = inflater.inflate(R.layout.layout_header_exhibitionfeeds, null);
		imageView_header = (NetworkImageView) headerview.findViewById(R.id.imageView1);
		listView.addHeaderView(headerview);
		imageView_header.setImageUrl(MyUtilities.getExhibitionImageUrl(ex_id, 3), VolleySingleton.getInstance(getActivity())
				.getImageLoader());
		listView.setAdapter(adapter);
		listView.setOnScrollListener(this);
		progressbar.bringToFront();
		progressbar.setVisibility(View.VISIBLE);
		isFragmentVisible = true;
		return view;
	}

	@Override
	public void onExhibitionReceive(boolean success) {

		AppLog.i("exhibition recived " + success);
		if (isFragmentVisible) {
			if (success) {

				exhibitionList = dataController.getExhibition(this, Long.valueOf(ex_id));
				if (exhibitionList != null && exhibitionList.size() > 0) {
					adapter.addAll(exhibitionList, true);
					loading = false;
				}
			}
			if (progressbar.isShown()) {
				AppLog.i("exhibition gone second");
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						progressbar.setVisibility(View.GONE);
					}
				});
			}
		}
		try {
			progressbar.setVisibility(View.INVISIBLE);
		} catch (Exception e) {

		}

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (exhibitionList.size() > 0) {
			AppLog.i("exhibition gone first");
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					progressbar.setVisibility(View.GONE);

				}
			});
		}

	}

	@Override
	public void onDestroyView() {
		isFragmentVisible = false;
		super.onDestroyView();
	}

	public void reloadFragment(Exhibitions exhibition) {
		if (exhibition != null) {
			ex_id = exhibition.ex_id;
			if (exhibition.ex_name != null) {
				tv_title.setText(exhibition.ex_name);
			} else {
				tv_title.setText("Exhibition");
			}
		}

		exhibitionList = dataController.getExhibition(this, Long.valueOf(ex_id));
		adapter = new ExhibitionFeedAdapter(getActivity(), new ArrayList<Exibition>(exhibitionList));
		if (exhibitionList != null && exhibitionList.size() > 0) {
			listView.setAdapter(adapter);
			listView.invalidate();
			imageView_header.setImageUrl(MyUtilities.getExhibitionImageUrl(ex_id, 3), VolleySingleton.getInstance(getActivity())
					.getImageLoader());
			progressbar.setVisibility(View.GONE);
		}

	}
}
