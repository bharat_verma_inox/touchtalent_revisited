package com.app.touchtalent.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.CategoryAdapter;
import com.app.touchtalent.adapters.MyFeedAdapter;
import com.app.touchtalent.database.Category;
import com.app.touchtalent.database.Feed;
import com.app.touchtalent.datacontroller.CategoryReceiver;
import com.app.touchtalent.datacontroller.Constant;
import com.app.touchtalent.datacontroller.DataController;
import com.app.touchtalent.datacontroller.DataController.PrefValues;
import com.app.touchtalent.datacontroller.FeedReceiver;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.volley.DataReciever;
import com.google.gson.Gson;

public class HomeScreenFragment extends Fragment implements DataReciever, FeedReceiver, OnScrollListener, OnClickListener,
		CategoryReceiver {

	private ListView listView;
	private DataController dataController;
	private MyFeedAdapter adapter;
	CategoryAdapter catAdapter;
	private Button btn_category;
	private ArrayList<Category> categories;
	private boolean isFragmentVisible = false;
	private String category_selected = "Everything";
	private int selected_catid = -777;
	private int filter_selected = Constant.FILTER_MOST_TRANDING;
	private Button btn_filter;
	private int currentCode;
	private ArrayAdapter<String> trendAdapter;
	private List<Feed> feedlist;
	private boolean hasMoreData = true;

	public HomeScreenFragment() {
		super();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		AppLog.i("cat_id_1", "onCreate(Bundle) of Fragment HOmeSCreen");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.layout_fragment_home, null);
		view.findViewById(R.id.btn_home_menu).setOnClickListener((MainActivity) getActivity());
		view.findViewById(R.id.btn_home_upload).setOnClickListener((MainActivity) getActivity());
		btn_category = (Button) view.findViewById(R.id.btn_home_category);
		btn_filter = (Button) view.findViewById(R.id.btn_home_trending);
		catAdapter = new CategoryAdapter(null, getActivity());
		categories = new ArrayList<Category>();
		btn_category.setOnClickListener(HomeScreenFragment.this);
		btn_filter.setOnClickListener(HomeScreenFragment.this);
		listView = (ListView) view.findViewById(R.id.list_home_feed);
		feedlist = new ArrayList<Feed>();
		adapter = new MyFeedAdapter(getActivity(), (ArrayList<Feed>) feedlist);
		listView.setAdapter(adapter);
		AppLog.i("adapter created " + adapter);
		dataController = new DataController(getActivity(), this);
		listView.setOnScrollListener(this);
		trendAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_dropdown);
		ArrayList<String> filterList = new ArrayList<String>();
		filterList.add("Trending");
		filterList.add("Most Recent");
		filterList.add("Most Popular");
		trendAdapter.addAll(filterList);
		AppLog.i("my token " + MyPref.getMyTtlToken());

		AppLog.i("cat_id_1", "onCreateView of Fragment HOmeSCreen");
		AppLog.i("data found " + MyPref.getLastDate());
		initiateListView();
		isFragmentVisible = true;
		return view;
	}

	private void initiateListView() {
		isFragmentVisible = true;

		List<Category> allcategories = dataController.getAllcategories(this);
		if (allcategories != null && allcategories.size() > 0) {
			Category category = new Category();
			category.setCat_id(-777);
			category.setCat_name("Everything");
			categories.add(category);
			categories.addAll(allcategories);
		}
		String cat_id = "-777";
		Bundle bundle = getArguments();
		if (bundle != null) {
			String json = bundle.getString("data");
			Category category = new Gson().fromJson(json, Category.class);
			category_selected = category.getCat_name();
			btn_category.setText(category_selected);
			cat_id = category.getCat_id() + "";
		}

		getFeedsByFilterAndCategory(cat_id);
	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		AppLog.i("Request Completed " + success + " " + callCode + object);

	}

	@Override
	public void onCategoryreceive(boolean suceess) {
		if (suceess) {
			Category category = new Category();
			category.setCat_id(-777);
			category.setCat_name("Everything");
			categories.add(category);
			categories.addAll(dataController.getAllcategories(this));

		} else {
		}
	}

	@Override
	public void onFeedReceive(int callcode, boolean status, PrefValues values) {
		AppLog.i("cat_id", "onFeedRecieve " + callcode + status + " " + isFragmentVisible);
		if (true) {
			adapter.setIsLoadingData(false);

			if (status) {
				if (Constant.CODE_HOME_FEEDS < callcode && callcode <= Constant.CODE_HOME_FEEDS_POPULAR) {
					List<Feed> homeFeeds = dataController.getHomeFeeds(filter_selected, adapter.getCount());
					dataController.updatePreferences(values, callcode);
					currentCode = Constant.CODE_HOME_FEEDS;
					if (homeFeeds != null && homeFeeds.size() > 0) {
						AppLog.i("number of items added " + homeFeeds.size());
						adapter.addAll(homeFeeds, true);
					} else {
						AppLog.i("data not found in onFeedRecieve");
						hasMoreData = false;
					}
				} else if (Constant.CODE_CATEGORY_FEEDS < callcode && callcode <= Constant.CODE_CATEGORY_FEEDS_POPULAR) {
					List<Feed> feedsByCategory = dataController.getFeedsByCategory(selected_catid, filter_selected,
							adapter.getCount());
					currentCode = Constant.CODE_CATEGORY_FEEDS;
					if (feedsByCategory != null && feedsByCategory.size() > 0) {
						AppLog.i("cat_id", "OnFeedReceive Cat Size " + feedsByCategory.size());
						adapter.addAll(feedsByCategory, true);
						// dataController.updatePreferences(values, callcode);
					} else {
						hasMoreData = false;
					}

				}

			} else {
				hasMoreData = false;
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		if (adapter.shouldRequestNextPage(firstVisibleItem, visibleItemCount, totalItemCount) && hasMoreData) {
			adapter.setIsLoadingData(true);
			if (currentCode == Constant.CODE_HOME_FEEDS) {
				feedlist = dataController.getHomeFeeds(filter_selected, totalItemCount);
				currentCode = Constant.CODE_HOME_FEEDS;
				if (feedlist != null && feedlist.size() > 0) {
					updateScoreAndIdsIfnecessery();
					adapter.addAll(feedlist, true);
					adapter.setIsLoadingData(false);
				}
			} else if (currentCode == Constant.CODE_CATEGORY_FEEDS) {
				feedlist = dataController.getFeedsByCategory(selected_catid, filter_selected, totalItemCount);
				currentCode = Constant.CODE_CATEGORY_FEEDS;
				if (feedlist != null && feedlist.size() > 0) {
					updateScoreAndIdsIfnecessery();
					adapter.addAll(feedlist, true);
					adapter.setIsLoadingData(false);
				}
			}

		}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_home_category:
			AppLog.i("btn_clicked");

			catAdapter.setDataList(categories);
			new AlertDialog.Builder(getActivity()).setTitle("Choose Category")
					.setAdapter(catAdapter, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Category category = categories.get(which);
							btn_category.setText(category.getCat_name() + "");
							category_selected = category.getCat_name();
							btn_category.setBackgroundResource(R.drawable.btn_1_active);
							getFeedsByFilterAndCategory(category.getCat_id().toString());
							dialog.dismiss();
						}
					}).create().show();
			break;
		case R.id.btn_home_trending:
			new AlertDialog.Builder(getActivity()).setTitle("List By")
					.setAdapter(trendAdapter, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int which) {
							switch (which) {
							case 0:
								filter_selected = Constant.FILTER_MOST_TRANDING;
								btn_filter.setText("Trending");
								getFeedsByFilterAndCategory(selected_catid + "");
								break;
							case 1:
								filter_selected = Constant.FILTER_MOST_RECENT;
								btn_filter.setText("Most Recent");
								getFeedsByFilterAndCategory(selected_catid + "");
								break;
							case 2:
								filter_selected = Constant.FILTER_MOST_POPULAR;
								btn_filter.setText("Most Popular");
								getFeedsByFilterAndCategory(selected_catid + "");
								break;

							default:
								break;
							}

						}

					}).create().show();

			break;

		default:
			break;
		}

	}

	private void getFeedsByFilterAndCategory(String cat_id) {
		// TODO Auto-generated method stub
		hasMoreData = true;
		selected_catid = Integer.valueOf(cat_id);
		if (cat_id.equals("-777")) {
			AppLog.i("cat_id", "home feeds");
			feedlist = dataController.getHomeFeeds(filter_selected, 0);
			adapter.setIsLoadingData(true);
			currentCode = Constant.CODE_HOME_FEEDS;
			adapter.clear(true);
			if (feedlist != null && feedlist.size() > 0) {
				AppLog.i("cat_id", "home feeds retrieved " + feedlist.size());
				updateScoreAndIdsIfnecessery();
				adapter.addAll(feedlist, true);
				adapter.setIsLoadingData(false);
			}

		} else {
			AppLog.i("adapter status " + adapter);
			if (adapter == null)
				adapter = new MyFeedAdapter(getActivity(), new ArrayList<Feed>());
			adapter.clear(true);
			feedlist = dataController.getFeedsByCategory(Integer.parseInt(cat_id), filter_selected, 0);
			currentCode = Constant.CODE_CATEGORY_FEEDS;
			adapter.setIsLoadingData(true);
			if (feedlist != null && feedlist.size() > 0) {
				AppLog.i("cat_id", "catid feeds " + feedlist.size());
				adapter.addAll(feedlist, true);
				adapter.setIsLoadingData(false);
			}
		}

	}

	private void updateScoreAndIdsIfnecessery() {
		Feed feed = feedlist.get(feedlist.size() - 1);
		switch (filter_selected) {

		case Constant.FILTER_MOST_RECENT:
			if (MyPref.getRecentPostId() == MyPref.INVALIDATE_PID) {
				MyPref.setRecentDetails(feed.getPost_score(), feed.getPost_id());
			}

			break;
		case Constant.FILTER_MOST_POPULAR:
			if (MyPref.getPopularLastPostId() == MyPref.INVALIDATE_PID
					|| MyPref.getPopularLastScore() == MyPref.INVALIDATED_FLOAT) {
				MyPref.setPolpularDetails(feed.getPost_score(), feed.getPost_id());
			}

			break;
		case Constant.FILTER_MOST_TRANDING:
			if (MyPref.getTrendingLastPostId() == MyPref.INVALIDATE_PID
					|| MyPref.getTrendingLastScore() == MyPref.INVALIDATED_FLOAT) {
				MyPref.setTrendingDetails(feedlist.get(0).getPost_score(), feed.getPost_id(), feed.getPost_score());
			}

			break;

		default:
			break;
		}
	}

	public void reloadFragment(String data) {

		String json = data;
		isFragmentVisible = true;
		Category category = new Gson().fromJson(json, Category.class);
		category_selected = category.getCat_name();
		AppLog.i("cat data recieved " + category.getCat_id());
		if (btn_category != null) {
			btn_category.setText(category_selected);
		}
		String cat_id = category.getCat_id() + "";
		getFeedsByFilterAndCategory(cat_id);
	}

	@Override
	public void onDestroyView() {
		isFragmentVisible = false;
		super.onDestroyView();
	}

}