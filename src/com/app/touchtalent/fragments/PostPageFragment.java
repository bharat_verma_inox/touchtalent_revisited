package com.app.touchtalent.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.MyFeedAdapter;
import com.app.touchtalent.database.Feed;
import com.app.touchtalent.datacontroller.DataController;
import com.app.touchtalent.http.gson.ArtInfo;
import com.app.touchtalent.http.gson.ArtInfoGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;
import com.app.touchtalent.volley.VolleySingleton;
import com.google.gson.Gson;

public class PostPageFragment extends Fragment implements OnClickListener, DataReciever {
	public Feed feedGson;
	private TextView tv_title, tv_category, tv_views, tv_comments;
	private ImageButton btn_love, btn_comments, btn_rock, btn_awesome, btn_hatsoff, btn_salute;
	private NetworkImageView imageView;
	private ImageButton btn_broadcast;
	private ImageButton btn_menu, btn_follow;
	private ImageButton btn_twitter;
	private ImageButton btn_facebook;
	private ImageButton btn_gplus;
	private ImageButton btn_pintrest;
	private ImageButton btn_comment;
	private ImageButton btn_in;
	private TextView tv_content;
	private WebView webView;
	private ImageView btn_play;
	private TextView tv_username;
	private NetworkImageView image_dp;
	private TextView tv_toast;
	// private RelativeLayout rl_artist;
	private ImageButton btn_favourite;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_postpage, null);
		btn_menu = (ImageButton) view.findViewById(R.id.btn_postpage_menu);
		btn_follow = (ImageButton) view.findViewById(R.id.btn_postpage_follow);
		btn_favourite = (ImageButton) view.findViewById(R.id.btn_postpage_favorite);
		btn_menu.setOnClickListener((MainActivity) getActivity());
		// rl_artist = (RelativeLayout)
		// view.findViewById(R.id.rl_postpage_profile);
		imageView = (NetworkImageView) view.findViewById(R.id.image_postpage_photo);
		imageView.setDefaultImageResId(R.drawable.ic_image_loading);
		imageView.setErrorImageResId(R.drawable.ic_image_loading);
		btn_awesome = (ImageButton) view.findViewById(R.id.btn_postpage_awesome);
		btn_twitter = (ImageButton) view.findViewById(R.id.btn_postpage_twitter);
		btn_facebook = (ImageButton) view.findViewById(R.id.btn_postpage_facebook);
		btn_gplus = (ImageButton) view.findViewById(R.id.btn_postpage_gplus);
		btn_pintrest = (ImageButton) view.findViewById(R.id.btn_postpage_pinterest);
		btn_broadcast = (ImageButton) view.findViewById(R.id.btn_postpage_broadcast);
		btn_love = (ImageButton) view.findViewById(R.id.btn_postpage_love);
		btn_hatsoff = (ImageButton) view.findViewById(R.id.btn_postpage_hatsoff);
		btn_salute = (ImageButton) view.findViewById(R.id.btn_postpage_salute);
		btn_comment = (ImageButton) view.findViewById(R.id.btn_postpage_comment);
		btn_rock = (ImageButton) view.findViewById(R.id.btn_postpage_rock);

		tv_comments = (TextView) view.findViewById(R.id.tv_postpage_comments);
		tv_category = (TextView) view.findViewById(R.id.tv_postpage_category);
		tv_views = (TextView) view.findViewById(R.id.tv_postpage_views);
		tv_title = (TextView) view.findViewById(R.id.tv_postpage_title);
		tv_toast = (TextView) view.findViewById(R.id.tv_postpage_toast);
		btn_in = (ImageButton) view.findViewById(R.id.btn_postpage_linkedin);
		tv_content = (TextView) view.findViewById(R.id.tv_postpage_textcontent);
		webView = (WebView) view.findViewById(R.id.image_postpage_webview);
		btn_play = (ImageView) view.findViewById(R.id.image_postpage_play);
		tv_username = (TextView) view.findViewById(R.id.tv_postpage_username);

		image_dp = (NetworkImageView) view.findViewById(R.id.image_postpage_userdp);
		image_dp.setOnClickListener(this);
		tv_username.setOnClickListener(this);
		// rl_artist.setOnClickListener(this);
		btn_broadcast.setOnClickListener(this);
		btn_follow.setOnClickListener(this);
		btn_twitter.setOnClickListener(this);
		btn_gplus.setOnClickListener(this);
		btn_in.setOnClickListener(this);
		btn_favourite.setOnClickListener(this);
		btn_pintrest.setOnClickListener(this);
		btn_facebook.setOnClickListener(this);
		btn_comment.setOnClickListener(this);
		btn_awesome.setOnClickListener(this);
		btn_hatsoff.setOnClickListener(this);
		btn_love.setOnClickListener(this);
		btn_rock.setOnClickListener(this);
		btn_salute.setOnClickListener(this);
		imageView.setOnClickListener(this);
		// rl_broadcast = (RelativeLayout)
		// view.findViewById(R.id.rl_postpage_broadcastbuttons);
		initiateFragmentViews(null, false);
		return view;

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_postpage_awesome:
			((ImageButton) v).setImageResource(R.drawable.btn_postpage_awesome_active);
			postOpinion(v, MyFeedAdapter.AWESOME);
			MyUtilities.showAnimation(getActivity(), tv_toast, "Awesome");
			break;
		case R.id.btn_postpage_love:
			((ImageButton) v).setImageResource(R.drawable.btn_postpage_love_active);
			postOpinion(v, MyFeedAdapter.LOVED_IT);
			MyUtilities.showAnimation(getActivity(), tv_toast, "Loved It");

			break;
		case R.id.btn_postpage_comment:
			// shareImage("subject", null);
			((MainActivity) getActivity()).showFragement(feedGson.getPost_id(), MainActivity.COMMENT_FRAGMENT, true);
			break;
		case R.id.btn_postpage_rock:
			((ImageButton) v).setImageResource(R.drawable.btn_postpage_rock_active);
			postOpinion(v, MyFeedAdapter.YOU_ROCK);
			MyUtilities.showAnimation(getActivity(), tv_toast, "You Rock");
			break;
		case R.id.btn_postpage_hatsoff:
			((ImageButton) v).setImageResource(R.drawable.btn_postpage_hat_active);
			postOpinion(v, MyFeedAdapter.HATS_OFF);
			MyUtilities.showAnimation(getActivity(), tv_toast, "Hats Off!");
			break;
		case R.id.btn_postpage_salute:
			((ImageButton) v).setImageResource(R.drawable.btn_postpage_salute_active);
			postOpinion(v, MyFeedAdapter.SALUTE);
			MyUtilities.showAnimation(getActivity(), tv_toast, "Salute");
			break;
		case R.id.btn_postpage_facebook:
			MyUtilities.shareToFacebook(MyUtilities.getPostUrl(feedGson.getPost_slug(), "" + feedGson.getPost_id(),
					feedGson.getCategory_slug(), false), getActivity());
			break;
		case R.id.btn_postpage_twitter:
			MyUtilities.shareToTwitter(feedGson.getPost_title(), MyUtilities.getPostUrl(feedGson.getPost_slug(),
					"" + feedGson.getPost_id(), feedGson.getCategory_slug(), false), "", getActivity());
			break;
		case R.id.btn_postpage_pinterest:
			String imageUrl = "";
			if (Integer.valueOf(feedGson.getPost_mediatype()) == 0) {
				imageUrl = MyUtilities.getImageUrl(feedGson.getPost_slug(), "" + feedGson.getPost_id(), false);
			} else {
				imageUrl = "static.touchtalent.com/fb_dp.png ";
			}
			MyUtilities.shareToPinterest(feedGson.getPost_title(), MyUtilities.getPostUrl(feedGson.getPost_slug(),
					"" + feedGson.getPost_id(), feedGson.getCategory_slug(), false), imageUrl, getActivity());
			break;
		case R.id.btn_postpage_linkedin:
			MyUtilities.shareToLinkedIn(MyUtilities.getPostUrl(feedGson.getPost_slug(), "" + feedGson.getPost_id(),
					feedGson.getCategory_slug(), false), getActivity());
			break;
		case R.id.btn_postpage_gplus:
			MyUtilities.shareToGooglePlus(feedGson.getPost_title(), MyUtilities.getPostUrl(feedGson.getPost_slug(),
					"" + feedGson.getPost_id(), feedGson.getCategory_slug(), false), getActivity());
			break;
		case R.id.btn_postpage_broadcast:
			/*
			 * if (rl_broadcast.getVisibility() == View.VISIBLE) {
			 * 
			 * rl_broadcast.setVisibility(View.INVISIBLE); ((ImageButton)
			 * v).setImageResource(R.drawable.btn_postpage_broadcast); } else {
			 * ((ImageButton)
			 * v).setImageResource(R.drawable.btn_postpage_close);
			 * rl_broadcast.setVisibility(View.VISIBLE); }
			 */
			MyUtilities.shareDataOnApps(feedGson, getActivity());
			break;
		case R.id.btn_postpage_follow:
			Integer value = (Integer) v.getTag();
			if (value == 0) {
				ApiController.getInstance(getActivity()).followUser(this, feedGson.getUser_id() + "");
				v.setBackgroundResource(R.drawable.btn_post_follow);
				v.setTag(1);
			} else {
				v.setBackgroundResource(R.drawable.btn_post_unfollow);
				ApiController.getInstance(getActivity()).unfollowUser(this, feedGson.getUser_id() + "");
				v.setTag(0);
			}
			break;
		case R.id.image_postpage_userdp:
			((MainActivity) getActivity()).showFragement(feedGson.getUser_id(), MainActivity.PROFILE_FRAGMENT, true);
			break;
		case R.id.tv_postpage_username:
			((MainActivity) getActivity()).showFragement(feedGson.getUser_id(), MainActivity.PROFILE_FRAGMENT, true);
			break;
		/*
		 * case R.id.rl_postpage_profile: ((MainActivity)
		 * getActivity()).showFragement(feedGson.getUser_id(),
		 * MainActivity.PROFILE_FRAGMENT, true); break;
		 */
		case R.id.btn_postpage_favorite:
			if (feedGson.getIs_favourite().contains("0") || feedGson.getIs_favourite().contains("false")) {
				ApiController.getInstance(getActivity()).markFavorite(this, "" + feedGson.getPost_id());
				((ImageButton) v).setImageResource(R.drawable.btn_post_unfavourite);
				new DataController(getActivity(), null).updateMarkFavourate(feedGson.getPost_id());
			}
			break;
		default:
			break;
		}

	}

	private void postOpinion(View view, String type) {
		// TODO Auto-generFeedGson feedGson = (FeedGson) view.getTag();
		ApiController.getInstance(getActivity()).postOpinion(this, type, "1", "" + feedGson.getPost_id(), "0");

	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		// TODO Auto-generated method stub
		if (success) {
			AppLog.i("gcm", "recieved success");
			if (callCode == MyUtilities.CODE_ARTWORK) {
				AppLog.i("gcm", "recieved code ");
				ArtInfoGson gson = (ArtInfoGson) object;
				ArtInfo post = gson.post;
				try {
					Feed feed = new Feed(Long.valueOf(post.p_id), post.p_slug, post.p_title, Long.valueOf(post.u_id),
							post.p_description, "", post.p_mediatype, Integer.valueOf(post.cat_id),
							Integer.valueOf(post.num_comments), Integer.valueOf(post.p_views), Float.valueOf("0.0"),
							post.p_vlink, post.if_favourite, post.if_follow, post.p_timestamp, post.u_fname, post.u_lname,
							post.u_slug, post.isfile, post.cat_name, post.cat_slug);
					initiateFragmentViews(feed, true);
				} catch (Exception e) {
					AppLog.i("exception caught " + e);
					e.printStackTrace();
					initiateFragmentViews(null, true);
				}

			}
		}
	}

	private void initiateFragmentViews(Feed feed, boolean fromIntent) {
		if (fromIntent) {
			feedGson = feed;
		} else {
			Bundle bundle = getArguments();
			if (bundle != null) {
				String data = bundle.getString("data");

				Gson gson = new Gson();
				feedGson = gson.fromJson(data, Feed.class);
			}
		}
		if (feedGson != null) {
			String post_mediatype = feedGson.getPost_mediatype();
			int mediatype = Integer.valueOf(post_mediatype);
			if (mediatype == 0) {
				imageView.setVisibility(View.VISIBLE);
				tv_content.setVisibility(View.INVISIBLE);
				webView.setVisibility(View.INVISIBLE);
				imageView.setImageUrl(
						"http://full.creative.touchtalent.com/" + feedGson.getPost_slug() + "-" + feedGson.getPost_id() + ".jpg",
						VolleySingleton.getInstance(getActivity()).getImageLoader());
				btn_play.setVisibility(View.INVISIBLE);

			} else if (mediatype == 1) {
				if (feedGson.getIs_file() == null) {
					ApiController.getInstance(getActivity()).getCompleteArtInfo(this, feedGson.getPost_id() + "");
				} else {
					if (feedGson.getIs_file().contains("1")) {
						String url = "http://small.art.touchtalent.com/" + feedGson.getPost_slug() + "-small-"
								+ feedGson.getPost_id() + ".jpg";
						imageView.setImageUrl(url, VolleySingleton.getInstance(getActivity()).getImageLoader());

					} else {

						// webView.setVisibility(View.VISIBLE);
						// imageView.setVisibility(View.INVISIBLE);
						tv_content.setVisibility(View.INVISIBLE);
						// webView.getSettings().setJavaScriptEnabled(true);
						// webView.getSettings().setPluginState(PluginState.ON);
						// webView.loadUrl("http://www.youtube.com/embed/" +
						// feedGson.getPost_vlink() + "?autoplay=1&vq=small");
						// webView.setWebChromeClient(new WebChromeClient());
						imageView.setImageResource(R.drawable.logo_im);
						if (imageView != null) {
							AppLog.i("image url "
									+ MyUtilities.getImageUrl(feedGson.getPost_slug(), "" + feedGson.getPost_id(), true));
							imageView.setImageUrl("http://img.youtube.com/vi/" + feedGson.getPost_vlink() + "/0.jpg",
							// MyUtilities.,
									VolleySingleton.getInstance(getActivity()).getImageLoader());
						}
					}
					btn_play.setVisibility(View.INVISIBLE);

				}
			} else if (mediatype == 2) {
				imageView.setVisibility(View.INVISIBLE);
				tv_content.setVisibility(View.VISIBLE);
				webView.setVisibility(View.INVISIBLE);
				tv_content.setText(Html.fromHtml(feedGson.getPost_description()));
				btn_play.setVisibility(View.INVISIBLE);

			}

			tv_title.setText(feedGson.getPost_title());
			tv_category.setText(feedGson.getCategory_name());
			tv_comments.setText(feedGson.getNum_comments() + "");
			tv_views.setText(feedGson.getPost_views() + "");
			tv_username.setText(feedGson.getUser_fname() + " " + feedGson.getUser_lname());
			image_dp.setImageUrl(MyUtilities.getUserProfileImageUrl(feedGson.getUser_id() + "", feedGson.getUser_slug()),
					VolleySingleton.getInstance(getActivity()).getImageLoader());

			if (feedGson.getIs_followed() != null && feedGson.getIs_followed().contains("1")) {
				btn_follow.setTag(1);
				btn_follow.setBackgroundResource(R.drawable.btn_post_unfollow);

			} else {
				btn_follow.setTag(0);
				btn_follow.setBackgroundResource(R.drawable.btn_post_follow);

			}
			if ("false".equals(feedGson.getIs_favourite())) {
				btn_favourite.setImageResource(R.drawable.btn_post_favourite);
			} else if ("true".equals(feedGson.getIs_favourite()) || "1".equals(feedGson.getIs_favourite())) {
				btn_favourite.setImageResource(R.drawable.btn_post_unfavourite);
			}
		}

	}

	@Override
	public void onStop() {
		super.onStop();
	}

	public void loadFromNotification(String post_id) {

		ApiController.getInstance(getActivity()).getCompleteArtInfo(this, post_id);
	}

}
