package com.app.touchtalent.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.http.gson.Interactions;
import com.app.touchtalent.http.gson.UserGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.VolleySingleton;
import com.google.gson.Gson;

public class ProfileStatsFragment extends Fragment {

	private NetworkImageView imageView;
	private TextView tv_focus;
	private TextView tv_name;
	private TextView tv_location;
	// private TextView tv_keywords;
	private TextView tv_content;
	private TextView tv_opinion;
	private TextView tv_views;
	private TextView tv_following;
	private TextView tv_broadcast;
	private TextView tv_followers;
	private TextView tv_achievements;
	private TextView tv_bio;
	private TextView tv_interests;
	private boolean isFragmentVisible;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_aboutme, null);
		imageView = (NetworkImageView) view.findViewById(R.id.image_aboutme_dp);
		tv_focus = (TextView) view.findViewById(R.id.tv_aboutme_focus);
		tv_name = (TextView) view.findViewById(R.id.tv_aboutme_name);
		tv_location = (TextView) view.findViewById(R.id.tv_aboutme_location);
		// tv_keywords = (TextView) view.findViewById(R.id.tv_aboutme_keywords);

		tv_followers = (TextView) view.findViewById(R.id.tv_aboutme_follower1);
		tv_broadcast = (TextView) view.findViewById(R.id.tv_aboutme_broadcast1);
		tv_following = (TextView) view.findViewById(R.id.tv_aboutme_following1);
		tv_views = (TextView) view.findViewById(R.id.tv_aboutme_views1);
		tv_opinion = (TextView) view.findViewById(R.id.tv_aboutme_opinions1);
		tv_bio = (TextView) view.findViewById(R.id.tv_aboutme_bio);
		tv_achievements = (TextView) view.findViewById(R.id.tv_aboutme_achievements);
		tv_interests = (TextView) view.findViewById(R.id.tv_aboutme_interests);
		view.findViewById(R.id.btn_aboutme_menu).setOnClickListener((MainActivity) getActivity());
		String parcel = getArguments().getString("data");
		AppLog.i("user " + parcel);
		if (parcel != null) {
			Gson gson = new Gson();
			UserGson user = gson.fromJson(parcel, UserGson.class);
			initializeViews(user);
		}

		return view;
	}

	private void initializeViews(UserGson user) {
		// TODO Auto-generated method stub
		if (user.profile_interests != null && !user.profile_interests.contains("null")) {
			tv_focus.setText(user.profile_interests);
		} else {
			tv_focus.setText("");
		}
		if (user.profile_name != null && !user.profile_name.contains("null")) {
			tv_name.setText(user.profile_name);
		}

		if (user.profile_keywords != null && !user.profile_keywords.contains("null")) {
			// tv_keywords.setText(user.profile_keywords);
		} else {
			// tv_keywords.setText("");
		}
		if (user.profile_place != null && !user.profile_place.contains("null")) {
			tv_location.setText(user.profile_place);
		} else {
			tv_location.setText("");
		}
		imageView.setImageUrl(MyUtilities.getUserProfileImageUrl(user.u_id, user.u_slug),
				VolleySingleton.getInstance(getActivity()).getImageLoader());

		Interactions stats = user.interactions;

		tv_opinion.setText(stats.totalcomments + "");
		tv_views.setText(stats.totalviews + "");
		tv_following.setText(stats.totalfavourites + "");
		tv_broadcast.setText(stats.totalbroadcasts + "");
		tv_followers.setText(stats.totalposts + "");

		tv_bio.setText(user.profile_bio);
		tv_interests.setText(user.profile_interests);
		tv_achievements.setText(user.pro_achivements);

	}
}
