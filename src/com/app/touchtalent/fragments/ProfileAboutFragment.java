package com.app.touchtalent.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.http.gson.Interactions;
import com.app.touchtalent.http.gson.MessageThreadGson;
import com.app.touchtalent.http.gson.UserGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;
import com.app.touchtalent.volley.VolleySingleton;
import com.google.gson.Gson;

public class ProfileAboutFragment extends Fragment implements OnClickListener, DataReciever {

	private String u_id;
	private NetworkImageView imageView;
	private TextView tv_focus;
	private TextView tv_name;
	private TextView tv_location;
	private ImageButton btn_message;
	private ImageButton btn_views;
	private ImageButton btn_stats;
	private ImageButton btn_wow;
	private ImageButton btn_pro;
	private TextView tv_follow;
	private ImageButton btn_hireme;
	private TextView tv_skills;
	private TextView tv_services;
	private TextView tv_achivements;
	private TextView tv_aboutme;
	private UserGson user;
	private RelativeLayout rl_clickable;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_profile_about, null);

		view.findViewById(R.id.btn_profileabout_menu).setOnClickListener((MainActivity) getActivity());
		initializeViews(view);
		UserGson userInfoGson = null;
		Bundle data = getArguments();
		String json = data.getString("data");
		Gson gson = new Gson();
		userInfoGson = gson.fromJson(json, UserGson.class);
		if (userInfoGson != null) {
			u_id = userInfoGson.u_id;
			initiateViews(userInfoGson);
			AppLog.i("hello " + userInfoGson.pro_achivements + " ds " + userInfoGson.profile_bio);
		}
		AppLog.i("u_id sent " + u_id);
		return view;

	}

	private void initializeViews(View headerView) {
		imageView = (NetworkImageView) headerView.findViewById(R.id.image_profileabout_dp);
		tv_focus = (TextView) headerView.findViewById(R.id.tv_profileabout_focus);
		rl_clickable = (RelativeLayout) headerView.findViewById(R.id.rl_profileabout_profileholder);
		rl_clickable.setOnClickListener(this);
		tv_name = (TextView) headerView.findViewById(R.id.tv_profileabout_name);
		tv_location = (TextView) headerView.findViewById(R.id.tv_profileabout_location);
		btn_message = (ImageButton) headerView.findViewById(R.id.btn_profileabout_message);
		btn_views = (ImageButton) headerView.findViewById(R.id.btn_profileabout_views);
		btn_stats = (ImageButton) headerView.findViewById(R.id.btn_profileabout_stats);
		btn_wow = (ImageButton) headerView.findViewById(R.id.btn_profileabout_like);
		btn_pro = (ImageButton) headerView.findViewById(R.id.btn_profileabout_pro);
		btn_hireme = (ImageButton) headerView.findViewById(R.id.btn_profileabout_hireme);
		tv_follow = (TextView) headerView.findViewById(R.id.tv_profileabout_follow);

		headerView.findViewById(R.id.btn_profileabout_next).setOnClickListener(this);
		tv_aboutme = (TextView) headerView.findViewById(R.id.tv_profileabout_about1);
		tv_achivements = (TextView) headerView.findViewById(R.id.tv_profileabout_achivements1);
		tv_services = (TextView) headerView.findViewById(R.id.tv_profileabout_services1);
		tv_skills = (TextView) headerView.findViewById(R.id.tv_profileabout_skills1);
		tv_follow.setOnClickListener(this);
		btn_hireme.setOnClickListener(this);
		btn_stats.setOnClickListener(this);
		btn_views.setOnClickListener(this);
		btn_message.setOnClickListener(this);
		btn_wow.setOnClickListener(this);
		rl_clickable.setOnClickListener(this);

	}

	public void initiateViews(UserGson user) {
		this.user = user;
		if (user.u_type.contains("1")) {
			btn_pro.setVisibility(View.VISIBLE);
		} else {
			btn_pro.setVisibility(View.INVISIBLE);
		}
		String my_id = MyPref.getMyUserId();
		if (u_id.equals(my_id)) {
			btn_views.setVisibility(View.VISIBLE);
			btn_hireme.setVisibility(View.GONE);
			btn_stats.setVisibility(View.VISIBLE);
			btn_message.setVisibility(View.GONE);
			tv_follow.setVisibility(View.INVISIBLE);

		} else {
			btn_views.setVisibility(View.GONE);
			btn_hireme.setVisibility(View.VISIBLE);
			btn_stats.setVisibility(View.GONE);

			/*
			 * if (user.being_followed.contains("0")) { tv_follow.setTag(0);
			 * tv_follow.setBackgroundResource(R.drawable.btn_addprofile); }
			 * else { tv_follow.setTag(1);
			 * tv_follow.setBackgroundResource(R.drawable
			 * .btn_unfollow_postpage); // }
			 */
			if (user.being_followed.contains("0")) {
				tv_follow.setTag(0);
				tv_follow.setCompoundDrawablePadding(5);
				tv_follow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_post_follow, 0, 0, 0);
				tv_follow.setText("Follow");
				tv_follow.setCompoundDrawablePadding(8);
				tv_follow.setTextColor(getResources().getColor(R.color.header_text));
			} else {
				tv_follow.setTag(1);
				tv_follow.setText("Following");
				tv_follow.setTextColor(getResources().getColor(R.color.text_unfollow));
				tv_follow.setCompoundDrawablePadding(8);
				tv_follow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_post_unfollow, 0, 0, 0);
			}
		}
		if (user.profile_interests != null && !user.profile_interests.contains("null")) {
			tv_focus.setText(user.profile_interests);
		} else {
			tv_focus.setText("");
		}
		if (user.profile_name != null && !user.profile_name.contains("null")) {
			tv_name.setText(user.profile_name);
		}

		if (user.profile_place != null && !user.profile_place.contains("null")) {
			tv_location.setText(user.profile_place);
		} else {
			tv_location.setText("");
		}

		if (user.profile_bio != null) {
			tv_aboutme.setText(user.profile_bio);
		} else {
			tv_aboutme.setText("not available");
		}
		if (user.pro_skills != null) {
			tv_skills.setText(user.pro_skills);
		} else {
			tv_skills.setText("not available");
		}
		if (user.pro_achivements != null) {
			tv_achivements.setText(user.pro_achivements);
		} else {
			tv_achivements.setText("not available");
		}
		if (user.pro_services != null) {
			tv_services.setText(user.pro_services);
		} else {
			tv_services.setText("not available");
		}
		imageView.setImageUrl(MyUtilities.getUserProfileImageUrl(user.u_id, user.u_slug),
				VolleySingleton.getInstance(getActivity()).getImageLoader());

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_profileabout_views:
			((MainActivity) getActivity()).showFragement(null, MainActivity.VIEWER_FRAGMENT, true);
			break;
		case R.id.btn_profileabout_hireme:
			ConversationFragment.isHireMessage = true;
			MessageThreadGson messageThreadGson = new MessageThreadGson();
			messageThreadGson.u_id = user.u_id;
			messageThreadGson.u_fname = user.u_fname;
			messageThreadGson.u_lname = user.u_lname;
			((MainActivity) getActivity()).showFragement(messageThreadGson, MainActivity.CONVERSATION_FRAGMENT, true);
			break;
		case R.id.tv_profileabout_follow:
			Integer value = (Integer) v.getTag();
			if (value != null) {
				if (value == 0) {
					((TextView) v).setText("Following");
					((TextView) v).setTextColor(getResources().getColor(R.color.text_unfollow));
					ApiController.getInstance(getActivity()).followUser(this, u_id);
					((TextView) v).setCompoundDrawablePadding(8);
					((TextView) v).setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_post_unfollow, 0, 0, 0);
					v.setTag(1);
				} else {
					((TextView) v).setTextColor(getResources().getColor(R.color.header_text));
					((TextView) v).setCompoundDrawablePadding(5);
					((TextView) v).setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_post_follow, 0, 0, 0);
					ApiController.getInstance(getActivity()).unfollowUser(this, u_id);
					((TextView) v).setText("Follow");
					v.setTag(0);
				}
			}

			break;
		case R.id.btn_profileabout_share:
			String subject = "Hi, check out this Artist at Touchtalent";
			if (user.u_id.equals(MyPref.getMyUserId())) {
				subject = "Hi, Check out my profile at Touchtalent";
			}
			MyUtilities.shareText(MyUtilities.getUserProfileLink(user.u_slug, user.u_id), subject, getActivity());
			break;

		case R.id.btn_profileabout_stats:
			if (user != null) {
				((MainActivity) getActivity()).showFragement(user, MainActivity.ABOUTME_FRAGMENT, true);
			}
			break;
		case R.id.btn_profileabout_message:
			if (user != null) {
				MessageThreadGson messageThreadGson1 = new MessageThreadGson();
				messageThreadGson1.u_id = user.u_id;
				messageThreadGson1.u_fname = user.u_fname;
				messageThreadGson1.u_lname = user.u_lname;
				ConversationFragment.isHireMessage = false;
				((MainActivity) getActivity()).showFragement(messageThreadGson1, MainActivity.CONVERSATION_FRAGMENT, true);

			}
		case R.id.btn_profileabout_like:
			if (user != null) {
				Interactions intraction = user.interactions;
				if (!(intraction.totalbroadcasts == 0 && intraction.totalcomments == 0 && intraction.totalfavourites == 0
						&& intraction.totalposts == 0 && intraction.totalviews == 0)) {
					((MainActivity) getActivity()).showFragement(user.u_id, MainActivity.WOW_FRAGMENT, true);
				} else {
					Toast.makeText(getActivity(), "you dont have any intraction yet", Toast.LENGTH_SHORT).show();
				}

			}
			break;
		case R.id.btn_profileabout_next:
			if (user != null) {
				((MainActivity) getActivity()).showFragement(user.u_id, MainActivity.PROFILE_FRAGMENT, true);
			}
			break;
		case R.id.rl_profileabout_profileholder:
			if (user != null) {
				((MainActivity) getActivity()).showFragement(user.u_id, MainActivity.PROFILE_FRAGMENT, true);
			}
			break;
		default:
			break;
		}

	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		// TODO Auto-generated method stub

	}

}
