package com.app.touchtalent.fragments;

import java.util.ArrayList;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.R;
import com.app.touchtalent.http.gson.Country;
import com.app.touchtalent.http.gson.UserInfoGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;
import com.app.touchtalent.volley.VolleySingleton;

public class WowFragment extends Fragment implements DataReciever, OnClickListener {

	private TextView tv_headertext;
	private LinearLayout ll_scoreholder;
	private Typeface tf_avenir;
	private Typeface tf_score;
	private LinearLayout ll_countryholder;
	private UserInfoGson gson;
	private TextView tv_countryline;
	private TextView tv_bottomline;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		Bundle arguments = getArguments();
		String u_id = arguments.getString("data");

		View view = inflater.inflate(R.layout.layout_fragment_wow, null);
		tf_avenir = Typeface.createFromAsset(getActivity().getAssets(), "avenir-lt-95-black.ttf");
		tf_score = Typeface.createFromAsset(getActivity().getAssets(), "helveticaneue-bold.ttf");
		tv_headertext = (TextView) view.findViewById(R.id.tv_wow_headertext);
		tv_bottomline = (TextView) view.findViewById(R.id.tv_wow_bottomline);
		TextView tv_wow = (TextView) view.findViewById(R.id.tv_wow_headertext_wow);
		tv_headertext.setTypeface(tf_avenir);
		tv_wow.setTypeface(tf_avenir);
		tv_bottomline.setTypeface(tf_avenir);
		ll_scoreholder = (LinearLayout) view.findViewById(R.id.frame_wow_scoreholder);
		ll_countryholder = (LinearLayout) view.findViewById(R.id.ll_wow_countryholder);
		tv_countryline = (TextView) view.findViewById(R.id.tv_wow_countryline);
		Button btn_back = (Button) view.findViewById(R.id.btn_wow_back);
		btn_back.setTypeface(tf_avenir);
		btn_back.setOnClickListener(this);
		String headertext = "you are becoming popular<br>at Tochtalent";
		tv_headertext.setText(Html.fromHtml(headertext));

		if (u_id != null) {
			AppLog.i("uid recieved " + u_id);
			ApiController.getInstance(getActivity()).getUserProfileById(u_id, this);
		}
		view.findViewById(R.id.image_wow_facebook).setOnClickListener(this);
		view.findViewById(R.id.image_wow_gplus).setOnClickListener(this);
		view.findViewById(R.id.image_wow_in).setOnClickListener(this);
		view.findViewById(R.id.image_wow_pinterest).setOnClickListener(this);
		view.findViewById(R.id.image_wow_twitter).setOnClickListener(this);
		return view;

	}

	public void onDataReceived(Object object, int callCode, boolean success) {
		// TODO Auto-generated method stub
		if (success) {
			AppLog.i("onDataRecieved wow fragment");
			gson = (UserInfoGson) object;
			manageScoreView(gson.interactions);
			manageCountryViews(gson.countries);
			if (!gson.user.u_id.equals(MyPref.getMyUserId())) {
				String headertext = gson.user.u_fname + " is becoming popular<br>at Tochtalent";

				tv_headertext.setText(Html.fromHtml(headertext));
				tv_bottomline.setText("Intractions on " + gson.user.u_fname + "'s artworks");
			}

		}

	}

	private void manageCountryViews(ArrayList<Country> countries) {

		if (countries != null && countries.size() > 0) {
			int index = 0;
			for (Country country : countries) {
				String url = getCountryImageUrl(country.country_name);
				AppLog.i("country url " + country.country_name + " " + url);
				addCountryView(url, country.country_name, index);
				index++;
			}
		} else {
			AppLog.i("countries " + countries);
			tv_countryline.setVisibility(View.INVISIBLE);
		}
	}

	private void manageScoreView(String[] interactions) {
		// TODO Auto-generated method stub
		if (interactions != null && interactions.length > 0) {
			for (String digit : interactions) {
				addScoreDigit(digit);
			}
		} else {
			for (int i = 0; i < 5; i++) {
				addScoreDigit("0");

			}
		}
	}

	private String getCountryImageUrl(String name) {
		return "http://www.touchtalent.com/images/flags/Flag%20of%20" + name + ".png";
	}

	private void addCountryView(String url, String name, int index) {
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.country_item, null);
		NetworkImageView imageView = (NetworkImageView) view.findViewById(R.id.image_country);
		TextView text_countryName = (TextView) view.findViewById(R.id.tv_country);
		text_countryName.setText(name);
		text_countryName.setTypeface(tf_avenir, Typeface.BOLD);
		imageView.setImageUrl(url, VolleySingleton.getInstance(getActivity()).getImageLoader());
		ll_countryholder.addView(view);
	}

	private void addScoreDigit(String digit) {
		LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		layoutParams.leftMargin = 5;
		layoutParams.rightMargin = 5;
		layoutParams.bottomMargin = 2;
		layoutParams.topMargin = 2;
		TextView textView = new TextView(getActivity());
		textView.setBackgroundResource(R.drawable.bg_score);
		textView.setLayoutParams(layoutParams);
		textView.setText(digit);
		textView.setTextColor(Color.WHITE);
		textView.setTextSize(44);
		textView.setTypeface(tf_score);
		textView.setGravity(Gravity.CENTER);
		ll_scoreholder.addView(textView);
	}

	@Override
	public void onClick(View v) {
		if (gson != null && gson.user != null) {
			switch (v.getId()) {
			case R.id.image_wow_facebook:
				MyUtilities.shareToFacebook(getWowUrl(gson.user.u_id, gson.user.u_slug), getActivity());
				break;
			case R.id.image_wow_twitter:
				MyUtilities.shareToTwitter(" ", getWowUrl(gson.user.u_id, gson.user.u_slug), "", getActivity());
				break;
			case R.id.image_wow_pinterest:

				MyUtilities.shareToPinterest("", "", getWowUrl(gson.user.u_id, gson.user.u_slug), getActivity());
				break;
			case R.id.image_wow_in:
				MyUtilities.shareToLinkedIn(getWowUrl(gson.user.u_id, gson.user.u_slug), getActivity());
				break;
			case R.id.image_wow_gplus:
				MyUtilities.shareToGooglePlus("", getWowUrl(gson.user.u_id, gson.user.u_slug), getActivity());
				break;
			case R.id.btn_wow_back:
				getActivity().onBackPressed();
			default:
				break;
			}
		}

	}

	private String getWowUrl(String u_id, String u_slug) {
		return "www.touchtalent.com/artist-popularity/" + u_slug + "-" + u_id;
	}

	public void reloadFragment(String data) {
		if (data != null) {
			ll_countryholder.removeAllViews();
			ll_scoreholder.removeAllViews();
			AppLog.i("uid recieved " + data);
			ApiController.getInstance(getActivity()).getUserProfileById(data, this);
		}

	}

}
