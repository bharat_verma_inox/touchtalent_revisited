package com.app.touchtalent.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.CommentAdapter;
import com.app.touchtalent.http.ApiSource;
import com.app.touchtalent.http.gson.ArtInfoGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;

public class CommentFragment extends Fragment implements DataReciever, OnClickListener {

	private Button btn_post;
	private Button btn_menu;
	private ListView listview_comments;
	private CommentAdapter adapter;
	private String p_id;
	private boolean isFragmentVisible = false;
	EditText text;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_comments, null);
		btn_post = (Button) view.findViewById(R.id.btn_comments_post);
		btn_post.setOnClickListener(this);
		btn_menu = (Button) view.findViewById(R.id.btn_comments_menu);
		btn_menu.setOnClickListener((MainActivity) getActivity());
		listview_comments = (ListView) view.findViewById(R.id.listview_comments);

		text = (EditText) view.findViewById(R.id.et_comments_text);

		text.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				AppLog.i("IN onEditorAction");
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_GO) {
					AppLog.i("IN onEditorAction action code matches");
					// postComment(v.getText(),)
					ApiController.getInstance(getActivity()).postOpinion(CommentFragment.this, "1", MyPref.getMyUserId(), p_id,
							(String) v.getText().toString());
					text.clearFocus();
					text.setText("");
					hideSoftKeyboard(getActivity());
					ApiController.getInstance(getActivity()).getCompleteArtInfo(CommentFragment.this, p_id);
					return true;
				}
				return false;
			}
		});
		isFragmentVisible = true;
		p_id = getArguments().getString("data");
		ApiController.getInstance(getActivity()).getCompleteArtInfo(this, p_id);
		adapter = new CommentAdapter(getActivity(), null);
		listview_comments.setAdapter(adapter);
		// ApiController.getInstance(getActivity()).getCompleteArtInfo(CommentFragment.this,
		// p_id);
		return view;
	}

	@Override
	public void onDataReceived(final Object object, int callCode, boolean success) {
		AppLog.i("comment data recieved " + callCode + success + object);
		if (isFragmentVisible) {
			if (success) {
				if (callCode == MyUtilities.CODE_ARTWORK) {
					if (object != null)
						AppLog.i("setting adapter ");
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							AppLog.i("in runnable");
							adapter.setDataList(((ArtInfoGson) object).post.comments);

						}
					});
					// listview_comments.invalidate();
				} else {
					AppLog.i("object null ");
				}
			}
		}
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.btn_comments_post) {

			ApiController.getInstance(getActivity()).postOpinion(CommentFragment.this, "1", MyPref.getMyUserId(), p_id,
					(String) text.getText().toString());
			text.clearFocus();
			text.setText("");
			hideSoftKeyboard(getActivity());
		}
	}

	public static void hideSoftKeyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		if (inputMethodManager.isActive())
			inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}

	@Override
	public void onDestroyView() {
		isFragmentVisible = false;
		super.onDestroyView();
	}

	public void reloadFragment(String data) {
		// TODO Auto-generated method stub
		AppLog.i("reloading fragment " + data);
		// listview_comments.cl
	//	adapter.clearData();
		p_id = data;
		ApiController.getInstance(getActivity()).getCompleteArtInfo(this, p_id);
		// ApiController.getInstance(getActivity()).getCompleteArtInfo(CommentFragment.this,
		// p_id);

	}
}
