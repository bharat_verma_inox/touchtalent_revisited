package com.app.touchtalent.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.MailBoxAdapter;
import com.app.touchtalent.http.gson.MessageThreadFetchGson;
import com.app.touchtalent.http.gson.MessageThreadGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;

public class MailBoxFragment extends Fragment implements DataReciever,
		OnItemClickListener {

	private ListView listView;
	private MailBoxAdapter adapter;
	private boolean isFragmentVisible = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_inbox, null);
		listView = (ListView) view.findViewById(R.id.listview_messages);
		view.findViewById(R.id.btn_inbox_menu).setOnClickListener(
				(MainActivity) getActivity());
		listView.setOnItemClickListener(this);
		adapter = new MailBoxAdapter(getActivity(),
				new ArrayList<MessageThreadGson>());
		listView.setAdapter(adapter);
		ApiController.getInstance(getActivity()).getUserMailbox(this);
		return view;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long arg3) {
		AppLog.i("onItemClick ");
		MessageThreadGson messageObject = adapter.getMessageObject(position);
		ConversationFragment.isHireMessage = false;
		((MainActivity) getActivity()).showFragement(messageObject,
				MainActivity.CONVERSATION_FRAGMENT, true);

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		isFragmentVisible = true;

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		isFragmentVisible = false;
		super.onStop();
	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {
		// TODO Auto-generated method stub
		if (isFragmentVisible) {
			if (success && callCode == MyUtilities.CODE_MAILBOX) {
				MessageThreadFetchGson gson = (MessageThreadFetchGson) object;
				adapter.setDataList(gson.messages);
			}
		}

	}
}
