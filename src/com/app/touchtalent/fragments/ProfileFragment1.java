package com.app.touchtalent.fragments;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.app.touchtalent.MainActivity;
import com.app.touchtalent.R;
import com.app.touchtalent.adapters.MyFeedAdapter;
import com.app.touchtalent.database.Feed;
import com.app.touchtalent.datacontroller.DataController;
import com.app.touchtalent.datacontroller.UserFeedListener;
import com.app.touchtalent.http.gson.Interactions;
import com.app.touchtalent.http.gson.MessageThreadGson;
import com.app.touchtalent.http.gson.UserGson;
import com.app.touchtalent.http.gson.UserInfoGson;
import com.app.touchtalent.parser.DataManager;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;
import com.app.touchtalent.volley.VolleySingleton;

public class ProfileFragment1 extends Fragment implements DataReciever, UserFeedListener, OnClickListener, OnScrollListener {

	TextView tv_name, tv_location;
	NetworkImageView imageView;
	ImageButton btn_views, btn_stats, btn_wow;
	private ListView listView;
	private DataController dataController;
	private MyFeedAdapter adapter;
	private String u_id;
	private View headerView;
	private ImageButton btn_pro;
	private ImageButton btn_hireme;
	private UserGson user;
	private TextView tv_follow;
	private int page = 0;
	private ImageButton btn_message;
	private View emptyView;
	private View view;
	private NetworkImageView imageView1;
	// private TextView tv_focus1;
	private TextView tv_name1;
	private TextView tv_location1;
	private ImageButton btn_message1;
	private ImageButton btn_views1;
	private ImageButton btn_stats1;
	private ImageButton btn_hireme1;
	private boolean hasMoreData = true;
	private ImageButton btn_aboutpage1;
	private ImageButton btn_aboutpage;
	private RelativeLayout rl_clickable;
	private RelativeLayout rl_clickable1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.layout_profile_one, null);
		view.findViewById(R.id.btn_profileone_share).setOnClickListener(this);
		headerView = inflater.inflate(R.layout.layout_header_profile, null);
		emptyView = inflater.inflate(R.layout.layout_header_profile, null);
		view.findViewById(R.id.btn_profileone_menu).setOnClickListener((MainActivity) getActivity());
		initializeViews();
		u_id = getArguments().getString("data");
		AppLog.i("u_id sent " + u_id);
		ApiController.getInstance(getActivity()).getUserProfileById(u_id, this);
		dataController = new DataController(getActivity(), null);
		page = 1;
		dataController.getUserFeeds(Integer.valueOf(u_id), page, this);
		listView.addHeaderView(headerView);
		adapter = new MyFeedAdapter(getActivity(), new ArrayList<Feed>());
		listView.setAdapter(adapter);
		listView.setOnScrollListener(this);
		return view;

	}

	private void initializeViews() {
		imageView = (NetworkImageView) headerView.findViewById(R.id.image_profileone_dp);
		rl_clickable = (RelativeLayout) headerView.findViewById(R.id.rl_profileone_clickable);

		// tv_focus = (TextView)
		// headerView.findViewById(R.id.tv_profileone_focus);
		tv_name = (TextView) headerView.findViewById(R.id.tv_profileone_name);
		tv_location = (TextView) headerView.findViewById(R.id.tv_profileone_location);
		btn_message = (ImageButton) headerView.findViewById(R.id.btn_profileone_message);
		// tv_keywords = (TextView)
		// headerView.findViewById(R.id.tv_profileone_keywords);
		btn_views = (ImageButton) headerView.findViewById(R.id.btn_profileone_views);
		btn_stats = (ImageButton) headerView.findViewById(R.id.btn_profileone_stats);
		btn_wow = (ImageButton) headerView.findViewById(R.id.btn_profileone_like);
		rl_clickable1 = (RelativeLayout) emptyView.findViewById(R.id.rl_profileone_clickable);
		imageView1 = (NetworkImageView) emptyView.findViewById(R.id.image_profileone_dp);
		// tv_focus1 = (TextView)
		// emptyView.findViewById(R.id.tv_profileone_focus);
		tv_name1 = (TextView) emptyView.findViewById(R.id.tv_profileone_name);
		tv_location1 = (TextView) emptyView.findViewById(R.id.tv_profileone_location);
		btn_message1 = (ImageButton) emptyView.findViewById(R.id.btn_profileone_message);
		btn_aboutpage1 = (ImageButton) emptyView.findViewById(R.id.btn_profileone_next);
		btn_aboutpage = (ImageButton) headerView.findViewById(R.id.btn_profileone_next);
		btn_aboutpage.setVisibility(View.VISIBLE);
		btn_aboutpage1.setVisibility(View.VISIBLE);
		btn_aboutpage.setOnClickListener(this);
		btn_aboutpage1.setOnClickListener(this);
		rl_clickable.setOnClickListener(this);
		rl_clickable1.setOnClickListener(this);

		btn_views1 = (ImageButton) emptyView.findViewById(R.id.btn_profileone_views);
		btn_stats1 = (ImageButton) emptyView.findViewById(R.id.btn_profileone_stats);
		listView = (ListView) view.findViewById(R.id.list_profileone_feed);

		btn_pro = (ImageButton) headerView.findViewById(R.id.btn_profileone_pro);
		btn_hireme = (ImageButton) headerView.findViewById(R.id.btn_profileone_hireme);
		tv_follow = (TextView) view.findViewById(R.id.tv_profileone_follow);
		tv_follow.setOnClickListener(this);
		btn_hireme.setOnClickListener(this);
		btn_stats.setOnClickListener(this);
		btn_views.setOnClickListener(this);
		btn_message.setOnClickListener(this);
		btn_hireme1 = (ImageButton) emptyView.findViewById(R.id.btn_profileone_hireme);

		btn_hireme1.setOnClickListener(this);
		btn_stats1.setOnClickListener(this);
		btn_views1.setOnClickListener(this);
		btn_message1.setOnClickListener(this);
		btn_wow.setOnClickListener(this);

	}

	@Override
	public void onDataReceived(Object object, int callCode, boolean success) {

		if (callCode == MyUtilities.CODE_USERINFO) {
			if (success) {
				UserInfoGson userGson = (UserInfoGson) object;
				user = userGson.user;
				if (user.u_type.contains("1")) {
					btn_pro.setVisibility(View.VISIBLE);
				} else {
					btn_pro.setVisibility(View.INVISIBLE);
				}
				String my_id = DataManager.getMnger(getActivity()).getUser().u_id;
				if (u_id.equals(my_id)) {
					btn_views.setVisibility(View.VISIBLE);
					btn_hireme.setVisibility(View.GONE);
					btn_stats.setVisibility(View.VISIBLE);
					btn_message.setVisibility(View.GONE);
					tv_follow.setVisibility(View.INVISIBLE);
					btn_views1.setVisibility(View.VISIBLE);
					btn_hireme1.setVisibility(View.GONE);
					btn_stats1.setVisibility(View.VISIBLE);
					btn_message1.setVisibility(View.GONE);
				} else {
					btn_views.setVisibility(View.GONE);
					btn_hireme.setVisibility(View.VISIBLE);
					btn_stats.setVisibility(View.GONE);
					btn_views1.setVisibility(View.GONE);
					btn_hireme1.setVisibility(View.VISIBLE);
					btn_stats1.setVisibility(View.GONE);
					btn_message.setVisibility(View.VISIBLE);
					btn_message1.setVisibility(View.VISIBLE);
					if (user.being_followed.contains("0")) {
						tv_follow.setTag(0);
						tv_follow.setCompoundDrawablePadding(5);
						tv_follow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_post_follow, 0, 0, 0);
						tv_follow.setText("Follow");
						tv_follow.setCompoundDrawablePadding(8);
						tv_follow.setTextColor(Color.parseColor("#00feff"));
					} else {
						tv_follow.setTag(1);
						tv_follow.setText("Following");
						tv_follow.setTextColor(Color.parseColor("#063A3A"));
						tv_follow.setCompoundDrawablePadding(8);
						tv_follow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_post_unfollow, 0, 0, 0);
					}

				}
				if (user.profile_name != null && !user.profile_name.contains("null")) {
					tv_name.setText(user.profile_name);
					tv_name1.setText(user.profile_name);
				}

				// if (user.profile_keywords != null &&
				// !user.profile_keywords.contains("null")) {
				// tv_keywords.setText(user.profile_keywords);
				// } else {
				// tv_keywords.setText("");
				// }
				if (user.profile_place != null && !user.profile_place.contains("null")) {
					tv_location.setText(user.profile_place);
					tv_location1.setText(user.profile_place);
				} else {
					tv_location.setText("");
					tv_location1.setText("");
				}
				imageView.setImageUrl(MyUtilities.getUserProfileImageUrl(user.u_id, user.u_slug),
						VolleySingleton.getInstance(getActivity()).getImageLoader());
				imageView1.setImageUrl(MyUtilities.getUserProfileImageUrl(user.u_id, user.u_slug),
						VolleySingleton.getInstance(getActivity()).getImageLoader());
			} else {
				AppLog.i("Error occured");
			}

		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_profileone_views:
			((MainActivity) getActivity()).showFragement(null, MainActivity.VIEWER_FRAGMENT, true);
			break;
		case R.id.btn_profileone_hireme:
			ConversationFragment.isHireMessage = true;
			MessageThreadGson messageThreadGson = new MessageThreadGson();
			messageThreadGson.u_id = user.u_id;
			messageThreadGson.u_fname = user.u_fname;
			messageThreadGson.u_lname = user.u_lname;
			((MainActivity) getActivity()).showFragement(messageThreadGson, MainActivity.CONVERSATION_FRAGMENT, true);
			break;
		case R.id.tv_profileone_follow:
			Integer value = (Integer) v.getTag();
			if (value != null) {
				if (value == 0) {
					((TextView) v).setText("Following");
					((TextView) v).setTextColor(getResources().getColor(R.color.text_unfollow));
					ApiController.getInstance(getActivity()).followUser(this, u_id);
					((TextView) v).setCompoundDrawablePadding(8);
					((TextView) v).setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_post_unfollow, 0, 0, 0);
					v.setTag(1);
				} else {
					((TextView) v).setTextColor(getResources().getColor(R.color.header_text));
					((TextView) v).setCompoundDrawablePadding(5);
					((TextView) v).setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_post_follow, 0, 0, 0);
					ApiController.getInstance(getActivity()).unfollowUser(this, u_id);
					((TextView) v).setText("Follow");
					v.setTag(0);
				}
			}
			break;

		case R.id.btn_profileone_stats:
			if (user != null) {
				((MainActivity) getActivity()).showFragement(user, MainActivity.ABOUTME_FRAGMENT, true);
			}
			break;
		case R.id.btn_profileone_message:
			if (user != null) {
				MessageThreadGson messageThreadGson1 = new MessageThreadGson();
				messageThreadGson1.u_id = user.u_id;
				messageThreadGson1.u_fname = user.u_fname;
				messageThreadGson1.u_lname = user.u_lname;
				ConversationFragment.isHireMessage = false;
				((MainActivity) getActivity()).showFragement(messageThreadGson1, MainActivity.CONVERSATION_FRAGMENT, true);

			}
		case R.id.btn_profileone_like:
			if (user != null) {
				Interactions intraction = user.interactions;
				if (!(intraction.totalbroadcasts == 0 && intraction.totalcomments == 0 && intraction.totalfavourites == 0
						&& intraction.totalposts == 0 && intraction.totalviews == 0)) {
					((MainActivity) getActivity()).showFragement(user.u_id, MainActivity.WOW_FRAGMENT, true);
				} else {
					Toast.makeText(getActivity(), "you dont have any intraction yet", Toast.LENGTH_SHORT).show();
				}

			}
			break;
		case R.id.btn_profileone_next:
			if (user != null) {
				((MainActivity) getActivity()).showFragement(user, MainActivity.PROFILE_ABOUT_FRAGMENT, true);
			}
			break;
		case R.id.rl_profileone_clickable:
			if (user != null) {
				((MainActivity) getActivity()).showFragement(user, MainActivity.PROFILE_ABOUT_FRAGMENT, true);
			}
			break;
		case R.id.btn_profileone_share:
			String subject = "Hi, check out this Artist at Touchtalent";
			if (user.u_id.equals(MyPref.getMyUserId())) {
				subject = "Hi, Check out my profile at Touchtalent";
			}
			MyUtilities.shareText(MyUtilities.getUserProfileLink(user.u_slug, user.u_id), subject, getActivity());
			break;
		default:
			break;
		}

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		if (adapter.shouldRequestNextPage(firstVisibleItem, visibleItemCount, totalItemCount) && hasMoreData) {
			adapter.setIsLoadingData(true);
			page++;
			List<Feed> feedlist1 = dataController.getUserFeeds(Long.parseLong(u_id), page, this);
			if (feedlist1 != null && feedlist1.size() > 0) {
				adapter.addAll(feedlist1, true);
				adapter.setIsLoadingData(false);
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserFeedRecieved(boolean success, int callCode, List<Feed> feedList, int page) {
		if (success) {
			adapter.addAll(feedList, true);
			adapter.setIsLoadingData(false);
		} else {
			if (adapter.isEmpty()) {
				ViewGroup group = (ViewGroup) listView.getParent();
				LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
				layoutParams.addRule(RelativeLayout.BELOW, R.id.rl_profileone_header);

				group.removeAllViews();
				group.addView(emptyView, layoutParams);
			}
			adapter.setIsLoadingData(false);
			hasMoreData = false;

		}
	}

	public void reloadFragment(String parcal) {

		u_id = parcal;
		adapter.clear(true);
		ApiController.getInstance(getActivity()).getUserProfileById(u_id, this);
		page = 1;
		dataController.getUserFeeds(Integer.valueOf(u_id), page, this);
	}

}
