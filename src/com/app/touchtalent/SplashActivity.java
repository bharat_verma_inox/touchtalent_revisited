package com.app.touchtalent;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.app.touchtalent.http.ApiSource;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.ContactExtractor;
import com.app.touchtalent.utils.MyGcmHelper;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.DataReciever;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;

public class SplashActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		findViewById(R.id.btn_splash_login).setOnClickListener(this);
		findViewById(R.id.btn_splash_register).setOnClickListener(this);
		MyPref.init(this);
		MyGcmHelper helper = new MyGcmHelper(this);
		helper.getGcmRegId();
		String user_token = MyPref.getMyTtlToken();
		startContactsFetching();
		if (user_token != null && user_token.length() > 0) {
			// changing here
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();

		}
		AppLog.i("inviting");
		// ApiController.inviteSpecificFriend("767538372",
		// TtlConst.TT_FB_APP_ID, MyPref.getMyFbToken());

	}

	private void startContactsFetching() {

		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				// sendContacts();
				return null;
			}
		}.execute();
		// TODO Auto-generated method stub

		sendContacts();
	}

	public void onLoginButtonClick(View view) {
		AppLog.i("onLoginButtonClicked");
		goToLogin(true);
	}

	public void onRegisterButtonClick(View view) {
		goToLogin(false);
	}

	private void goToLogin(boolean value) {
		AppLog.i("onLoginButtonClicked with value");
		Intent in = new Intent(this, RegLoginActivity.class);
		in.putExtra("isLogin", value);
		startActivity(in);
		finish();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_splash_login:
			onLoginButtonClick(v);
			break;
		case R.id.btn_splash_register:
			onRegisterButtonClick(v);

			break;

		default:
			break;
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, MyUtilities.FLURRY_KEY);
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
		//
		// long start = System.currentTimeMillis();
		// // ContactExtractor.getContacts(this);
		// long first = System.currentTimeMillis();
		// // ContactExtractor.getContacts(this);
		// long second = System.currentTimeMillis();
		// long diff1 = first - start;
		// long diff2 = second - first;
		// AppLog.i("time taken " + diff1 + " and " + diff2);

	}

	private void sendContacts() {
		if (!MyPref.getIsContectSent()) {
			AppLog.i("contact", "not sent");
			String contacts = ContactExtractor.getContactDetails(this);
			Log.i("contacts", "here are the contacts" + contacts);
			ApiSource.getInstance().sendContactData(contacts, "", new DataReciever() {

				@Override
				public void onDataReceived(Object object, int callCode, boolean success) {
					// TODO Auto-generated method stub
					if (success) {
						AppLog.i("success recieved");
						// MyPref.setIsContectSent(true);
						ApiSource.getInstance().sendContactData("", ContactExtractor.getEmailDetails(SplashActivity.this),
								new DataReciever() {

									@Override
									public void onDataReceived(Object object, int callCode, boolean success) {
										// TODO Auto-generated method stub
										AppLog.i("success recieved");
										MyPref.setIsContectSent(true);
									}
								}, SplashActivity.this);
					}
				}
			}, this);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);
	}

}
