package com.app.touchtalent.parser;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;


public class Portfolio extends BaseData{

	private static final long serialVersionUID = 15465467L;

	@SerializedName("portfolios")
	public ArrayList<PortFolioData> portfolios; 

	public class PortFolioData implements Serializable{
		private static final long serialVersionUID = 15465323L;

		@SerializedName("folder_id")
		public String folder_id; 

		@SerializedName("folder_name")
		public String folder_name; 

		@SerializedName("folder_slug")
		public String folder_slug; 

		@SerializedName("folder_status")
		public String folder_status; 

		@SerializedName("folder_count")
		public String folder_count; 

	}

}
