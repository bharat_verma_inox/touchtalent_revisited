package com.app.touchtalent.parser;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class User implements Serializable{

	private static final long serialVersionUID = 19320L;

	@SerializedName("u_id")
	public String u_id;
	
	@SerializedName("u_fname")
	public String u_fname;
	
	@SerializedName("u_lname")
	public String u_lname;
	
	@SerializedName("u_email")
	public String u_email;
	
	@SerializedName("u_fbid")
	public String u_fbid;
	
	@SerializedName("u_fbmail")
	public String u_fbmail;
	
	@SerializedName("u_gplusid")
	public String u_gplusid;
	
	@SerializedName("u_gmail")
	public String u_gmail;
	
	@SerializedName("u_status")
	public String u_status;
	
	@SerializedName("u_slug")
	public String u_slug;
	
	@SerializedName("u_conn")
	public String u_conn;
	
	@SerializedName("u_token")
	public String u_token;
	
	@SerializedName("u_type")
	public String u_type="";
	
	public boolean isProUser(){
		return true;//u_type.equalsIgnoreCase("1");
	}
	
}
