package com.app.touchtalent.parser;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class BaseData implements Serializable{

	private static final long serialVersionUID = 15465467L;
	
	@SerializedName("status")
	public String status;
	
	@SerializedName("message")
	public String message;

}
