package com.app.touchtalent.parser;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.app.touchtalent.database.Category;
import com.app.touchtalent.datacontroller.DataController;
import com.app.touchtalent.utils.MyPref;

public class DataManager {

	private static DataManager sDataManager = null;

	private static SharedPreferences mSharedpref;
	public static final String PREFERENCES = "TouchTalent";

	public static final String NAME = "usename";
	public static final String UID = "user_id";
	private static Editor mEditor;

	public static final int TYPE_EMAIL = 121;
	public static final int TYPE_FB = 122;
	public static final int TYPE_GPLUS = 131;

	public static final String TYPE = "login_type";
	public static final String u_id = "u_id";
	public static final String u_fname = "u_fname";
	public static final String u_lname = "u_lname";
	public static final String u_email = "u_email";
	public static final String u_fbid = "u_fbid";
	public static final String u_fbmail = "u_fbmail";
	public static final String u_gplusid = "u_gmail";
	public static final String u_gmail = "";
	public static final String u_status = "u_status";
	public static final String u_slug = "u_slug";
	public static final String u_conn = "u_conn";
	public static final String u_token = "u_token";

	private Catagories mCatagories = null;

	private Context context;

	public void setCatagories(Catagories catagories) {
		mCatagories = catagories;
	}

	public ArrayList<Category> getCatagories() {
		return (ArrayList<Category>) new DataController(context, null).getAllcategories(null);
	}

	private DataManager(Context context) {
		if (mSharedpref == null) {
			mSharedpref = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
			mEditor = mSharedpref.edit();

		}
		this.context = context;
	}

	public static DataManager getMnger(Context context) {
		if (sDataManager == null)
			sDataManager = new DataManager(context);

		return sDataManager;
	}

	public User getUser() {
		User user = new User();
		if (mSharedpref != null) {
			user.u_id = mSharedpref.getString(u_id, "");
			user.u_fname = mSharedpref.getString(u_fname, "");
			user.u_lname = mSharedpref.getString(u_lname, "");

			user.u_email = mSharedpref.getString(u_email, "");
			user.u_fbid = mSharedpref.getString(u_fbid, "");
			user.u_fbmail = mSharedpref.getString(u_fbmail, "");
			user.u_gplusid = mSharedpref.getString(u_gplusid, "");
			user.u_gmail = mSharedpref.getString(u_gmail, "");
			user.u_status = mSharedpref.getString(u_status, "");
			user.u_slug = mSharedpref.getString(u_slug, "");
			user.u_conn = mSharedpref.getString(u_conn, "");
			user.u_token = mSharedpref.getString(u_token, "");
		}
		return user;
	}

	public int getLoginType() {
		return mSharedpref != null ? mSharedpref.getInt(TYPE, 0) : 0;
	}

	public void saveUser(User token, int type) {
		if (mEditor != null) {
			mEditor.putInt(TYPE, type).commit();
			mEditor.putString(u_id, token.u_id).commit();
			mEditor.putString(u_fname, token.u_fname).commit();
			mEditor.putString(u_lname, token.u_lname).commit();
			mEditor.putString(u_email, token.u_email).commit();
			mEditor.putString(u_fbid, token.u_fbid).commit();
			mEditor.putString(u_fbmail, token.u_fbmail).commit();
			mEditor.putString(u_gplusid, token.u_gplusid).commit();
			mEditor.putString(u_gmail, token.u_gmail).commit();
			mEditor.putString(u_status, token.u_status).commit();
			mEditor.putString(u_slug, token.u_slug).commit();
			mEditor.putString(u_conn, token.u_conn).commit();
			mEditor.putString(u_token, token.u_token).commit();
		}
	}

}
