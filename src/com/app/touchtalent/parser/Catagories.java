package com.app.touchtalent.parser;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;


public class Catagories extends BaseData{

	private static final long serialVersionUID = 15465467L;
	
	@SerializedName("categories")
	public ArrayList<Category> category; 

	public class Category implements Serializable{
		private static final long serialVersionUID = 15465323L;

		@SerializedName("cat_id")
		public String cat_id; 

		@SerializedName("cat_name")
		public String cat_name; 

		@SerializedName("cat_slug")
		public String cat_slug; 

		@SerializedName("cat_domain")
		public String cat_domain; 

	}
	
	public void setEveryhting(){
		if(category!=null){
			Category category= new Category();
			category.cat_id="Everything";
			category.cat_name="Everything";
			this.category.add(0, category);
		}
	}
}
