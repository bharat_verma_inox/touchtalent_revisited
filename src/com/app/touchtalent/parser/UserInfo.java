package com.app.touchtalent.parser;

import com.google.gson.annotations.SerializedName;

public class UserInfo extends BaseData{

	private static final long serialVersionUID = 15465467L;
	
	@SerializedName("user")
	public User user; 
	
}
