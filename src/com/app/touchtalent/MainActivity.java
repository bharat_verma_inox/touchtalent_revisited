package com.app.touchtalent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.app.slidingmenu.lib.SlidingMenu;
import com.app.touchtalent.adapters.ExpendableAdapter;
import com.app.touchtalent.database.Category;
import com.app.touchtalent.datacontroller.CategoryReceiver;
import com.app.touchtalent.datacontroller.DataController;
import com.app.touchtalent.fragments.CommentFragment;
import com.app.touchtalent.fragments.ConversationFragment;
import com.app.touchtalent.fragments.ExhibitionFeedFragment;
import com.app.touchtalent.fragments.ExhibitionFragment;
import com.app.touchtalent.fragments.HomeScreenFragment;
import com.app.touchtalent.fragments.MailBoxFragment;
import com.app.touchtalent.fragments.PostPageFragment;
import com.app.touchtalent.fragments.ProfileAboutFragment;
import com.app.touchtalent.fragments.ProfileFragment1;
import com.app.touchtalent.fragments.ProfileStatsFragment;
import com.app.touchtalent.fragments.SearchFragment;
import com.app.touchtalent.fragments.ViewerFragment;
import com.app.touchtalent.fragments.WowFragment;
import com.app.touchtalent.http.ApiCallResponse;
import com.app.touchtalent.http.ApiSource;
import com.app.touchtalent.http.gson.Exhibitions;
import com.app.touchtalent.http.gson.MessageThreadGson;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.ContactExtractor;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.ApiController;
import com.app.touchtalent.volley.DataReciever;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.google.gson.Gson;

public class MainActivity extends FragmentActivity implements OnClickListener, CategoryReceiver, DataReciever {

	private static final String GA_PROPERTY_ID = "UA-27402899-3";
	private static final String CAMPAIGN_MEDIUM_PARAM = "utm_medium";
	private static final String CAMPAIGN_CAMPAIGN_PARAM = "utm_campaign";
	Tracker mTracker;

	private SlidingMenu slidingMenu;
	private Fragment[] fragments = new Fragment[13];
	public static final int HOME_FRAGMENT = 0;
	public static final int POSTPAGE_FRAGMENT = 1;
	public static final int COMMENT_FRAGMENT = 2;
	public static final int PROFILE_FRAGMENT = 3;
	public static final int MAILBOX_FRAGMENT = 4;
	public static final int CONVERSATION_FRAGMENT = 5;
	public static final int EXHIBITION_FRAGMENT = 6;
	public static final int VIEWER_FRAGMENT = 7;
	public static final int EXHIBITIONFEED_FRAGMENT = 8;
	public static final int ABOUTME_FRAGMENT = 9;
	public static final int SEARCH_FRAGMENT = 10;
	public static final int WOW_FRAGMENT = 11;
	public static final int PROFILE_ABOUT_FRAGMENT = 12;
	FragmentManager fm;
	private ExpendableAdapter adapter;
	private DataController controller;
	private View btn_search;
	private EditText edit_box;
	private int currentIndex = -1;
	private List<Category> allcategories;
	private Object previousdata;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTracker = GoogleAnalytics.getInstance(this).getTracker(GA_PROPERTY_ID);
		// AppIterate.onCreate(this);
		ApiController.getInstance(this);
		initializeFragments();
		configureSlidingMenu();
		Intent intent = getIntent();
		handleIntentRecieved(intent);

	}

	/*
	 * @Override public boolean dispatchTouchEvent(MotionEvent event) {
	 * super.dispatchTouchEvent(event);
	 * AppIterate.getGestureDetector().onTouchEvent(event); return true; }
	 */

	private void handleIntentRecieved(Intent intent) {
		AppLog.i("URI of intent " + intent.getDataString());
		Uri uri = intent.getData();

		if (MyPref.getMyTtlToken() != null && !MyPref.getMyTtlToken().equals("")) {
			if (uri != null) {
				AppLog.i("from deep linking " + uri);

				handleDeepLinking(uri);

			} else {
				boolean isNotification = intent.getBooleanExtra("isnotification", false);
				String objecttype = intent.getStringExtra("objecttype");
				Fragment fragment = null;
				AppLog.i("ON create from notification " + isNotification + " " + objecttype);
				if (isNotification) {
					String objectid = intent.getStringExtra("objectid");
					if (objecttype != null) {

						if (objecttype.contains("post")) {
							fragment = getNewFragmentInstance(POSTPAGE_FRAGMENT);
							showThis(fragment);
							((PostPageFragment) fragment).loadFromNotification(objectid);
						} else if (objecttype.contains("profileviewers")) {
							showFragement(objectid, VIEWER_FRAGMENT, true);

						} else if (objecttype.contains("profile")) {
							showFragement(objectid, PROFILE_FRAGMENT, true);

						} else if (objecttype.contains("mailbox")) {
							MessageThreadGson messageobject = new MessageThreadGson();
							messageobject.u_id = objectid;
							messageobject.u_fname = null;
							String exjson = new Gson().toJson(messageobject);
							showFragement(exjson, CONVERSATION_FRAGMENT, true);

						} else if (objecttype.contains("wow")) {
							showFragement(objectid, WOW_FRAGMENT, true);

						} else if (objecttype.contains("exhibition")) {
							Exhibitions exhibitions = new Exhibitions();
							exhibitions.ex_id = objectid;
							showFragement(exhibitions, EXHIBITIONFEED_FRAGMENT, true);

						} else if (objecttype.contains("comment")) {
							AppLog.i("ON create from notification in commnt ");
							showFragement(objectid, COMMENT_FRAGMENT, true);

						} else {
							currentIndex = HOME_FRAGMENT;
							fragment = getNewFragmentInstance(currentIndex);
							showThis(fragment);
						}
					}

				} else {
					currentIndex = HOME_FRAGMENT;
					fragment = getNewFragmentInstance(currentIndex);
					showThis(fragment);
				}

			}
		} else {
			Intent intentl = new Intent(this, SplashActivity.class);
			startActivity(intentl);
			finish();
		}
		// sendContacts();
	}

	private void handleDeepLinking(Uri uri) {
		// TODO Auto-generated method stub
		String host = uri.getHost();
		List<String> pathSegments = uri.getPathSegments();
		mTracker.set(Fields.SCREEN_NAME, host);
		Fragment fragment = null;
		MapBuilder.createAppView().setAll(getReferrerMapFromUri(uri));
		if (host.equals("home")) {
			currentIndex = HOME_FRAGMENT;
			fragment = getNewFragmentInstance(currentIndex);
			showThis(fragment);

		} else if (host.equals("category")) {
			Category cat = null;
			if (pathSegments != null && pathSegments.size() > 0) {
				String id = pathSegments.get(0);
				if (allcategories != null && id != null) {
					// showFragement(allcategories.get(id), addToBackStack)
					for (Category category : allcategories) {

						if (category.hasId(Integer.valueOf(id))) {
							cat = category;
							break;

						}
					}
					AppLog.i("handle " + cat.getCat_id() + cat.getCat_name());
					showFragement(cat, HOME_FRAGMENT, true);

				}

			}
		} else if (host.equals("post")) {
			if (pathSegments != null && pathSegments.size() > 0) {
				String id = pathSegments.get(0);
				if (id != null) {
					currentIndex = POSTPAGE_FRAGMENT;
					fragment = getNewFragmentInstance(currentIndex);
					showThis(fragment);
					((PostPageFragment) fragment).loadFromNotification(id);
				}
			}
		}

		else if (host.equals("profile")) {
			if (pathSegments != null && pathSegments.size() > 0) {
				String id = pathSegments.get(0);
				if (id != null) {
					showFragement(id, PROFILE_FRAGMENT, true);
				}
			} else {
				AppLog.i("My id " + MyPref.getMyUserId());
				showFragement(MyPref.getMyUserId(), PROFILE_FRAGMENT, true);

			}

		}

		else if (host.equals("wow")) {
			if (pathSegments != null && pathSegments.size() > 0) {
				String id = pathSegments.get(0);
				if (id != null) {
					showFragement(id, WOW_FRAGMENT, true);
				}
			}
		}

		else if (host.equals("profileviewers")) {

			showFragement(null, VIEWER_FRAGMENT, true);

		}

		else if (host.equals("mailbox")) {
			if (pathSegments != null && pathSegments.size() > 0) {
				String id = pathSegments.get(0);
				if (id != null) {
					MessageThreadGson messageobject = new MessageThreadGson();
					messageobject.u_id = id;
					messageobject.u_fname = null;
					showFragement(messageobject, CONVERSATION_FRAGMENT, true);
				}
			} else {
				showFragement(MyPref.getMyUserId(), MAILBOX_FRAGMENT, true);

			}

		}

		else if (host.equals("exhibition")) {
			AppLog.i("Host is " + host);
			if (pathSegments != null && pathSegments.size() > 0) {
				String id = pathSegments.get(0);
				if (id != null) {
					Exhibitions exhibitions = new Exhibitions();
					exhibitions.ex_id = id;
					AppLog.i("showing exhibition " + host);
					showFragement(exhibitions, EXHIBITIONFEED_FRAGMENT, true);
				}
			} else {
				AppLog.i("showing exhibition list" + host);
				showFragement("", EXHIBITION_FRAGMENT, true);

			}
		} else if (host.equals("upload")) {

			startUploadActivity();
		}

	}

//	private void sendContacts() {
//		if (!MyPref.getIsContectSent()) {
//			AppLog.i("contact", "not sent");
//			String[] contacts = ContactExtractor.getContacts(this);
//			Log.i("contacts", "here are the contacts" + contacts[0] + " " + contacts[1]);
//			ApiSource.getInstance().sendContactData(contacts[0], contacts[1], new ApiCallResponse() {
//
//				@Override
//				public void onResponseRecieved(boolean status, int errorCode, Object extra) {
//					if (status) {
//						MyPref.setIsContectSent(true);
//					}
//				}
//			});
//		}
//	}

	private void initializeFragments() {
		fm = getSupportFragmentManager();
		fragments[HOME_FRAGMENT] = new HomeScreenFragment();
		fragments[POSTPAGE_FRAGMENT] = new PostPageFragment();
		fragments[COMMENT_FRAGMENT] = new CommentFragment();
		fragments[PROFILE_FRAGMENT] = new ProfileFragment1();
		fragments[MAILBOX_FRAGMENT] = new MailBoxFragment();
		fragments[CONVERSATION_FRAGMENT] = new ConversationFragment();
		fragments[EXHIBITION_FRAGMENT] = new ExhibitionFragment();
		fragments[VIEWER_FRAGMENT] = new ViewerFragment();
		fragments[EXHIBITIONFEED_FRAGMENT] = new ExhibitionFeedFragment();
		fragments[ABOUTME_FRAGMENT] = new ProfileStatsFragment();
		fragments[SEARCH_FRAGMENT] = new SearchFragment();
		fragments[WOW_FRAGMENT] = new WowFragment();
		fragments[PROFILE_ABOUT_FRAGMENT] = new ProfileAboutFragment();

		MyUtilities.startInviteService(this);
	}

	private boolean showThis(Fragment tempFragment) {
		FragmentTransaction transaction = fm.beginTransaction();
		tempFragment.onDestroyView();
		transaction.replace(R.id.fl, tempFragment);
		transaction.addToBackStack(null);
		transaction.commit();
		return true;
	}

	// public void showFragement(Object data, int fragementIndex, boolean
	// addToBackStack) {
	// AppLog.i("ON show fragment show " + fragementIndex);
	// if (fragementIndex == SEARCH_FRAGMENT && currentIndex == SEARCH_FRAGMENT)
	// {
	//
	// showThis(fragments[SEARCH_FRAGMENT]);
	// ((SearchFragment) fragments[SEARCH_FRAGMENT]).reloadView((String) data);
	// }
	// String parcal = null;
	// if (fragementIndex == HOME_FRAGMENT && currentIndex == HOME_FRAGMENT) {
	// showThis(fragments[HOME_FRAGMENT]);
	// Gson gson = new Gson();
	// parcal = gson.toJson(data);
	//
	// ((HomeScreenFragment) fragments[HOME_FRAGMENT]).reloadFragment(parcal);
	// slidingMenu.showContent(true);
	// } else if (fragementIndex == PROFILE_FRAGMENT && currentIndex ==
	// PROFILE_FRAGMENT) {
	// showThis(fragments[PROFILE_FRAGMENT]);
	// if (data instanceof String)
	// parcal = (String) data;
	// else {
	// Gson gson = new Gson();
	// parcal = gson.toJson(data);
	// }
	// ((ProfileFragment1) fragments[PROFILE_FRAGMENT]).reloadFragment(parcal);
	// slidingMenu.showContent(true);
	// } else if (fragementIndex == WOW_FRAGMENT && currentIndex ==
	// WOW_FRAGMENT) {
	// showThis(fragments[WOW_FRAGMENT]);
	//
	// ((WowFragment) fragments[WOW_FRAGMENT]).reloadFragment((String) data);
	// slidingMenu.showContent(true);
	// } else if (fragementIndex == EXHIBITIONFEED_FRAGMENT && currentIndex ==
	// EXHIBITIONFEED_FRAGMENT) {
	// showThis(fragments[EXHIBITIONFEED_FRAGMENT]);
	// ((ExhibitionFeedFragment)
	// fragments[EXHIBITIONFEED_FRAGMENT]).reloadFragment((Exhibitions) data);
	// slidingMenu.showContent(true);
	// } else if (fragementIndex == VIEWER_FRAGMENT && currentIndex ==
	// VIEWER_FRAGMENT) {
	// showThis(fragments[VIEWER_FRAGMENT]);
	//
	// ((ViewerFragment) fragments[VIEWER_FRAGMENT]).reloadFragment();
	// slidingMenu.showContent(true);
	// } else if (fragementIndex == COMMENT_FRAGMENT && currentIndex ==
	// COMMENT_FRAGMENT) {
	// showThis(fragments[COMMENT_FRAGMENT]);
	//
	// ((CommentFragment) fragments[COMMENT_FRAGMENT]).reloadFragment(data +
	// "");
	// slidingMenu.showContent(true);
	// }
	//
	// else {
	// Bundle bundle = null;
	//
	// if (!(data instanceof String)) {
	// Gson gson = new Gson();
	// parcal = gson.toJson(data);
	//
	// } else {
	// parcal = (String) data;
	// }
	// AppLog.i("before fragment " + fragementIndex + " " + currentIndex);
	// if (currentIndex != fragementIndex) {
	// bundle = fragments[fragementIndex].getArguments();
	// if (fragments[fragementIndex].isAdded()) {
	// fm.beginTransaction().remove(fragments[fragementIndex]).commit();
	// }
	// if (bundle == null) {
	// bundle = new Bundle();
	// bundle.putString("data", parcal);
	// fragments[fragementIndex].setArguments(bundle);
	// } else {
	// bundle.putString("data", parcal);
	// fragments[fragementIndex].setArguments(bundle);
	// }
	// currentIndex = fragementIndex;
	// showThis(fragments[currentIndex]);
	// }
	// }
	// }

	public void showFragement(Object data, int fragementIndex, boolean addToBackStack) {
		boolean isdatasame = false;
		if (data == null) {
//			if (previousdata == null) {
//				isdatasame = true;
//			}
		} else if (data != null) {
			/*if (data.equals(previousdata)) {
				isdatasame = true;
			}*/
		}
		if (currentIndex != fragementIndex || isdatasame == false) {

			AppLog.i("all same ");
			Bundle bundle = null;
			String parcal;

			if (!(data instanceof String)) {
				Gson gson = new Gson();
				parcal = gson.toJson(data);

			} else {
				parcal = (String) data;
			}
			bundle = new Bundle();
			bundle.putString("data", parcal);
			AppLog.i("setting data " + parcal);
			Fragment fragmentToShow = getNewFragmentInstance(fragementIndex);
			fragmentToShow.setArguments(bundle);
			currentIndex = fragementIndex;
			previousdata = data;
			showThis(fragmentToShow);
			slidingMenu.showContent();
		} else {
			AppLog.i("condition not satisfied ");
			slidingMenu.showContent();

		}

	}

	 @Override
	 public void onBackPressed() {
	 if (fm.getBackStackEntryCount() > 1) {
	 AppLog.i("onBack " + fm.getBackStackEntryCount());
	 fm.popBackStack();
	 } else {
	 finish();
	 }
	
	 }

	private void configureSlidingMenu() {
		if (slidingMenu == null) {
			slidingMenu = new SlidingMenu(this, SlidingMenu.SLIDING_CONTENT);
			slidingMenu.setMenu(R.layout.sliding_menu);
			slidingMenu.setMode(SlidingMenu.LEFT);
			slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
			slidingMenu.setFadeEnabled(false);
			slidingMenu.setBehindOffset(180);
			btn_search = findViewById(R.id.btn_slidingmenu_search);
			edit_box = (EditText) findViewById(R.id.edit_slidingmenu_search);
			btn_search.setOnClickListener(this);
			ExpandableListView listView = (ExpandableListView) findViewById(R.id.expandableListView1);
			controller = new DataController(this, null);
			allcategories = controller.getAllcategories(this);
			final ArrayList<Group> groups = getAllGroups(allcategories);
			adapter = new ExpendableAdapter(this, groups);
			listView.setAdapter(adapter);

			listView.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
					// TODO Auto-generated method stub
					if (groups.get(groupPosition).children.size() == 0) {
						String selectedGroup = (String) ((TextView) v.findViewById(R.id.tv_group)).getText();

						if (selectedGroup.equals("Home")) {
							showFragement(new Category(-777, "Everything", ""), HOME_FRAGMENT, true);
							slidingMenu.showContent(true);
						} else if (selectedGroup.equals("Me")) {
							MainActivity.this.showFragement(MyPref.getMyUserId() + "", PROFILE_FRAGMENT, true);
							slidingMenu.showContent(true);
						} else if (selectedGroup.contains("Inbox")) {
							currentIndex = MAILBOX_FRAGMENT;
							Fragment fragment = getNewFragmentInstance(currentIndex);
							showThis(fragment);
							slidingMenu.showContent(true);
						} else if (selectedGroup.contains("Upload Artwork")) {
							startUploadActivity();

							slidingMenu.showContent(true);
						} else if (selectedGroup.contains("Exhibitions")) {
							MainActivity.this.showFragement(new Object(), EXHIBITION_FRAGMENT, true);
							slidingMenu.showContent(true);
						} else if (selectedGroup.contains("Logout")) {
							ApiController.getInstance(MainActivity.this).logoutUser(MainActivity.this);
							slidingMenu.showContent(true);
						} else if (selectedGroup.contains("Feedback")) {
							launchMarket();
						}
					}

					return false;
				}
			});
		} else {
			slidingMenu.showMenu();
		}

	}

	private ArrayList<Group> getAllGroups(List<Category> allcategories) {
		final ArrayList<Group> groups = new ArrayList<Group>();
		ArrayList<Category> arrayList = new ArrayList<Category>();
		String[] group_names = { "Upload Artwork", "Home", "Me", "Inbox", "Categories", "Exhibitions", "Feedback", "Logout" };
		for (int i = 0; i < group_names.length; i++) {

			if (i == 4) {

				arrayList = new ArrayList<Category>();
				if (allcategories != null && !allcategories.isEmpty()) {
					Category category = new Category();
					category.setCat_id(-777);
					category.setCat_name("Everything");
					arrayList.add(category);
					arrayList.addAll(allcategories);
				}
				groups.add(new Group(group_names[i], arrayList));

			} else {
				Group group = new Group(group_names[i], new ArrayList<Category>());
				groups.add(group);
			}
		}
		return groups;
	}

	public void onDataReceived(Object object, int callCode, boolean success) {
		if (success) {
			if (callCode == MyUtilities.CODE_LOGOUT) {
				MyPref.logout();
				Toast.makeText(this, "you are successfully logged out", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(this, RegLoginActivity.class);
				startActivity(intent);
				finish();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_home_menu:
			slidingMenu.showMenu();
			break;

		case R.id.btn_postpage_menu:
			onBackPressed();
			break;
		case R.id.btn_comments_menu:
			onBackPressed();
			break;
		case R.id.btn_inbox_menu:
			onBackPressed();
			break;
		case R.id.btn_conversation_menu:
			onBackPressed();
			break;
		case R.id.btn_exhibition_menu:
			onBackPressed();
			break;
		case R.id.btn_profileabout_menu:
			onBackPressed();
			break;
		case R.id.btn_exfeeds_menu:
			onBackPressed();
			break;
		case R.id.btn_followers_menu:
			onBackPressed();
			break;
		case R.id.btn_profileone_menu:
			onBackPressed();
			break;
		case R.id.btn_aboutme_menu:
			onBackPressed();
			break;

		case R.id.btn_home_upload:
			startUploadActivity();

			break;

		case R.id.btn_slidingmenu_search:
			String text = edit_box.getText().toString();
			showFragement(text, SEARCH_FRAGMENT, true);
			slidingMenu.showContent(true);
			InputMethodManager imm = (InputMethodManager) this.getSystemService(Service.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edit_box.getWindowToken(), 0);

			break;

		default:
			break;
		}

	}

	private void startUploadActivity() {
		Intent intent = new Intent(MainActivity.this, UploadHomeActivity.class);
		startActivity(intent);
	}

	@Override
	public void onCategoryreceive(boolean suceess) {
		// TODO Auto-generated method stub
		List<Category> allcategories = controller.getAllcategories(this);
		adapter.setGroups(getAllGroups(allcategories));

	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);

		handleIntentRecieved(intent);

	}

	private void launchMarket() {
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, MyUtilities.FLURRY_KEY);
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);
	}

	Map<String, String> getReferrerMapFromUri(Uri uri) {
		AppLog.i("in reffere ");
		MapBuilder paramMap = new MapBuilder();

		// If no URI, return an empty Map.
		if (uri == null) {
			return paramMap.build();
		}

		// Source is the only required campaign field. No need to continue if
		// not
		// present.
		if (uri.getQueryParameter(CAMPAIGN_CAMPAIGN_PARAM) != null) {

			// MapBuilder.setCampaignParamsFromUrl parses Google Analytics
			// campaign
			// ("UTM") parameters from a string URL into a Map that can be set
			// on
			// the Tracker.
			paramMap.setCampaignParamsFromUrl(uri.toString());
			AppLog.i("params set here " + paramMap.toString() + " " + paramMap.get(CAMPAIGN_MEDIUM_PARAM));
			// If no source parameter, set authority to source and medium to
			// "referral".
		}
		Map<String, String> build = paramMap.build();
		AppLog.i("params set here " + build.toString() + " " + paramMap.get(CAMPAIGN_MEDIUM_PARAM));

		return build;
	}

	public Fragment getNewFragmentInstance(int fragmentIndex) {
		Fragment fragment = null;
		switch (fragmentIndex) {
		case HOME_FRAGMENT:
			fragment = new HomeScreenFragment();
			break;
		case POSTPAGE_FRAGMENT:
			fragment = new PostPageFragment();
			break;
		case COMMENT_FRAGMENT:
			fragment = new CommentFragment();
			break;
		case PROFILE_FRAGMENT:
			fragment = new ProfileFragment1();
			break;
		case MAILBOX_FRAGMENT:
			fragment = new MailBoxFragment();
			break;
		case CONVERSATION_FRAGMENT:
			fragment = new ConversationFragment();
			break;
		case EXHIBITION_FRAGMENT:
			fragment = new ExhibitionFragment();
			break;
		case VIEWER_FRAGMENT:
			fragment = new ViewerFragment();
			break;
		case EXHIBITIONFEED_FRAGMENT:
			fragment = new ExhibitionFeedFragment();
			break;
		case ABOUTME_FRAGMENT:
			fragment = new ProfileStatsFragment();
			break;
		case SEARCH_FRAGMENT:
			fragment = new SearchFragment();
			break;
		case WOW_FRAGMENT:
			fragment = new WowFragment();
			break;
		case PROFILE_ABOUT_FRAGMENT:
			fragment = new ProfileAboutFragment();
			break;

		default:
			break;
		}
		return fragment;

		// return fragments[fragmentIndex];

	}

}
