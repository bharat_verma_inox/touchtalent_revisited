package com.app.touchtalent;




import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;

@SuppressLint("NewApi") public class SampleLiveWallpaperService extends WallpaperService
{
	
	private int mheight;
	private int dstWidth;
@SuppressLint("NewApi") public void onCreate() 
{
	
	
    super.onCreate();

    DisplayMetrics metrics = this.getResources().getDisplayMetrics();
	dstWidth = metrics.widthPixels;
	mheight = metrics.heightPixels;
}

@SuppressLint("NewApi") public void onDestroy() 
{
    super.onDestroy();
}

public Engine onCreateEngine() 
{
    return new CercleEngine();
}

class CercleEngine extends Engine 
{
    public Bitmap image1;
	

    @SuppressLint("NewApi") CercleEngine() 
    {       
    	
        image1 = BitmapFactory.decodeResource(getResources(), R.drawable.bg);
        image1=Bitmap.createScaledBitmap(image1, dstWidth, mheight, true);
    }

    @SuppressLint("NewApi") public void onCreate(SurfaceHolder surfaceHolder) 
    {
        super.onCreate(surfaceHolder);
    }

    public void onOffsetsChanged(float xOffset, float yOffset, float xStep, float yStep, int xPixels, int yPixels) 
    {
        drawFrame();
    }

    @SuppressLint("NewApi") void drawFrame() 
    {
        final SurfaceHolder holder = getSurfaceHolder();

        Canvas c = null;
        try 
        
        
        {
            c = holder.lockCanvas();
            if (c != null) 
            {              
                 c.drawBitmap(image1, 0, 0, null);
            	
            }
        } finally 
        {
            if (c != null) holder.unlockCanvasAndPost(c);
        }
    }
}
}