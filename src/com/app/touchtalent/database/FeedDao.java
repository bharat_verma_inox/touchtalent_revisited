package com.app.touchtalent.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.DaoConfig;
import de.greenrobot.dao.Property;

import com.app.touchtalent.database.Feed;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table Feed.
*/
public class FeedDao extends AbstractDao<Feed, Long> {

    public static final String TABLENAME = "Feed";

    /**
     * Properties of entity Feed.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Post_id = new Property(0, Long.class, "post_id", true, "POST_ID");
        public final static Property Post_slug = new Property(1, String.class, "post_slug", false, "POST_SLUG");
        public final static Property Post_title = new Property(2, String.class, "post_title", false, "POST_TITLE");
        public final static Property User_id = new Property(3, Long.class, "user_id", false, "USER_ID");
        public final static Property Post_description = new Property(4, String.class, "post_description", false, "POST_DESCRIPTION");
        public final static Property Post_keywords = new Property(5, String.class, "post_keywords", false, "POST_KEYWORDS");
        public final static Property Post_mediatype = new Property(6, String.class, "post_mediatype", false, "POST_MEDIATYPE");
        public final static Property Category_id = new Property(7, Integer.class, "category_id", false, "CATEGORY_ID");
        public final static Property Num_comments = new Property(8, Integer.class, "num_comments", false, "NUM_COMMENTS");
        public final static Property Post_views = new Property(9, Integer.class, "post_views", false, "POST_VIEWS");
        public final static Property Post_score = new Property(10, Float.class, "post_score", false, "POST_SCORE");
        public final static Property Post_vlink = new Property(11, String.class, "post_vlink", false, "POST_VLINK");
        public final static Property Is_favourite = new Property(12, String.class, "is_favourite", false, "IS_FAVOURITE");
        public final static Property Is_followed = new Property(13, String.class, "is_followed", false, "IS_FOLLOWED");
        public final static Property Post_timestamp = new Property(14, String.class, "post_timestamp", false, "POST_TIMESTAMP");
        public final static Property User_fname = new Property(15, String.class, "user_fname", false, "USER_FNAME");
        public final static Property User_lname = new Property(16, String.class, "user_lname", false, "USER_LNAME");
        public final static Property User_slug = new Property(17, String.class, "user_slug", false, "USER_SLUG");
        public final static Property Is_file = new Property(18, String.class, "is_file", false, "IS_FILE");
        public final static Property Category_name = new Property(19, String.class, "category_name", false, "CATEGORY_NAME");
        public final static Property Category_slug = new Property(20, String.class, "category_slug", false, "CATEGORY_SLUG");
    };


    public FeedDao(DaoConfig config) {
        super(config);
    }
    
    public FeedDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'Feed' (" + //
                "'POST_ID' INTEGER PRIMARY KEY ," + // 0: post_id
                "'POST_SLUG' TEXT," + // 1: post_slug
                "'POST_TITLE' TEXT," + // 2: post_title
                "'USER_ID' INTEGER," + // 3: user_id
                "'POST_DESCRIPTION' TEXT," + // 4: post_description
                "'POST_KEYWORDS' TEXT," + // 5: post_keywords
                "'POST_MEDIATYPE' TEXT," + // 6: post_mediatype
                "'CATEGORY_ID' INTEGER," + // 7: category_id
                "'NUM_COMMENTS' INTEGER," + // 8: num_comments
                "'POST_VIEWS' INTEGER," + // 9: post_views
                "'POST_SCORE' REAL," + // 10: post_score
                "'POST_VLINK' TEXT," + // 11: post_vlink
                "'IS_FAVOURITE' TEXT," + // 12: is_favourite
                "'IS_FOLLOWED' TEXT," + // 13: is_followed
                "'POST_TIMESTAMP' TEXT," + // 14: post_timestamp
                "'USER_FNAME' TEXT," + // 15: user_fname
                "'USER_LNAME' TEXT," + // 16: user_lname
                "'USER_SLUG' TEXT," + // 17: user_slug
                "'IS_FILE' TEXT," + // 18: is_file
                "'CATEGORY_NAME' TEXT," + // 19: category_name
                "'CATEGORY_SLUG' TEXT);"); // 20: category_slug
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'Feed'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Feed entity) {
        stmt.clearBindings();
 
        Long post_id = entity.getPost_id();
        if (post_id != null) {
            stmt.bindLong(1, post_id);
        }
 
        String post_slug = entity.getPost_slug();
        if (post_slug != null) {
            stmt.bindString(2, post_slug);
        }
 
        String post_title = entity.getPost_title();
        if (post_title != null) {
            stmt.bindString(3, post_title);
        }
 
        Long user_id = entity.getUser_id();
        if (user_id != null) {
            stmt.bindLong(4, user_id);
        }
 
        String post_description = entity.getPost_description();
        if (post_description != null) {
            stmt.bindString(5, post_description);
        }
 
        String post_keywords = entity.getPost_keywords();
        if (post_keywords != null) {
            stmt.bindString(6, post_keywords);
        }
 
        String post_mediatype = entity.getPost_mediatype();
        if (post_mediatype != null) {
            stmt.bindString(7, post_mediatype);
        }
 
        Integer category_id = entity.getCategory_id();
        if (category_id != null) {
            stmt.bindLong(8, category_id);
        }
 
        Integer num_comments = entity.getNum_comments();
        if (num_comments != null) {
            stmt.bindLong(9, num_comments);
        }
 
        Integer post_views = entity.getPost_views();
        if (post_views != null) {
            stmt.bindLong(10, post_views);
        }
 
        Float post_score = entity.getPost_score();
        if (post_score != null) {
            stmt.bindDouble(11, post_score);
        }
 
        String post_vlink = entity.getPost_vlink();
        if (post_vlink != null) {
            stmt.bindString(12, post_vlink);
        }
 
        String is_favourite = entity.getIs_favourite();
        if (is_favourite != null) {
            stmt.bindString(13, is_favourite);
        }
 
        String is_followed = entity.getIs_followed();
        if (is_followed != null) {
            stmt.bindString(14, is_followed);
        }
 
        String post_timestamp = entity.getPost_timestamp();
        if (post_timestamp != null) {
            stmt.bindString(15, post_timestamp);
        }
 
        String user_fname = entity.getUser_fname();
        if (user_fname != null) {
            stmt.bindString(16, user_fname);
        }
 
        String user_lname = entity.getUser_lname();
        if (user_lname != null) {
            stmt.bindString(17, user_lname);
        }
 
        String user_slug = entity.getUser_slug();
        if (user_slug != null) {
            stmt.bindString(18, user_slug);
        }
 
        String is_file = entity.getIs_file();
        if (is_file != null) {
            stmt.bindString(19, is_file);
        }
 
        String category_name = entity.getCategory_name();
        if (category_name != null) {
            stmt.bindString(20, category_name);
        }
 
        String category_slug = entity.getCategory_slug();
        if (category_slug != null) {
            stmt.bindString(21, category_slug);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Feed readEntity(Cursor cursor, int offset) {
        Feed entity = new Feed( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // post_id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // post_slug
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // post_title
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3), // user_id
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // post_description
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // post_keywords
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // post_mediatype
            cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7), // category_id
            cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8), // num_comments
            cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9), // post_views
            cursor.isNull(offset + 10) ? null : cursor.getFloat(offset + 10), // post_score
            cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11), // post_vlink
            cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), // is_favourite
            cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13), // is_followed
            cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14), // post_timestamp
            cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15), // user_fname
            cursor.isNull(offset + 16) ? null : cursor.getString(offset + 16), // user_lname
            cursor.isNull(offset + 17) ? null : cursor.getString(offset + 17), // user_slug
            cursor.isNull(offset + 18) ? null : cursor.getString(offset + 18), // is_file
            cursor.isNull(offset + 19) ? null : cursor.getString(offset + 19), // category_name
            cursor.isNull(offset + 20) ? null : cursor.getString(offset + 20) // category_slug
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Feed entity, int offset) {
        entity.setPost_id(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setPost_slug(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setPost_title(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setUser_id(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
        entity.setPost_description(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setPost_keywords(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setPost_mediatype(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setCategory_id(cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7));
        entity.setNum_comments(cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8));
        entity.setPost_views(cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9));
        entity.setPost_score(cursor.isNull(offset + 10) ? null : cursor.getFloat(offset + 10));
        entity.setPost_vlink(cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11));
        entity.setIs_favourite(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setIs_followed(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13));
        entity.setPost_timestamp(cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14));
        entity.setUser_fname(cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15));
        entity.setUser_lname(cursor.isNull(offset + 16) ? null : cursor.getString(offset + 16));
        entity.setUser_slug(cursor.isNull(offset + 17) ? null : cursor.getString(offset + 17));
        entity.setIs_file(cursor.isNull(offset + 18) ? null : cursor.getString(offset + 18));
        entity.setCategory_name(cursor.isNull(offset + 19) ? null : cursor.getString(offset + 19));
        entity.setCategory_slug(cursor.isNull(offset + 20) ? null : cursor.getString(offset + 20));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Feed entity, long rowId) {
        entity.setPost_id(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Feed entity) {
        if(entity != null) {
            return entity.getPost_id();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
