package com.app.touchtalent.widget;

import android.app.ProgressDialog;
import android.content.Context;

import com.app.touchtalent.R;

public class TtlProgressDialog extends ProgressDialog{

	public TtlProgressDialog(Context context) {
		super(context);
		this.setCancelable(false);
		this.setMessage(getContext().getResources().getString(R.string.ttl_progress_loading));
	}

	public void updateTextandshow(int resId){
		this.show();
	}

	public void updateText(int resId){
		this.setMessage(getContext().getResources().getString(resId));
	}
}
