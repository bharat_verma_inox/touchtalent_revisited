package com.app.touchtalent.widget;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.app.touchtalent.R;
import com.app.touchtalent.adapters.MenuAdapter;
import com.app.touchtalent.database.Category;

public class CatogoryDialog extends Dialog implements OnItemClickListener {

	private ListView mListView = null;
	private MenuAdapter mAdapter;
	private onCatagorySelector catagorySelector;

	public CatogoryDialog(Context context, ArrayList<Category> arrays,
			onCatagorySelector listner) {
		super(context);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_catagory);
		catagorySelector = listner;
		mListView = (ListView) findViewById(R.id.listview);
		mAdapter = new MenuAdapter(context, true);
		mListView.setOnItemClickListener(this);
		mListView.setAdapter(mAdapter);
		mAdapter.addAllandShow(arrays);
		this.setTitle(R.string.hint_selected_cat);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		this.dismiss();
		if (catagorySelector != null) {
			mAdapter.updateSelected(arg2);
			catagorySelector.onSelect((Category) mAdapter.getItem(arg2));
		}

	}

	public interface onCatagorySelector {
		public void onSelect(Category category);
	}

}
