package com.app.touchtalent.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.app.touchtalent.R;

/**
 * @author s711270
 * abstract Class which will create the search view ,with search icon,clear button.
 * implement the onTextSeach() for writing the search logics.
 */
public  class TTLSearchView extends FrameLayout implements TextWatcher{

	private EditText mEtSearchBox = null;
	private ImageButton mBtClear = null;
	private onSearchText mSearchText = null;
	private OnClickListener mClickListener = null;

	public TTLSearchView(Context context) {
		super(context);
		initView(context);
	}

	public TTLSearchView(Context context,AttributeSet set) {
		super(context,set);
		initView(context);
	}

	public TTLSearchView(Context context,AttributeSet set,int defStyle) {
		super(context,set,defStyle);
		initView(context);
	}
	
	public void onSearchClickListener(OnClickListener listener){
		mClickListener = listener;
		mBtClear.setOnClickListener(mClickListener);
	}

	private void initView(Context context){
		mEtSearchBox = new EditText(context);
		FrameLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,Gravity.CENTER_VERTICAL);
		//mEtSearchBox.setCompoundDrawablesWithIntrinsicBounds(R.drawable.search, 0, 0, 0);
		mEtSearchBox.setBackgroundResource(android.R.color.transparent);
		mEtSearchBox.setTextColor(Color.WHITE);
		//params.gravity = Gravity.CENTER_VERTICAL;
		mEtSearchBox.setGravity(Gravity.CENTER|Gravity.LEFT);
		mEtSearchBox.setPadding(pixToDp(4),0,pixToDp(40), pixToDp(4));
		//mEtSearchBox.setCompoundDrawablePadding(pixToDp(3));
		
		mEtSearchBox.setSingleLine();
		this.addView(mEtSearchBox, params);
		mEtSearchBox.addTextChangedListener(this);
		mEtSearchBox.requestFocus();

		mBtClear = new ImageButton(context);
		params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT,Gravity.CENTER_VERTICAL|Gravity.RIGHT);
		params.gravity = Gravity.CENTER_VERTICAL|Gravity.RIGHT;
//		params.rightMargin =  pixToDp(6);
//		int padding  = pixToDp(6);
//		mBtClear.setPadding(padding,padding,padding,padding);
		this.addView(mBtClear, params);
		//mBtClear.setVisibility(View.GONE);
		mBtClear.setBackgroundResource(R.drawable.menu_background_bg);
		mBtClear.setImageResource(R.drawable.ic_search);
	}

	public int pixToDp(int pix){
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pix, getResources().getDisplayMetrics());
	}

	@Override
	public void afterTextChanged(Editable s) {
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		//mBtClear.setVisibility(View.VISIBLE);
		if(mSearchText!=null){
			mSearchText.onTextSeach(s, start, before, count);
		}
	}

//	@Override
//	public void onClick(View v) {
//		mEtSearchBox.setText(null);
//		mBtClear.setVisibility(View.GONE);
//	}

	public void setSearchBoxBackgroundDrawable (Drawable drawable){
		mEtSearchBox.setBackgroundDrawable (drawable);
	}

	public void setSearchBoxBackgroundResource (int res){
		mEtSearchBox.setBackgroundResource(res);
	}

	public void setSearchBoxBackgroundColor(int color){
		mEtSearchBox.setBackgroundColor(color);
	}

	public void setClearDrawable(Bitmap image){
		mBtClear.setImageBitmap(image);
	}

	public void setClearDrawable(Drawable image){
		mBtClear.setImageDrawable(image);
	}

	public void setClearDrawable(int image){
		mBtClear.setImageResource(image);
	}

	public EditText getSearchField(){
		return mEtSearchBox;
	}

	public void setHintText(String hint){
		mEtSearchBox.setHint(hint);
	}

	public void setHintText(int hint){
		mEtSearchBox.setHint(hint);
	}

	public void setTextSize(float points){
		mEtSearchBox.setTextSize(points);
	}

	public void setSearchIcon(int image){
		mEtSearchBox.setCompoundDrawablesWithIntrinsicBounds(image, 0, 0, 0);
	}

	public void setOnSearchListner(onSearchText search){
		mSearchText = search;
	}
}
