package com.app.touchtalent.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;

import com.app.touchtalent.http.multipart.IStreamListener;
import com.app.touchtalent.http.multipart.MyFileBody;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.TtlConst;

public class RequestCreater {

	public static HttpUriRequest Login(String email, String pswd, String reg_id) {
		try {
			String body = MessageFormat.format(
					WebServiceConst.REQUEST_LOGIN_BODY, email, pswd, reg_id,
					TtlConst.device_id);
			HttpPost httpPost = getHttpPostMethod(WebServiceConst.URL_LOGIN,
					body);
			return httpPost;
		} catch (Exception e) {
			return null;
		}
	}

	public static HttpUriRequest SignUp(String email, String pswd,
			String fname, String lname, String reg_id) {
		try {

			String body = MessageFormat.format(
					WebServiceConst.REQUEST_SIGNUP_BODY, email, pswd, fname,
					lname, reg_id, TtlConst.device_id);
			HttpPost httpPost = getHttpPostMethod(WebServiceConst.URL_SIGNUP,
					body);
			return httpPost;
		} catch (Exception e) {
			return null;
		}
	}

	public static HttpUriRequest GPlusLogin(String email, String google_id,
			String google_token, String fname, String lname, String url,
			String gender, String reg_id) {
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
			nameValuePairs.add(new BasicNameValuePair("email",
					email != null ? email : " "));
			nameValuePairs.add(new BasicNameValuePair("google_id",
					google_id != null ? google_id : " "));
			nameValuePairs.add(new BasicNameValuePair("google_token",
					google_token != null ? google_token : " "));
			nameValuePairs.add(new BasicNameValuePair("fname",
					fname != null ? fname : " "));
			nameValuePairs.add(new BasicNameValuePair("lname",
					lname != null ? lname : " "));
			nameValuePairs.add(new BasicNameValuePair("imgurl",
					url != null ? url : " "));
			nameValuePairs.add(new BasicNameValuePair("apn_token",
					reg_id != null ? reg_id : " "));
			nameValuePairs.add(new BasicNameValuePair("gender",
					gender != null ? gender : " "));

			nameValuePairs.add(new BasicNameValuePair("utoken_devicetype",
					TtlConst.device_id));
			HttpPost postMethod = new HttpPost(WebServiceConst.URL_GPLUS_LOGIN);
			postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			return postMethod;
		} catch (Exception e) {
			return null;
		}
	}

	public static HttpUriRequest FBLogin(String email, String fb_id,
			String fb_token, String reg_id) {
		try {
			String body = MessageFormat.format(WebServiceConst.REQUEST_FB_BODY,
					email, fb_id, fb_token, reg_id, TtlConst.device_id);
			HttpPost httpPost = getHttpPostMethod(WebServiceConst.URL_FB_LOGIN,
					body);
			return httpPost;
		} catch (Exception e) {
			return null;
		}
	}

	public static HttpUriRequest SignOut(String id) {
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("u_id", id));
			HttpPost postMethod = new HttpPost(WebServiceConst.URL_LOGOUT);
			postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			return postMethod;
		} catch (Exception e) {
			return null;
		}
	}

	public static HttpUriRequest getPortFolio(String id) {
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("u_id", id));
			HttpPost postMethod = new HttpPost(
					WebServiceConst.URL_GET_PORTFOLIO);
			postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			return postMethod;
		} catch (Exception e) {
			return null;
		}
	}

	public static HttpGet getCatagories() {
		HttpGet httpget = new HttpGet(WebServiceConst.URL_CATAGORIES);
		return httpget;
	}

	public static HttpUriRequest UploadText(String u_id, String category_id,
			String folder_id, String post_title, String post_desc,
			String post_tags, String choice) {
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
			nameValuePairs.add(new BasicNameValuePair("u_id", u_id));
			nameValuePairs.add(new BasicNameValuePair("category_id",
					category_id));
			nameValuePairs.add(new BasicNameValuePair("folder_id", folder_id));
			nameValuePairs
					.add(new BasicNameValuePair("post_title", post_title));
			nameValuePairs.add(new BasicNameValuePair("post_desc", post_desc));
			nameValuePairs.add(new BasicNameValuePair("post_tags", post_tags));
			nameValuePairs
					.add(new BasicNameValuePair("selling_choice", choice));
			HttpPost postMethod = new HttpPost(WebServiceConst.URL_UPLOAD_TEXT);
			postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			return postMethod;
		} catch (Exception e) {
			return null;
		}
	}

	public static MultipartEntity getImageParams(String u_id,
			String category_id, String folder_id, String post_title,
			String post_tags, String file, String choice,
			IStreamListener listner) throws FileNotFoundException,
			UnsupportedEncodingException {
		MultipartEntity entity = new MultipartEntity();
		File f = new File(file);
		MyFileBody fileBody = new MyFileBody(f);
		fileBody.setListener(listner);
		entity.addPart("postimage", fileBody);

		entity.addPart("u_id", new StringBody(u_id));
		entity.addPart("category_id", new StringBody(category_id));
		entity.addPart("folder_id", new StringBody(folder_id));
		entity.addPart("post_title", new StringBody(post_title));
		entity.addPart("post_tags", new StringBody(post_tags));
		entity.addPart("selling_choice", new StringBody(choice));

		return entity;
	}

	public static MultipartEntity getVideoParams(String u_id,
			String category_id, String folder_id, String post_title,
			String post_tags, String file, String choice,
			IStreamListener listner) throws FileNotFoundException,
			UnsupportedEncodingException {
		MultipartEntity entity = new MultipartEntity();
		File f = new File(file);
		MyFileBody fileBody = new MyFileBody(f);
		fileBody.setListener(listner);
		entity.addPart("postvideo", fileBody);

		entity.addPart("u_id", new StringBody(u_id));
		entity.addPart("category_id", new StringBody(category_id));
		entity.addPart("folder_id", new StringBody(folder_id));
		entity.addPart("post_title", new StringBody(post_title));
		entity.addPart("post_tags", new StringBody(post_tags));
		entity.addPart("selling_choice", new StringBody(choice));

		return entity;
	}

	/* helper methods */
	private static HttpPost getHttpPostMethod(String Url, String sendEntity)
			throws UnsupportedEncodingException {
		AppLog.i("MYAPPS", "url : " + Url);
		AppLog.i("MYAPPS", "sendEntity : " + sendEntity);
		HttpPost postMethod = new HttpPost(Url);
		postMethod.setEntity(new StringEntity(sendEntity));

		return postMethod;
	}

}
