package com.app.touchtalent.http.multipart;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.output.CountingOutputStream;
import org.apache.http.entity.mime.content.FileBody;


public class MyFileBody extends FileBody {

    private IStreamListener listener;
    private long ContentLength = 0;
    
    public MyFileBody(File file) {
        super(file);
        ContentLength =  getContentLength();
    }

    @Override
    public void writeTo(OutputStream out) throws IOException {
        CountingOutputStream output = new CountingOutputStream(out) {
            @Override
            protected void afterWrite(int n) throws IOException {
            	  listener.counterChanged((int) (100*getCount()/ContentLength));
            	super.afterWrite(n);
            }
        };
        super.writeTo(output);
    }

    public void setListener(IStreamListener listener) {
        this.listener = listener;
    }

    public IStreamListener getListener() {
        return listener;
    }

}