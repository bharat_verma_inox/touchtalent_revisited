package com.app.touchtalent.http;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import com.app.touchtalent.parser.BaseData;
import com.app.touchtalent.parser.Portfolio;
import com.google.gson.Gson;

public class PortfolioExe extends WebServiceExecuter {

	public PortfolioExe(OnReposnceReceived listner,String token,int code) {
		super(listner,token,code);
	}

	@Override
	public BaseData parse(InputStream in) {
		Gson gson = new Gson();
		Reader reader = new InputStreamReader(in);
		Portfolio response = gson.fromJson(reader, Portfolio.class);
		return response;
	}

}
