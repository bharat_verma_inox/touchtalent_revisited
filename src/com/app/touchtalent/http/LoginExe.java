package com.app.touchtalent.http;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import com.app.touchtalent.parser.BaseData;
import com.app.touchtalent.parser.UserInfo;
import com.google.gson.Gson;

public class LoginExe extends WebServiceExecuter {

	public LoginExe(OnReposnceReceived listner, int code) {
		super(listner, code);
	}

	@Override
	public BaseData parse(InputStream in) {
		// Log.d("MYAPPS","Data : "+read(in));
		Gson gson = new Gson();
		Reader reader = new InputStreamReader(in);
		UserInfo response = gson.fromJson(reader, UserInfo.class);
//		AppLog.i("userid " + response.user.u_id);

		return response;
	}

}
