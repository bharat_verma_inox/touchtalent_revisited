package com.app.touchtalent.http;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.app.touchtalent.utils.AppLog;
import com.app.touchtalent.utils.MyPref;
import com.app.touchtalent.utils.MyUtilities;
import com.app.touchtalent.volley.DataReciever;
import com.app.touchtalent.volley.MyFeedResponseListener;
import com.app.touchtalent.volley.VolleySingleton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;

public class ApiSource {
	public static final String CONTACTS = "contacts";
	public static final String EMAILS = "emails";
	public static final int MY_ACTIVITYS_AUTH_REQUEST_CODE = 5000;

	private static ApiSource instance;

	public static ApiSource getInstance() {

		if (instance == null) {
			instance = new ApiSource();
		}
		return instance;
	}

	public void sendContactData(String contacts, String emails, final DataReciever handler, Context context) {
		// AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		// RequestParams params = new RequestParams();
		//
		// // asyncHttpClient.setTimeout();
		// params.put(CONTACTS, contacts);
		// params.put(EMAILS, emails);
		// String url = WebServiceConst.URL_UPLOAD_CONTACTS;
		// AppLog.i("apisource", "url " + url +" contacts :"+ contacts +
		// " emails:" + emails);
		// asyncHttpClient.addHeader("ttl_key", "xxxx");
		// asyncHttpClient.post(url, params, new AsyncHttpResponseHandler() {
		//
		// public void onSuccess(String content) {
		// AppLog.i("apisource SUCCESS ", content);
		// JSONObject object = null;
		// // try {
		// // object = new JSONObject(content);
		// // if ("success".equals(object.get("status"))) {
		// handler.onResponseRecieved(true, 000, null);
		// return;
		// // }
		// // } catch (JSONException e) {
		// // // TODO Auto-generated catch block
		// // AppLog.i("apisource success exception " + e);
		// // handler.onResponseRecieved(false, 000, null);
		// // e.printStackTrace();
		// // }
		// // handler.onResponseRecieved(false, 000, null);
		//
		// };
		//
		// public void onFailure(Throwable error, String content) {
		// AppLog.i("apisource ", content);
		// handler.onResponseRecieved(false, 000, null);
		//
		// };
		// });
		final HashMap<String, String> params = new HashMap<String, String>();

		// asyncHttpClient.setTimeout();
		params.put(CONTACTS, contacts);
		params.put(EMAILS, emails);
		String url = WebServiceConst.URL_UPLOAD_CONTACTS;
		AppLog.i("apisource", "url " + url + " contacts :" + contacts + " emails:" + emails);
		if (MyUtilities.isInternetConnected(context)) {

			MyFeedResponseListener<String> myListener = new MyFeedResponseListener<String>(handler, MyUtilities.CODE_FOLLOW);
			StringRequest request = new StringRequest(Method.POST, url, myListener, myListener) {

				@Override
				protected Map<String, String> getParams() throws AuthFailureError {

					return params;
				}

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers = new HashMap<String, String>();
					headers.put("ttl_key", "xxxx");
					return headers;
				}
			};
			VolleySingleton.getInstance(context).getRequestQueue().add(request);
		} else {
			MyUtilities.showNetworkToast(context);
			handler.onDataReceived("Connection Error", 404, false);
		}
	}

	public void getAccessTokenForGPlus(final Activity activity, final String accountName, final String scope,
			final ApiCallResponse handler) {

		new AsyncTask<Void, Void, Void>() {
			String accessToken = "";

			@Override
			protected Void doInBackground(Void... params) {
				try {
					accessToken = GoogleAuthUtil.getToken(activity, accountName,
							"oauth2:https://www.googleapis.com/auth/plus.login");
				} catch (UserRecoverableAuthException e) {
					AppLog.i("gplus", "Exception Async getAccessToken");
					activity.startActivityForResult(e.getIntent(), MY_ACTIVITYS_AUTH_REQUEST_CODE);
					handler.onResponseRecieved(false, 101, "");
					e.printStackTrace();
				} catch (IOException e) {

					AppLog.i("gplus", "Exception Async getAccessToken");
					handler.onResponseRecieved(false, 101, "");
					e.printStackTrace();
				} catch (GoogleAuthException e) {
					AppLog.i("gplus", "Exception Async getAccessToken");
					handler.onResponseRecieved(false, 101, "");
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {

				super.onPostExecute(result);
				AppLog.i("gplus", "onPostExecute Async getAccessToken");
				handler.onResponseRecieved(true, 100, accessToken);
			}

		}.execute(null, null, null);
	}
}
