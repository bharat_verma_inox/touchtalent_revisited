package com.app.touchtalent.http;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import com.app.touchtalent.parser.BaseData;
import com.app.touchtalent.parser.Catagories;
import com.google.gson.Gson;

public class CatagoriesExe extends WebServiceExecuter {

	public CatagoriesExe(OnReposnceReceived listner,String token,int code) {
		super(listner,token,code);
	}

	@Override
	public BaseData parse(InputStream in) {
		Gson gson = new Gson();
		Reader reader = new InputStreamReader(in);
		Catagories response = gson.fromJson(reader, Catagories.class);
		return response;
	}

}
