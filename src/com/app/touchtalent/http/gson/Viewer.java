package com.app.touchtalent.http.gson;

public class Viewer {

	public String viewer_uid;
	public String profileview_timestamp;
	public String u_fname;
	public String u_lname;
	public String u_slug;
	public String profile_country;
	public String profile_place;
	public String is_following;

}
