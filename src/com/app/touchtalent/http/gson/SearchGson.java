package com.app.touchtalent.http.gson;

import java.util.ArrayList;

public class SearchGson {

	public String name;
	public String message;
	public String status;
	public ArrayList<PersonGson> users;

}
