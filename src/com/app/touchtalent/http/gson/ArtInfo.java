package com.app.touchtalent.http.gson;

import java.util.ArrayList;

public class ArtInfo {
	public String p_id;
	public String p_timestamp;
	public String p_title;
	public String p_score;
	public String p_slug;
	public String p_views;
	public String p_creator;
	public String p_description;
	public String p_mediatype;
	public String p_vlink;
	public String p_folder;
	public String p_small_w;
	public String p_small_h;
	public String num_comments;
	public String num_broadcasts;
	public String num_favorites;
	public String u_slug;
	public String u_fname;
	public String u_lname;
	public String u_id;
	public String u_type;
	public String sd_name;
	public String cat_id;
	public String cat_name;
	public String cat_slug;
	public String folder_name;
	public String folder_slug;
	public String og_slug;
	public String post_item_price;
	public String commercial_timestamp;
	public String if_favourite;
	public ArrayList<CommentGson> comments;
	public String if_follow;
	public String isfile;
}
