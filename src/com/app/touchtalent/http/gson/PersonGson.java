package com.app.touchtalent.http.gson;

import java.util.ArrayList;

public class PersonGson {
	public String u_id;
	public String u_fname;
	public String u_lname;
	public String u_slug;
	public String u_type;
	public String followers;
	public String profile_place;
	public ArrayList<Categories> interests;
}
