package com.app.touchtalent.http.gson;

import java.util.ArrayList;

public class UserGson {
	public String u_id;
	public String u_fbid;
	public String u_fname;
	public String u_lname;
	public String u_slug;
	public String u_type;
	public String u_tq;
	public String u_followers;
	public String u_followings;
	public String sd_name;
	public String profile_name;
	public String profile_keywords;
	public String profile_bio;
	public String profile_interests;
	public String profile_place;
	public String profile_services;
	public String profile_services_flag;
	public ArrayList<Exhibitions> exhibitions;
	public String being_followed;
	public String pro_skills;
	public Interactions interactions;
	public String pro_achivements;
	public String pro_services;
}
