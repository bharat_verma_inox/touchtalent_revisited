package com.app.touchtalent.http.gson;

import java.util.ArrayList;

public class UserInfoGson {

	public String message;
	public String status;
	public UserGson user;
	public String[] interactions;
	public ArrayList<Country> countries;
}
