package com.app.touchtalent.http.gson;


public class FeedGson {
	public long p_id;
	public String p_timestamp;
	public String p_title;
	public float p_score;
	public String p_slug;
	public int p_views;
	public String p_description;
	public String p_mediatype;
	public String p_vlink;
	public String p_folder;
	public String p_small_w;
	public String p_small_h;
	public int num_comments;
	public String num_broadcasts;
	public String num_favorites;
	public String u_slug;
	public String u_fname;
	public String u_lname;
	public long u_id;
	public String u_type;
	public String sd_name;
	public int cat_id;
	public String cat_name;
	public String cat_slug;
	public String folder_name;
	public String folder_slug;
	public String og_slug;
	public String post_item_price;
	public String commercial_timestamp;
	public String if_favourite;
//	public ArrayList<CommentGson> comments;
	public String if_follow;
	public String isfile;

	public String returnAll() {
		return " # " + p_id + " # " + p_timestamp + " # " + p_title + " # "
				+ p_score + " # " + p_slug + " # " + p_views + " # "
				+ p_description + " # " + p_mediatype + " # " + p_vlink + " # "
				+ p_folder + " # " + p_small_w + " # " + p_small_h + " # "
				+ num_comments + " # " + num_broadcasts + " # " + num_favorites
				+ " # " + u_slug + " # " + u_fname + " # " + u_lname + " # "
				+ u_id + " # " + u_type + " # " + sd_name + " # " + cat_id
				+ " # " + cat_name + " # " + cat_slug + " # " + folder_name
				+ " # " + folder_slug + " # " + og_slug + " # "
				+ post_item_price + " # " + commercial_timestamp + " # "
				+ if_favourite + " # " + if_follow;
	}
}
