package com.app.touchtalent.http.gson;

public class LoginGson {
	UserLoginDetails user;
	String status;
	String message;

	public UserLoginDetails getUser() {
		return user;
	}

	public void setUser(UserLoginDetails user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
