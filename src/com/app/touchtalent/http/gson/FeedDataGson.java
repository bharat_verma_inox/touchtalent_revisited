package com.app.touchtalent.http.gson;

import java.util.ArrayList;

public class FeedDataGson {
	public String filter;
	public String catid;
	public String param;
	public String recent;
	public String ranked;
	public String follow;
	public String shuffle;
	public String folderid;
	public String eid;
	public String page;
	public String last_score;
	public String displayed;
	public String slideval;
	public String lpid;
	public String uid;
	public ArrayList<FeedGson> feed;
	public String status;

	public String returnAll() {
		return " # " + filter + " # " + catid + " # " + param + " # " + recent
				+ " # " + ranked + " # " + follow + " # " + shuffle + " # "
				+ folderid + " # " + eid + " # " + page + " # " + last_score
				+ " # "  + " # " + slideval + " # " + lpid;
	}
}
