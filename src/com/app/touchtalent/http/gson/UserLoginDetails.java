package com.app.touchtalent.http.gson;

public class UserLoginDetails {

	String u_id;
	String u_fname;
	String u_lname;
	String u_email;
	String u_fbid;
	String u_fbmail;
	String u_gplusid;
	String u_gmail;
	String u_status;
	String u_slug;
	String u_conn;
	String u_type;
	String u_token;

	public String getU_id() {
		return u_id;
	}

	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	public String getU_fname() {
		return u_fname;
	}

	public void setU_fname(String u_fname) {
		this.u_fname = u_fname;
	}

	public String getU_lname() {
		return u_lname;
	}

	public void setU_lname(String u_lname) {
		this.u_lname = u_lname;
	}

	public String getU_email() {
		return u_email;
	}

	public void setU_email(String u_email) {
		this.u_email = u_email;
	}

	public String getU_fbid() {
		return u_fbid;
	}

	public void setU_fbid(String u_fbid) {
		this.u_fbid = u_fbid;
	}

	public String getU_fbmail() {
		return u_fbmail;
	}

	public void setU_fbmail(String u_fbmail) {
		this.u_fbmail = u_fbmail;
	}

	public String getU_gplusid() {
		return u_gplusid;
	}

	public void setU_gplusid(String u_gplusid) {
		this.u_gplusid = u_gplusid;
	}

	public String getU_gmail() {
		return u_gmail;
	}

	public void setU_gmail(String u_gmail) {
		this.u_gmail = u_gmail;
	}

	public String getU_status() {
		return u_status;
	}

	public void setU_status(String u_status) {
		this.u_status = u_status;
	}

	public String getU_slug() {
		return u_slug;
	}

	public void setU_slug(String u_slug) {
		this.u_slug = u_slug;
	}

	public String getU_conn() {
		return u_conn;
	}

	public void setU_conn(String u_conn) {
		this.u_conn = u_conn;
	}

	public String getU_type() {
		return u_type;
	}

	public void setU_type(String u_type) {
		this.u_type = u_type;
	}

	public String getU_token() {
		return u_token;
	}

	public void setU_token(String u_token) {
		this.u_token = u_token;
	}

	public String returnAll() {

		return "ucon "+u_conn + "u_email " + u_email + " u_fbid " + u_fbid + " u_fbmail " + u_fbmail
				+ " u_fname " + u_fname + "  u_gmail " + u_gmail + "  u_gplusid " + u_gplusid + " # "
				+ u_id + " # " + u_lname + " # " + u_slug + " # " + u_status
				+ " u_token " + u_token + " u_type " + u_type;
	}

}
