package com.app.touchtalent.http.gson;

public class CommentGson {

	public long comment_id;
	public String comment_pid;
	public String comment_text;
	public int comment_smiley;
	public long comment_uid;
	public String u_slug;
	public String u_fname;
	public String u_lname;
	public String u_type;
	public String sd_name;
	public String img;
}
