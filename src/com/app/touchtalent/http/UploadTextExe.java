package com.app.touchtalent.http;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import com.app.touchtalent.parser.BaseData;
import com.app.touchtalent.parser.UploadData;
import com.google.gson.Gson;

public class UploadTextExe extends WebServiceExecuter {

	public UploadTextExe(OnReposnceReceived listner,String token ,int code) {
		super(listner,token,code);
	}

	@Override
	public BaseData parse(InputStream in) {
		try{
			Gson gson = new Gson();
			Reader reader = new InputStreamReader(in);
			BaseData response = gson.fromJson(reader, UploadData.class);
			return response;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
