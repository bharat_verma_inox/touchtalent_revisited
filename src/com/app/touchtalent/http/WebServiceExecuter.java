package com.app.touchtalent.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.os.AsyncTask;

import com.app.touchtalent.parser.BaseData;

public abstract class WebServiceExecuter extends AsyncTask<HttpUriRequest, Void, Boolean> {

	protected OnReposnceReceived mReponseLister = null;
	protected String mErrorResponse = null;
	protected final static int STATUS = 200;
	private InputStream inStream = null;
	private BaseData mResponse = null;
	private String tocken = null;
	private int mRequestCode = 0;

	public WebServiceExecuter(OnReposnceReceived listner, int code) {
		mReponseLister = listner;
		mRequestCode = code;
	}

	public WebServiceExecuter(OnReposnceReceived listner, String token, int code) {
		this.tocken = token;
		mReponseLister = listner;
		mRequestCode = code;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Boolean doInBackground(HttpUriRequest... params) {
		try {
			HttpUriRequest uriRequest = params[0];

			if (uriRequest != null) {
				uriRequest.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				uriRequest.addHeader("Accept", "*/*");
				uriRequest.addHeader("ttl_key", "xxxx");
				uriRequest.getParams().setBooleanParameter("http.protocol.expect-continue", false);
				if (this.tocken != null)
					uriRequest.addHeader("ttl_token", this.tocken);
				// uriRequest.addHeader("Cookie","beta-sessionid=df13db1ade1c1fef262cf985bbc8f99c");
			}

			DefaultHttpClient client = new DefaultHttpClient();

			final HttpParams httpParams = client.getParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
			HttpResponse httpResponse = client.execute(uriRequest);
			int code = httpResponse.getStatusLine().getStatusCode();

			if (STATUS == code) {
				HttpEntity resEntity = httpResponse.getEntity();
				inStream = resEntity.getContent();
				mResponse = parse(inStream);
				// AppLog.i(" user id "+ mResponse.message)
				close();
				return true;
			} else {
				mErrorResponse = "Connection Error";
			}
		} catch (IOException ioEx) {
			ioEx.printStackTrace();
			mErrorResponse = "Sorry !!! " + ioEx.getMessage();
		} catch (SecurityException secEx) {
			secEx.printStackTrace();
			mErrorResponse = "Security Fail: " + secEx.getMessage();
		} catch (IllegalArgumentException argEx) {
			argEx.printStackTrace();
			mErrorResponse = " " + argEx.getMessage();
		} catch (Exception ex) {

			ex.printStackTrace();
			mErrorResponse = "An unkown error occured " + ex.getMessage();
		}
		return false;
	}

	protected String getStr(InputStream inStream) {
		BufferedReader r = new BufferedReader(new InputStreamReader(inStream));
		StringBuilder total = new StringBuilder();
		String line;
		try {
			while ((line = r.readLine()) != null) {
				total.append(line);
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
		return total.toString();
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if (mReponseLister != null && result && mResponse != null && !mResponse.status.equalsIgnoreCase("ERROR")) {
			mReponseLister.onRequestComplete(mResponse, mRequestCode);
		} else if (mReponseLister != null) {
			try {
				mReponseLister.onRequestFailed(result ? mResponse.message : mErrorResponse, mRequestCode);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected String read(InputStream in) {
		try {
			StringBuilder sb = new StringBuilder();
			BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
			for (String line = r.readLine(); line != null; line = r.readLine()) {
				sb.append(line);
			}
			in.close();
			return sb.toString();
		} catch (IOException e) {
			return "";
		}
	}

	protected void close() {
		try {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
		}
	}

	public abstract BaseData parse(InputStream in);
}
