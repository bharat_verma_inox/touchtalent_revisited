package com.app.touchtalent.http;

import com.app.touchtalent.utils.TtlConst;

public class WebServiceConst {

	public static final String BASE_URL = TtlConst.API_URL;
	public static final String URL_UPLOAD_CONTACTS = BASE_URL
			+ "/user/phonecontacts";
	public static final String URL_LOGIN = BASE_URL + "/user/emaillogin";
	public static final String URL_SIGNUP = BASE_URL + "/user/signup";
	public static final String URL_FB_LOGIN = BASE_URL + "/user/facebooklogin";
	public static final String URL_GPLUS_LOGIN = BASE_URL + "/user/googlelogin";
	public static final String URL_LOGOUT = BASE_URL + "/user/logout";
	public static final String URL_UPLOAD_TEXT = BASE_URL + "/post/createtext";
	public static final String URL_UPLOAD_IMAGE = BASE_URL
			+ "/post/createimage";
	public static final String URL_UPLOAD_VIDEO = BASE_URL
			+ "/post/createvideo";

	public static final String URL_CATAGORIES = BASE_URL + "/art/getcategories";
	public static final String URL_GET_PORTFOLIO = BASE_URL
			+ "/user/getportfolios";

	// /REQUESST
	public static final String REQUEST_LOGIN_BODY = "email={0}&password={1}&apn_token={2}&apn_status={3}&utoken_devicetype={4}";
	public static final String REQUEST_SIGNUP_BODY = "email={0}&password={1}&fname={2}&lname={3}&apn_token={4}&apn_status={5}&utoken_devicetype={6}";
	public static final String REQUEST_FB_BODY = "email={0}&facebook_id={1}&facebook_token={2}&apn_token={3}&apn_status={4}&utoken_devicetype={5}";
}
