package com.app.touchtalent.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.text.format.DateUtils;

public class CurrentTimeAndDate {
	/**
	 * 
	 * @return system current Time in AM/PM
	 */
	public static String getCurrentTime() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		String formated_time = df.format(c.getTime());
		formated_time = getAmPm(formated_time);
		return formated_time;

	}

	/**
	 * 
	 * @param time
	 * @return TimewithAmPm
	 */
	private static String getAmPm(String time) {
		String[] split = time.split(":");
		int hour = Integer.parseInt(split[0]);
		int minute = Integer.parseInt(split[1]);
		String result = null;
		if (hour > 12) {

			result = (hour - 12) + ":" + (String.valueOf(minute) + " PM");
		}
		if (hour == 12) {
			result = "12" + ":" + (String.valueOf(minute) + " PM");
		}
		if (hour < 12) {
			result = String.valueOf(hour) + ":" + (String.valueOf(minute) + " AM");
		}
		return result;
	}

	/**
	 * 
	 * @return SystemCurrentDate
	 */
	public static String getCurrentDate() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String formated_date = df.format(c.getTime());
		return formated_date;

	}

	/**
	 * 
	 * @param previousDate
	 * @param previousTime
	 * @return No. of hours
	 */
	public static int calculateHours(String previousDate, String previousTime) {

		int calculated_hours = 0;
		try {
			String format = "dd/MM/yyyy hh:mm a";
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			AppLog.i("current date n time " + getCurrentDate() + "  " + getCurrentTime());
			Date previousDateObj = sdf.parse(previousDate + " " + previousTime);
			Date currentDateObj = sdf.parse(getCurrentDate() + " " + getCurrentTime());
			long diff = currentDateObj.getTime() - previousDateObj.getTime();
			// Log.i("diff : " + diff);
			calculated_hours = (int) (diff / ((double) 1000 * 60 * 60));

		} catch (ParseException e) {
			// Log.i(e.getMessage());
			// e.printStackTrace();
			AppLog.i("Parse Exception");
			calculated_hours = 0;
		}
		return calculated_hours;

	}

	/**
	 * 
	 * @return CurrentTimeinMiliSecond
	 */
	public static long getCurrentTimeinMiliSecond() {

		long currentTimeMillis = System.currentTimeMillis();
		return currentTimeMillis;
	}

}
