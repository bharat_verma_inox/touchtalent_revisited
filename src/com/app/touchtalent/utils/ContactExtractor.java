package com.app.touchtalent.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Base64;

import com.google.gson.Gson;

public class ContactExtractor {
	// public static String SECRET_KEY = "00NsBaloXmb+JWrwlhRZbq5dTKj3mH+L";
	private static SecretKeySpec secretKey;

	public static String[] getContacts(Context context) {
		AppLog.i("contacts", "getting them");
		String[] data = new String[2];
		String contactJson = "";
		String emailJson = "";
		ArrayList<String> contactList = new ArrayList<String>();
		ArrayList<String> emailList = new ArrayList<String>();
		ContentResolver cr = context.getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					// Log.i("contacts", "name : " + name + ", ID : " + id);

					// get the phone number
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { id }, null);
					while (pCur.moveToNext()) {
						String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						contactList.add(phone);
						// Log.i("contacts", "name : " + name + ", ID : " + id +
						// " phone : " + phone);
					}
					pCur.close();

					Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
							ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[] { id }, null);

					while (emailCur.moveToNext()) {
						// This would allow you get several email addresses
						// if the email addresses were stored in an array
						String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
						emailList.add(email);
					}
					emailCur.close();

				}
			}

		}
		Gson gson = new Gson();
		contactJson = gson.toJson(contactList);
		// Log.i("contacts", contactJson);
		// data[0] = contactJson;
		emailJson = gson.toJson(emailList);
		// Log.i("email", emailJson);
		// data[1] = emailJson;

		data[0] = encrypt(contactJson.trim());
		data[1] = encrypt(emailJson.trim());
		// Log.i("contacts", data[0] + " " + data[1]);
		return data;

	}

	public static String encrypt(String strToEncrypt) {
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

			cipher.init(Cipher.ENCRYPT_MODE, setKey("touchtalent12345"));

			// AppLog.i("dipesh 3",
			// Base64.encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")),
			// Base64.CRLF));
			// AppLog.i("dipesh 4",
			// Base64.encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")),
			// Base64.NO_WRAP));
			// AppLog.i("dipesh 5",
			// Base64.encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")),
			// Base64.URL_SAFE));
			// AppLog.i("dipesh 6",
			// Base64.encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")),
			// Base64.NO_CLOSE));
			return Base64.encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")), Base64.DEFAULT);
		} catch (Exception e) {

			// System.out.println("Error while encrypting: " + e.toString());
			return null;
		}
		// return
	}

	public static SecretKeySpec setKey(String myKey) {

		try {
			byte[] key = myKey.getBytes("UTF-8");

			System.out.println(new String(key, "UTF-8"));
			secretKey = new SecretKeySpec(key, "AES");

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		AppLog.i("encode", "key generated");
		return secretKey;

	}

	public static String getContactDetails(Context context) {
		// ArrayList<String> emlRecs = new ArrayList<String>();
		// HashSet<String> emlRecsHS = new HashSet<String>();
		ArrayList<String> contactList = new ArrayList<String>();
		ContentResolver cr = context.getContentResolver();
		String[] PROJECTION = new String[] { ContactsContract.CommonDataKinds.Phone.NUMBER,
				ContactsContract.Contacts.DISPLAY_NAME };

		Cursor cur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION, null, null, null);
		if (cur.moveToFirst()) {
			do {
				// names comes in hand sometimes
				// AppLog.i("data retrived " + cur.getString(0) + " 2- " +
				// cur.getString(1) + " 3- " + " 4- ");
				contactList.add(cur.getString(0));
			} while (cur.moveToNext());
		}
		cur.close();
		// return contactList;
		Gson gson = new Gson();
		String contactJson = gson.toJson(contactList);
		String contactencrypt = encrypt(contactJson);
		return contactencrypt;

	}

	public static String getEmailDetails(Context context) {
		ArrayList<String> contactList = new ArrayList<String>();
		ContentResolver cr = context.getContentResolver();
		String[] PROJECTION = new String[] { ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.CommonDataKinds.Email.DATA, };

		Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, null, null, null);
		if (cur.moveToFirst()) {
			do {
				// names comes in hand sometimes
				String name = cur.getString(1);
				contactList.add(name);

				// keep unique only

			} while (cur.moveToNext());
		}

		cur.close();
		Gson gson = new Gson();
		String contactJson = gson.toJson(contactList);
		String contactencrypt = encrypt(contactJson);
		return contactencrypt;

	}

	public static String[] getContacts2(Context context) {
		Gson gson = new Gson();
		String[] data = new String[2];
		String contactJson = gson.toJson(getContactDetails(context));
		// Log.i("contacts", contactJson);
		// data[0] = contactJson;
		String emailJson = gson.toJson(getEmailDetails(context));
		// Log.i("email", emailJson);
		// data[1] = emailJson;
		AppLog.i("contact gson " + contactJson);
		AppLog.i("Email json " + emailJson);
		data[0] = encrypt(contactJson.trim());
		data[1] = encrypt(emailJson.trim());
		// Log.i("contacts", data[0] + " " + data[1]);
		return data;

	}

}
