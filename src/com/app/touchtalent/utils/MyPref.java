package com.app.touchtalent.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class MyPref {
	private static final String TODAY_INVITATION_COUNT = "today_invitation_count";
	private static final String LAST_FRIEND_POSTION = "last_friend_postion";
	private static final String FRIEND_HASHMAP = "friend_hashmap";
	private static final String RECENT_LASTSCORE = "recent_lastscore";
	private static final String POPULAR_LPID = "popular_lpid";
	public static final int INVALIDATE_PID = -100;
	public static final float INVALIDATED_FLOAT = -120.09f;
	public static Context context;
	private static SharedPreferences mPrefs;
	private static Editor editor;
	public static final String PREFERENCES = "TouchTalent";

	public final static String REGISTERED = "registered";
	public final static String USER_EMAIL = "user_email";
	public final static String GCM_ID = "registration_id";
	public final static String USER_FB_ID = "user_fb_id";
	public final static String USER_FB_TOKEN = "user_fb_token";
	public final static String USER_ACCESS_TOKEN = "user_access_token";
	public final static String APP_VERSION = "app_version";
	private static final String PROPERTY_REG_STATUS = "reg_status";
	private static final String RECIEVED_URL = "url";
	private static final String APP_STATUS = "app_status";// open or closed
	private static final String USER_ID = "u_id";

	public static final String TYPE = "login_type";
	public static final String u_fname = "u_fname";
	public static final String u_lname = "u_lname";
	public static final String u_email = "u_email";
	public static final String u_fbid = "u_fbid";
	public static final String u_fbmail = "u_fbmail";
	public static final String u_gplusid = "u_gmail";
	public static final String u_gmail = "";
	public static final String u_status = "u_status";
	public static final String u_slug = "u_slug";
	public static final String u_conn = "u_conn";
	public static final String TTL_TOKEN = "u_token";
	private static final String RECENT_POSTID = "recent_pid";
	private static final String POPULAR_LASTSCORE = "popular_lscore";
	private static final String TRENDING_LSCORE = "trending_lastscore";
	private static final String TRENDING_STARTSCORE = "trending_startscore";
	private static final String TRENDING_LPID = "trending_lpid";
	private static final String LAST_DATE_SAVED = "last_date_saved";
	private static final String IS_CONTACTS_SENT = "is_contect_sent";
	private static final String ALL_REQ_SENT = "all_req_sent";

	public static void init(Context context) {
		if (mPrefs == null) {
			MyPref.context = context;
			mPrefs = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
			editor = mPrefs.edit();
		}
	}

	public static void setGcmRegistartionId(String regid) {

		editor.putString(GCM_ID, regid);
		editor.commit();
	}

	public static String getGcmRegistrationId() {
		return mPrefs.getString(GCM_ID, "");
	}

	public static void setUserEmail(String email) {
		editor.putString(USER_EMAIL, email);
		editor.commit();
	}

	public static String getUserEmail() {
		return mPrefs.getString(USER_EMAIL, null);
	}

	public static void setUserFbId(String fb_id) {
		editor.putString(USER_FB_ID, fb_id);
		editor.commit();
	}

	public static String getUserFbId() {
		return mPrefs.getString(USER_FB_ID, null);
	}

	public static void setIsContectSent(boolean issent) {
		editor.putBoolean(IS_CONTACTS_SENT, issent);
		editor.commit();
	}

	public static boolean getIsContectSent() {
		return mPrefs.getBoolean(IS_CONTACTS_SENT, false);
	}

	public static void setUserAccessToken(String access_token) {
		editor.putString(USER_ACCESS_TOKEN, access_token);
		editor.commit();
	}

	public static String getUserAccessToken() {
		return mPrefs.getString(USER_ACCESS_TOKEN, null);
	}

	public static void setRegistrationStatus(boolean registered) {
		editor.putBoolean(REGISTERED, registered);
		editor.commit();
	}

	public static boolean getRegistrationStatus() {
		return mPrefs.getBoolean(REGISTERED, false);
	}

	public static void setAppVersion(int app_version) {
		editor.putInt(APP_VERSION, app_version);
		editor.commit();
	}

	public static int getAppVersion() {
		return mPrefs.getInt(APP_VERSION, Integer.MIN_VALUE);
	}

	public static void setGcmRegistrationStatus(boolean status) {
		editor.putBoolean(PROPERTY_REG_STATUS, status);
		editor.commit();
	}

	public static boolean getGcmRegistrationStatus() {
		return mPrefs.getBoolean(PROPERTY_REG_STATUS, false);
	}

	public static void setNotificationUrl(String url) {
		editor.putString(RECIEVED_URL, url);
		editor.commit();
	}

	public static String getNotificationUrl() {
		return mPrefs.getString(RECIEVED_URL, null);
	}

	public static void setAppStatus(boolean status) {
		editor.putBoolean(APP_STATUS, status);
		editor.commit();
	}

	public static boolean getAppStatus() {
		return mPrefs.getBoolean(APP_STATUS, false);
	}

	public static String getMyUserId() {
		return mPrefs.getString(USER_ID, null);
	}

	public static String getMyTtlToken() {
		return mPrefs.getString(TTL_TOKEN, "");
	}

	public static void setMyTtlToken(String ttl_token) {
		editor.putString(TTL_TOKEN, ttl_token);
		editor.commit();
	}

	public static void setMyUserid(String user_id) {
		editor.putString(USER_ID, user_id);
		editor.commit();
	}

	public static long getRecentPostId() {
		return mPrefs.getLong(RECENT_POSTID, INVALIDATE_PID);
	}

	public static void setRecentDetails(Float p_lscore, Long lpid) {
		editor.putFloat(RECENT_LASTSCORE, p_lscore);
		editor.putLong(RECENT_POSTID, lpid);
		editor.commit();
	}

	public static long getTrendingLastPostId() {
		return mPrefs.getLong(TRENDING_LPID, INVALIDATE_PID);
	}

	public static float getTrendingLastScore() {
		return mPrefs.getFloat(TRENDING_LSCORE, INVALIDATED_FLOAT);
	}

	public static float getTrendingFirstScore() {
		return mPrefs.getFloat(TRENDING_STARTSCORE, INVALIDATED_FLOAT);
	}

	public static void setTrendingDetails(Float p_firstscore, Long lpid, Float p_lscore) {

		editor.putLong(TRENDING_LPID, lpid);
		editor.putFloat(TRENDING_LSCORE, p_lscore);
		if (getTrendingFirstScore() == INVALIDATED_FLOAT) {
			editor.putFloat(TRENDING_STARTSCORE, p_firstscore);
		}
		editor.commit();
	}

	public static float getPopularLastScore() {
		return mPrefs.getFloat(POPULAR_LASTSCORE, INVALIDATED_FLOAT);
	}

	public static void setPolpularDetails(Float p_lscore, Long lpid) {
		editor.putFloat(POPULAR_LASTSCORE, p_lscore);
		editor.putLong(POPULAR_LPID, lpid);
		editor.commit();
	}

	public static long getPopularLastPostId() {
		return mPrefs.getLong(POPULAR_LPID, INVALIDATE_PID);
	}

	public static String getLastDate() {
		return mPrefs.getString(LAST_DATE_SAVED, null);
	}

	public static void setLastDate(String dateString) {
		editor.putString(LAST_DATE_SAVED, dateString);
		editor.commit();
	}

	public static void resetScoreAndIds() {
		editor.putFloat(POPULAR_LASTSCORE, INVALIDATED_FLOAT);
		editor.putLong(POPULAR_LPID, INVALIDATE_PID);
		editor.putFloat(RECENT_LASTSCORE, INVALIDATED_FLOAT);
		editor.putLong(RECENT_POSTID, INVALIDATE_PID);
		editor.putFloat(TRENDING_LSCORE, INVALIDATED_FLOAT);
		editor.putLong(TRENDING_LPID, INVALIDATE_PID);
		editor.putFloat(TRENDING_STARTSCORE, INVALIDATED_FLOAT);
		editor.commit();
	}

	public static void logout() {
		if (editor != null) {
			editor.remove(USER_ACCESS_TOKEN);
			editor.remove(USER_EMAIL);
			editor.remove(USER_FB_ID);
			editor.remove(USER_ID);
			editor.remove(TTL_TOKEN);
			editor.commit();
			resetScoreAndIds();

		}
	}

	public static boolean getInvitationSent() {
		return mPrefs.getBoolean(ALL_REQ_SENT, false);
	}

	public static void setInvitationSent(Boolean dateString) {
		editor.putBoolean(ALL_REQ_SENT, dateString);
		editor.commit();
	}

	public static String getMyFbToken() {
		// TODO Auto-generated method stub
		return mPrefs.getString(USER_FB_TOKEN, null);

	}

	public static void setMyFbToken(String dateString) {
		editor.putString(USER_FB_TOKEN, dateString);
		editor.commit();
	}

	public static String getFriendHastMap() {

		return mPrefs.getString(FRIEND_HASHMAP, null);
	}

	public static void setLastFriendPostion(int hashmap) {
		editor.putInt(LAST_FRIEND_POSTION, hashmap);
		editor.commit();
	}

	public static int getLastFriendPostition() {

		return mPrefs.getInt(LAST_FRIEND_POSTION, 0);

	}

	public static void setFriendHashMap(String hashmap) {
		editor.putString(FRIEND_HASHMAP, hashmap);
		editor.commit();
	}

	public static int getTodaysCount() {

		return mPrefs.getInt(TODAY_INVITATION_COUNT, 0);

	}

	public static void setTodaysCount(int count) {
		editor.putInt(TODAY_INVITATION_COUNT, count);
		editor.commit();
	}

}
