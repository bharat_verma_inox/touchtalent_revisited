package com.app.touchtalent.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Picture;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebView.PictureListener;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.app.touchtalent.R;

public class LinkedInShare extends Dialog {

	ProgressDialog progress;
	String shareURL;

	ImageView close;

	public LinkedInShare(Context context, ProgressDialog progress,
			String shareURL) {
		super(context);
		this.progress = progress;
		this.shareURL = shareURL;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		requestWindowFeature(Window.FEATURE_NO_TITLE);// must call before super.
		super.onCreate(savedInstanceState);

		setContentView(R.layout.ln_dialog);

		close = (ImageView) findViewById(R.id.close_button);

		WebView mWebView = (WebView) findViewById(R.id.webkitWebView1);
		mWebView.getSettings().setJavaScriptEnabled(true);

		mWebView.loadUrl(shareURL);
		mWebView.setWebViewClient(new HelloWebViewClient());

		mWebView.setPictureListener(new PictureListener() {
			@Override
			public void onNewPicture(WebView view, Picture picture) {
				if (progress != null && progress.isShowing()) {
					progress.dismiss();
				}

			}
		});

		close.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				LinkedInShare.this.cancel();

			}
		});

	}

	class HelloWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			Log.i("url", url);

			view.loadUrl(url);

			return true;
		}
	}
}
