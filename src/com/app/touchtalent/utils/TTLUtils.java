package com.app.touchtalent.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.Base64;
import android.view.Gravity;
import android.widget.Toast;

public class TTLUtils {

	private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";

	public static void printFBHashKey(Context context){
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(
					"com.app.touchtalent", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures){
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				AppLog.i("MYAPPS", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		}
	}

	public static String getCatagoryUrl(String path){
		if(path!=null && path.equalsIgnoreCase("Everything")){
			return TtlConst.MENU_URL;
		}else{
			return MessageFormat.format(TtlConst.MENU_CAT_URL,path);
		}
	}

	public static String getSearchUrl(String path){
		return MessageFormat.format(TtlConst.SEARCH,path);
	}

	public static String getUploadUrl(String path){
		return MessageFormat.format(TtlConst.UPLOAD_REDIRECT,path);
	}


	public static void showToast(Context context,int messageKey){
		showToast(context,context.getResources().getString(messageKey));
	}

	public static void showToast(Context context,String message){
		if(context!=null){
			Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);;//new Toast(context);
			if(toast!=null){
				toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
			}
		}
	}

	public static  boolean validateEmail(String email){
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher m = p.matcher(email);
		boolean matchFound = m.matches();
		if (matchFound)
			return true;
		else
			return false;
	}


	public static Uri getTempUri() {
		return Uri.fromFile(getTempFile());
	}

	private static File getTempFile() {
		if (isSDCARDMounted()) {
			File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
			try {
				f.createNewFile();
			} catch (IOException e) {
			}
			return f;
		} else {
			return null;
		}
	}

	public static File writeFile(Bitmap bitmap) {
		if (isSDCARDMounted()) {
			try {
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
				File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
				f.createNewFile();
				FileOutputStream fo = new FileOutputStream(f);
				fo.write(bytes.toByteArray());
				fo.close();
				return f;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	private static boolean isSDCARDMounted(){
		String status = Environment.getExternalStorageState();
		if (status.equals(Environment.MEDIA_MOUNTED))
			return true;
		return false;
	}


	public static Bitmap decodeUri(String path) throws FileNotFoundException {

		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path,o);
		//BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = 140;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE
					|| height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeFile(path,o2);
	}


	public static String getRealPathFromURI(Uri contentUri,Context context) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
}
