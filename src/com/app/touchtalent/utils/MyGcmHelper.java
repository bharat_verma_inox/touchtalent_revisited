package com.app.touchtalent.utils;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class MyGcmHelper {
	private static final String TAG = "gcmbharat";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private String SENDER_ID = TtlConst.GCM_SENDER_ID;
	private GoogleCloudMessaging gcm;

	private Context context;
	private String regid;

	public MyGcmHelper(Context context) {
		super();
		this.context = context;
		AppLog.i("gcm id " + MyPref.getGcmRegistrationId());
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				AppLog.i(TAG, "This device is not supported.");

			}
			return false;
		}
		return true;
	}

	public void getGcmRegId() {
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(context);
			regid = getRegistrationId(context);

			if (regid.isEmpty()) {
				registerInBackground();
			}
		} else {
			AppLog.i(TAG, "No valid Google Play Services APK found.");
		}
	}

	private String getRegistrationId(Context context) {
		AppLog.i(TAG, "Registering");
		String registrationId = MyPref.getGcmRegistrationId();
		if (registrationId.isEmpty()) {
			AppLog.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.

		int currentVersion = getAppVersion(context);
		if (MyPref.getAppVersion() != currentVersion) {
			AppLog.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	private void registerInBackground() {
		new AsyncTask<Object, Object, Object>() {

			@Override
			protected void onPostExecute(Object msg) {
				// mDisplay.append(msg + "\n");
			}

			@Override
			protected Object doInBackground(Object... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;

					AppLog.i(TAG, "gcm registered " + regid);
					MyPref.setGcmRegistartionId(regid);
					MyPref.setAppVersion(getAppVersion(context));
					MyPref.setRegistrationStatus(false);

				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
					AppLog.i("exception in gcm : " + msg);

				}

				return null;
			}
		}.execute(null, null, null);

	}

}
