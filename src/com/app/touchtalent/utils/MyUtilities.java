package com.app.touchtalent.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.app.touchtalent.R;
import com.app.touchtalent.database.Feed;
import com.app.touchtalent.service.MyService;
import com.app.touchtalent.volley.VolleySingleton;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.android.gms.plus.PlusShare;
import com.google.gson.Gson;
import com.pinterest.pinit.PinIt;
import com.pinterest.pinit.PinItListener;

public class MyUtilities {
	private static UiLifecycleHelper uiHelper;
	private static Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {

		}
	};

	private static final String LOVED_IT = "Loved It";
	public static String YOU_ROCK = "You Rock";
	public static String AWESOME = "Awesome";
	public static String SALUTE = "Salute";
	public static String HATS_OFF = "Hats Off";
	public static final String FLURRY_KEY = "YBCT8KFF23K7QJ5RNDJ6";
	public static final int CODE_FEED_FETCH = 1101;
	public static final int CODE_MARK_FAVORITE = 1102;
	public static final int CODE_POST_COMMENT = 1103;
	public static final int CODE_ARTWORK = 1104;
	public static final int CODE_USERINFO = 31105;
	public static final int CODE_MAILBOX = 1106;
	public static final int CODE_CONVERSTION = 1107;
	public static final int CODE_SENDMESSAGE = 1108;
	public static final int CODE_FEEDOLD = 1109;
	public static final int CODE_FEEDNEW = 1110;
	public static final int CODE_EXHIBITION = 1111;
	public static final int CODE_CATEGORIES = 4112;
	public static final int CODE_VIEWERS = 5113;
	public static final int CODE_LOGOUT = 6114;
	public static final int CODE_FOLLOW = 7115;
	public static final int CODE_UNFOLLOW = 8116;
	public static final int CODE_SEARCH = 9117;
	public static final String MESSAGE_HIRE = "I would like to hire you for a creative project. Please revert if interested.";

	public static String getUserProfileLink(String u_slug,String u_id)
	{
		return "http://www.touchtalent.com/artist/"+u_id+"/"+u_slug;
	}
	public static void showNetworkToast(Context context) {
		

		Toast toast = new Toast(context);
		View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_toast,
				null);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
		toast.setView(view);
		toast.show();
	}

	public static void shareToTwitter(String shareDescription, String shareURL, String shareImageUL, Context context) {

		ProgressDialog progressDialog = new ProgressDialog(context);

		LinkedInShare d = new LinkedInShare(context, progressDialog, "http://twitter.com/share?text=" + shareDescription
				+ "&url=http://" + shareURL);
		d.show();

		progressDialog.setMessage("Loading...");
		progressDialog.setCancelable(true);
		progressDialog.show();

	}

	public static String getPostUrl(String p_slug, String p_id, String cat_slug, boolean httpRequired) {
		if (httpRequired) {
			return "http://www.touchtalent.com/" + cat_slug + "/art/" + p_slug + "-" + p_id;
		} else {
			return "www.touchtalent.com/" + cat_slug + "/art/" + p_slug + "-" + p_id;
		}
	}

	public static String getImageUrl(String p_slug, String p_id, boolean httpRequired) {
		if (httpRequired) {
			return "http://full.creative.touchtalent.com/" + p_slug + "-" + p_id + ".jpg";
		} else {
			return "full.creative.touchtalent.com/" + p_slug + "-" + p_id + ".jpg";
		}
	}

	public static void shareToLinkedIn(String shareURL, Context context) {

		ProgressDialog progressDialog = new ProgressDialog(context);

		LinkedInShare d = new LinkedInShare(context, progressDialog, "http://www.linkedin.com/shareArticle?mini=true&url=http://"
				+ shareURL);
		d.show();

		progressDialog.setMessage("Loading...");
		progressDialog.setCancelable(true);
		progressDialog.show();
	}

	public static void shareToGooglePlus(String shareTitle, String shareURL, Context context) {

		Intent shareIntent = new PlusShare.Builder(context).setType("text/plain").setText(shareTitle)
				.setContentUrl(Uri.parse("http://" + shareURL)).getIntent();

		((Activity) context).startActivityForResult(shareIntent, 0);
	}

	public static void shareToPinterest(String shareTitle, String shareURL, String shareImageURL, Context context) {

		// Log.d("Method", "Pinterest");

		PinIt pinIt = new PinIt();
		PinIt.setPartnerId(TtlConst.TT_PINTEREST_CLIENT_ID);
		PinIt.setDebugMode(true);

		pinIt.setImageUrl("http://" + shareImageURL);
		pinIt.setUrl("http://" + shareURL);
		pinIt.setDescription(shareTitle);
		pinIt.setListener(new PinItListener() {
			@Override
			public void onStart() {
				super.onStart();
			}

			@Override
			public void onComplete(boolean completed) {
				super.onComplete(completed);
				if (completed) {

				}
			}

			@Override
			public void onException(Exception e) {
				super.onException(e);
				e.printStackTrace();
			}
		});
		// Do the pin
		pinIt.doPinIt(context);

	}

	public static void shareToFacebook(String shareURL, Context context) {
		try {
			uiHelper = new UiLifecycleHelper((Activity) context, callback);
			FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder((Activity) context).setLink("http://" + shareURL)
					.build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		} catch (Exception e) {
			e.printStackTrace();
			AppLog.i("FACEBOOK APP NOT INSTALLED");
		}

	}

	public static String getUserProfileImageUrl(String u_id, String u_slug) {
		return "http://www.touchtalent.com/userpics/" + u_id + ".jpg";

	}

	public static String getExhibitionImageUrl(String ex_id, int size) {
		String sizepostfix = "";
		switch (size) {
		case 1:
			sizepostfix = "_1";
			break;
		case 2:
			sizepostfix = "_2";
			break;
		case 3:
			sizepostfix = "_3";
			break;

		default:
			break;
		}

		return "http://art.exhibition.touchtalent.com/ex-" + ex_id + sizepostfix + ".png";

	}

	public static void showAnimation(Context context, final View view) {
		// image_frame = (Relativeimage_frame)
		// findViewById(R.id.date_toast_root);

		Animation anim2 = null;
		AppLog.i("animation", "view " + view);

		view.setVisibility(View.VISIBLE);
		view.bringToFront();
		anim2 = AnimationUtils.loadAnimation(context, R.anim.notification_animation);
		view.bringToFront();
		anim2.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				AppLog.i("animation", "animation started");

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				view.setVisibility(View.INVISIBLE);
				AppLog.i("animation", "animationended");

			}
		});
		view.startAnimation(anim2);

	}

	public static void showAnimation(Context context, final View view, String text) {

		Animation anim2 = null;
		AppLog.i("animation", "view " + view);

		view.setVisibility(View.VISIBLE);
		view.bringToFront();
		((TextView) view).setText(text);
		anim2 = AnimationUtils.loadAnimation(context, R.anim.notification_animation);
		view.bringToFront();
		anim2.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				AppLog.i("animation", "animation started");

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				view.setVisibility(View.INVISIBLE);
				AppLog.i("animation", "animationended");

			}
		});
		// anim2.setInterpolator(new BounceInterpolator());
		view.startAnimation(anim2);

	}

	public static boolean isInternetConnected(Context context) {
		final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected())
			return true;
		else
			return false;
	}

	public static String getNextFriendToInvite(Context context) {
		MyPref.init(context);
		String friendGson = MyPref.getFriendHastMap();
		Gson gson = new Gson();
		HashMap<Integer, String> frndsBack = gson.fromJson(friendGson, HashMap.class);
		if (frndsBack != null) {
			int lastFriendPostition = MyPref.getLastFriendPostition();
			AppLog.i("friends gson " + lastFriendPostition + " " + frndsBack.size());
			if (lastFriendPostition <= frndsBack.size() - 1) {
				MyPref.setLastFriendPostion(lastFriendPostition + 1);
				return frndsBack.get(lastFriendPostition + "");
			} else {

				return null;
			}
		}
		return null;

	}

	public static void startInviteService(Context context) {
		Calendar cal = Calendar.getInstance();
		Intent intent = new Intent(context, MyService.class);
		PendingIntent pintent = PendingIntent.getService(context, 0, intent, 0);

		AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		// Start every 30 seconds
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1 * 1000 * 60 * 60, pintent);
	}

	private static void shareImage(final Feed feedGson, final Context context) {
		String url = MyUtilities.getImageUrl(feedGson.getPost_slug(), feedGson.getPost_id() + "", true);
		AppLog.i("url " + url);
		VolleySingleton.getInstance(context).getImageLoader().get(url, new ImageListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				// TODO Auto-generated method stub
				String path = saveImage(response.getBitmap(), feedGson.getPost_id() + "");
				Intent i = new Intent(android.content.Intent.ACTION_SEND);
				i.setType("image/*");
				AppLog.i(feedGson.getPost_title() + " " + feedGson.getPost_description());
				i.putExtra(android.content.Intent.EXTRA_SUBJECT, feedGson.getPost_title() + "");
				i.putExtra(android.content.Intent.EXTRA_TEXT, feedGson.getPost_title() + "");
				AppLog.i("path :" + path + " uri " + Uri.fromFile(new File(path)));
				i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(path)));
				context.startActivity(Intent.createChooser(i, "Share via"));

			}
		});

	}

	private static String saveImage(Bitmap finalBitmap, String postid) {

		String root = Environment.getExternalStorageDirectory().toString();
		File myDir = new File(root + "/touchtalent/saved_images");
		myDir.mkdirs();

		String fname = "Image-" + postid + ".jpg";
		File file = new File(myDir, fname);
		if (file.exists())
			file.delete();
		try {
			FileOutputStream out = new FileOutputStream(file);
			finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
			String path = file.getAbsolutePath();
			AppLog.i("absolute path " + path);
			return path;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * private static void ShareText(Feed feedGson, Context context) { Intent i
	 * = new Intent(android.content.Intent.ACTION_SEND);
	 * i.setType("text/plain"); String rawdata = feedGson.getPost_description();
	 * String datatoshare = Html.fromHtml(rawdata).toString();
	 * i.putExtra(android.content.Intent.EXTRA_SUBJECT,
	 * feedGson.getPost_title()); i.putExtra(android.content.Intent.EXTRA_TEXT,
	 * datatoshare); context.startActivity(Intent.createChooser(i,
	 * "Share via")); }
	 */

	public static void shareText(String textToShare, String subject, Context context) {
		Intent i = new Intent(android.content.Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
		i.putExtra(android.content.Intent.EXTRA_TEXT, textToShare);
		context.startActivity(Intent.createChooser(i, "Share via"));

	}

	public static void shareDataOnApps(Feed feedGson, Context context) {
		if (feedGson != null) {
			try {
				String post_mediatype = feedGson.getPost_mediatype();
				int mediatype = Integer.valueOf(post_mediatype);
				if (mediatype == 0) {
					AppLog.i("media type is " + mediatype);
					shareImage(feedGson, context);
				} else if (mediatype == 2) {
					String rawdata = feedGson.getPost_description();
					String datatoshare = Html.fromHtml(rawdata).toString();
					shareText(datatoshare, feedGson.getPost_title(), context);
				}
			} catch (Exception e) {
				Toast.makeText(context, "Oops ! Something went wrong", Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(context, "Sharing Failed !! Please try later", Toast.LENGTH_SHORT).show();

		}

	}
}
