package com.app.touchtalent.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import twitter4j.internal.org.json.JSONArray;
import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.app.touchtalent.database.Category;
import com.app.touchtalent.database.CategoryDao;
import com.app.touchtalent.database.Comments;
import com.app.touchtalent.database.CommentsDao;
import com.app.touchtalent.database.DaoMaster;
import com.app.touchtalent.database.DaoMaster.DevOpenHelper;
import com.app.touchtalent.database.DaoSession;
import com.app.touchtalent.database.Exibition;
import com.app.touchtalent.database.ExibitionDao;
import com.app.touchtalent.database.Feed;
import com.app.touchtalent.database.FeedDao;
import com.app.touchtalent.database.FeedDao.Properties;
import com.app.touchtalent.datacontroller.Constant;
import com.app.touchtalent.http.gson.CommentGson;
import com.app.touchtalent.http.gson.FeedGson;
import com.facebook.SessionDefaultAudience;
import com.sromku.simple.fb.Permissions;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.utils.Logger;

public class DatabaseManager extends Application {

	private static final String APP_ID = TtlConst.TT_FB_APP_ID;
	private static final String APP_NAMESPACE = TtlConst.TT_FB_APP_NAMESPACE;
	private static SQLiteDatabase db;
	public static Context applicationContext;
	private static CategoryDao categoryDao;
	private static DaoMaster daoMaster;
	private static DaoSession daoSession;
	private static CommentsDao commentsDao;
	private static ExibitionDao exibitionDao;
	private static FeedDao feedDao;

	@Override
	public void onCreate() {
		super.onCreate();
		applicationContext = getApplicationContext();
		MyPref.init(applicationContext);
		DevOpenHelper devOpenHelper = new DevOpenHelper(this, "touchtalent-db", null);
		db = devOpenHelper.getWritableDatabase();
		daoMaster = new DaoMaster(db);
		daoSession = daoMaster.newSession();
		categoryDao = daoSession.getCategoryDao();
		commentsDao = daoSession.getCommentsDao();
		feedDao = daoSession.getFeedDao();
		exibitionDao = daoSession.getExibitionDao();
		Logger.DEBUG_WITH_STACKTRACE = true;
		// initialize facebook configuration
		Permissions[] permissions = new Permissions[] { Permissions.BASIC_INFO, Permissions.EMAIL, Permissions.PUBLISH_ACTION,
				Permissions.PUBLISH_STREAM };

		SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder().setAppId(APP_ID)
				.setNamespace(APP_NAMESPACE).setPermissions(permissions).setDefaultAudience(SessionDefaultAudience.FRIENDS)
				.build();

		SimpleFacebook.setConfiguration(configuration);
		if (MyPref.getLastDate() == null) {
			AppLog.i("date null ");
			MyPref.setLastDate(CurrentTimeAndDate.getCurrentDate() + "+" + CurrentTimeAndDate.getCurrentTime());
		}
		AppLog.i("previous date found " + MyPref.getLastDate());
		StringTokenizer st = new StringTokenizer(MyPref.getLastDate(), "+");
		String date = st.nextToken();
		String time = st.nextToken();
		int hourDifference = CurrentTimeAndDate.calculateHours(date, time);
		AppLog.i("hour difference found " + hourDifference);
		if (hourDifference >= 24) {
			AppLog.i("deleting data ");
			feedDao.deleteAll();
			exibitionDao.deleteAll();
			MyPref.resetScoreAndIds();
			MyPref.setLastDate(CurrentTimeAndDate.getCurrentDate() + "+" + CurrentTimeAndDate.getCurrentTime());

		}
	}

	public static List<Category> getCategories() {
		return categoryDao.loadAll();
	}

	public static boolean storeFeeds(List<FeedGson> feedList) {
		boolean dataStatus = false;
		for (FeedGson feedjson : feedList) {
			Feed feed = feedDao.queryBuilder().where(FeedDao.Properties.Post_id.eq(feedjson.p_id)).unique();
			if (feed == null) {
				feed = new Feed();
				if (!dataStatus)
					dataStatus = true;
				feed.setIs_followed(feedjson.if_follow);
				feed.setIs_favourite(feedjson.if_favourite);
				feed.setIs_file(feedjson.isfile);
				feed.setNum_comments(feedjson.num_comments);
				feed.setCategory_id(feedjson.cat_id);
				feed.setPost_description(feedjson.p_description);
				feed.setPost_id(feedjson.p_id);
				feed.setPost_mediatype(feedjson.p_mediatype);
				feed.setPost_slug(feedjson.p_slug);
				feed.setPost_title(feedjson.p_title);
				feed.setPost_views(feedjson.p_views);
				feed.setPost_vlink(feedjson.p_vlink);
				feed.setUser_id(feedjson.u_id);
				feed.setPost_timestamp(feedjson.p_timestamp);
				feed.setUser_fname(feedjson.u_fname);
				feed.setUser_lname(feedjson.u_lname);
				feed.setUser_slug(feedjson.u_slug);

				feed.setCategory_name(feedjson.cat_name);
				feed.setCategory_slug(feedjson.cat_slug);
				feed.setPost_score(feedjson.p_score);

				feedDao.insert(feed);
			} else {
				feed.setIs_followed(feedjson.if_follow);
				feed.setIs_favourite(feedjson.if_favourite);
				feed.setNum_comments(feedjson.num_comments);
				feed.setCategory_id(feedjson.cat_id);
				feed.setIs_file(feedjson.isfile);
				feed.setPost_description(feedjson.p_description);
				feed.setPost_id(feedjson.p_id);
				feed.setPost_mediatype(feedjson.p_mediatype);
				feed.setPost_slug(feedjson.p_slug);
				feed.setPost_title(feedjson.p_title);
				feed.setPost_views(feedjson.p_views);
				feed.setPost_vlink(feedjson.p_vlink);
				feed.setUser_id(feedjson.u_id);

				feed.setPost_timestamp(feedjson.p_timestamp);
				feed.setUser_fname(feedjson.u_fname);
				feed.setUser_lname(feedjson.u_lname);
				feed.setUser_slug(feedjson.u_slug);

				feed.setCategory_name(feedjson.cat_name);
				feed.setCategory_slug(feedjson.cat_slug);
				feed.setPost_score(feedjson.p_score);

				feedDao.update(feed);
			}

		}
		return dataStatus;
	}

	public static List<Feed> getFeeds() {
		return feedDao.loadAll();
	}

	public static void storeCategory(List<com.app.touchtalent.http.gson.Category> categoryList) {
		for (com.app.touchtalent.http.gson.Category categoryjson : categoryList) {
			Category category = categoryDao.queryBuilder().where(CategoryDao.Properties.Cat_id.eq(categoryjson.cat_id)).unique();
			if (category == null) {
				category = new Category();
				category.setCat_id(categoryjson.cat_id);
				category.setCat_name(categoryjson.cat_name);
				category.setCat_slug(categoryjson.cat_slug);
				categoryDao.insert(category);
			}
		}
	}

	public static void storeComments(List<CommentGson> listComments, long postID) {
		if (listComments != null && listComments.size() > 0) {
			for (CommentGson commentGson : listComments) {
				Comments comment = commentsDao.queryBuilder().where(CommentsDao.Properties.Comment_id.eq(commentGson.comment_id))
						.unique();
				if (comment == null) {
					comment = new Comments();
					comment.setComment_id(commentGson.comment_id);
					comment.setComment_smiley(commentGson.comment_smiley);
					comment.setComment_text(commentGson.comment_text);
					comment.setComment_uid(commentGson.comment_uid);
					comment.setPost_id(postID);
					comment.setU_fname(commentGson.u_fname);
					comment.setU_lname(commentGson.u_lname);
					comment.setU_slug(commentGson.u_slug);
					commentsDao.insert(comment);
				} else {
					comment.setComment_smiley(commentGson.comment_smiley);
					comment.setComment_text(commentGson.comment_text);
					comment.setComment_uid(commentGson.comment_uid);
					comment.setPost_id(postID);
					comment.setU_fname(commentGson.u_fname);
					comment.setU_lname(commentGson.u_lname);
					comment.setU_slug(commentGson.u_slug);
					commentsDao.update(comment);
				}

			}

		}

	}

	public static List<Feed> getMostRecentFeeds(int category, int skip) {
		return feedDao.queryBuilder().where(FeedDao.Properties.Category_id.eq(category)).orderAsc(FeedDao.Properties.Post_score)
				.offset(skip).limit(30).list();
	}

	public static String getDisplayedHome(int filter) {
		List<Feed> list = null;
		if (filter == Constant.FILTER_MOST_POPULAR) {
			list = feedDao.queryBuilder().where(Properties.Post_score.ge(MyPref.getPopularLastScore())).list();

		} else if (filter == Constant.FILTER_MOST_RECENT) {
			list = feedDao.queryBuilder().orderDesc(FeedDao.Properties.Post_id).list();
		} else if (filter == Constant.FILTER_MOST_TRANDING) {
			list = feedDao
					.queryBuilder()
					.where(FeedDao.Properties.Post_score.ge(MyPref.getTrendingLastScore()),
							FeedDao.Properties.Post_score.le(MyPref.getTrendingFirstScore()))
					.orderDesc(FeedDao.Properties.Post_id).list();
		}

		if (list != null && list.size() > 0) {
			List<String> idList = new ArrayList<String>();
			for (Feed feed : list) {
				idList.add("" + feed.getPost_id());
			}
			JSONArray jsonArray = new JSONArray(idList);
			return jsonArray.toString();
		}
		return "";
	}

	public static String getDisplayedCategory(int cat_id, int filter) {
		List<Feed> list = null;
		if (filter == Constant.FILTER_MOST_POPULAR) {
			AppLog.i("db-debug", "Db Manger feeds else part 1");
			list = feedDao.queryBuilder()
					.where(Properties.Post_score.ge(MyPref.getPopularLastScore()), Properties.Category_id.eq(cat_id)).list();

		} else if (filter == Constant.FILTER_MOST_RECENT) {
			AppLog.i("db-debug", "Db Manger feeds else part 2");
			list = feedDao.queryBuilder().where(Properties.Category_id.eq(cat_id)).orderDesc(FeedDao.Properties.Post_id).list();
		} else if (filter == Constant.FILTER_MOST_TRANDING) {
			AppLog.i("db-debug", "Db Manger feeds else part 3");
			list = feedDao
					.queryBuilder()
					.where(FeedDao.Properties.Post_score.ge(MyPref.getTrendingLastScore()),
							FeedDao.Properties.Post_score.le(MyPref.getTrendingFirstScore()), Properties.Category_id.eq(cat_id))
					.list();
		}

		if (list != null && list.size() > 0) {
			List<String> idList = new ArrayList<String>();
			for (Feed feed : list) {
				idList.add("" + feed.getPost_id());
			}
			JSONArray jsonArray = new JSONArray(idList);
			return jsonArray.toString();
		}
		return "";
	}

	public static List<Feed> getHomeFeeds(int filter, int skip) {
		// if this method returns null... it means pref. are not saved ;
		AppLog.i("homefeeds getting filter " + filter + " skip " + skip);
		try {
			List<Feed> homeFeedList = null;
			if (skip > 0) {
				AppLog.i("skip value not zero ");
				if (filter == Constant.FILTER_MOST_POPULAR && MyPref.getPopularLastScore() != MyPref.INVALIDATED_FLOAT) {
					homeFeedList = feedDao.queryBuilder().where(Properties.Post_score.ge(MyPref.getPopularLastScore()))
							.orderDesc(FeedDao.Properties.Post_score).offset(skip).limit(30).list();

				} else if (filter == Constant.FILTER_MOST_RECENT && MyPref.getRecentPostId() != MyPref.INVALIDATE_PID) {
					homeFeedList = feedDao.queryBuilder().where(Properties.Post_id.ge(MyPref.getRecentPostId()))
							.orderDesc(FeedDao.Properties.Post_id).offset(skip).limit(30).list();

				} else if (filter == Constant.FILTER_MOST_TRANDING && MyPref.INVALIDATED_FLOAT != MyPref.getTrendingLastScore()) {
					List<Feed> tempList = feedDao
							.queryBuilder()
							.where(Properties.Post_score.ge(MyPref.getPopularLastScore()),
									Properties.Post_score.le(MyPref.getTrendingFirstScore()))
							.orderDesc(FeedDao.Properties.Post_score).list();
					AppLog.i("db-debug", "temp " + tempList.size() + "");

					homeFeedList = feedDao
							.queryBuilder()
							.where(FeedDao.Properties.Post_score.le(MyPref.getTrendingLastScore()),
									FeedDao.Properties.Post_score.ge(MyPref.getTrendingFirstScore()))
							.orderDesc(FeedDao.Properties.Post_id).offset(skip).limit(30).list();

				}
				if (homeFeedList == null) {

					homeFeedList = new ArrayList<Feed>();
				}
				return homeFeedList;
			} else {
				AppLog.i("getting  else part ");
				if (filter == Constant.FILTER_MOST_POPULAR) {
					homeFeedList = feedDao.queryBuilder().where(Properties.Post_score.ge(MyPref.getPopularLastScore()))
							.offset(skip).limit(30).list();

				} else if (filter == Constant.FILTER_MOST_RECENT) {
					AppLog.i("db-debug", "Db Manger feeds else part 2 ");
					homeFeedList = feedDao.queryBuilder().orderDesc(FeedDao.Properties.Post_id).offset(skip).limit(30).list();
				} else if (filter == Constant.FILTER_MOST_TRANDING) {
					AppLog.i("db-debug", "Db Manger feeds else part 3 ");
					homeFeedList = feedDao
							.queryBuilder()
							.where(Properties.Post_score.ge(MyPref.getPopularLastScore()),
									Properties.Post_score.le(MyPref.getTrendingFirstScore()))
							.orderDesc(FeedDao.Properties.Post_id).offset(skip).limit(30).list();
				}
				if (homeFeedList.size() == 0) {
					homeFeedList = null;
				}
				return homeFeedList;

			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static boolean storeExibition(List<FeedGson> feedList, long exibition_id) {

		boolean status = false;
		for (FeedGson feedjson : feedList) {
			Exibition feed = exibitionDao.queryBuilder().where(ExibitionDao.Properties.Post_id.eq(Long.valueOf(feedjson.p_id)))
					.unique();
			if (feed == null) {
				feed = new Exibition();
				feed.setExibition_id(exibition_id);
				feed.setIs_followed(feedjson.if_follow);
				feed.setIs_favourite(feedjson.if_favourite);
				feed.setIs_file(feedjson.isfile);
				feed.setNum_comments(feedjson.num_comments);
				feed.setCategory_id(feedjson.cat_id);
				feed.setPost_description(feedjson.p_description);
				feed.setPost_id(feedjson.p_id);
				feed.setPost_mediatype(feedjson.p_mediatype);
				feed.setPost_slug(feedjson.p_slug);
				feed.setPost_title(feedjson.p_title);
				feed.setPost_views(feedjson.p_views);
				feed.setPost_vlink(feedjson.p_vlink);
				feed.setUser_id(feedjson.u_id);
				feed.setPost_timestamp(feedjson.p_timestamp);
				feed.setUser_fname(feedjson.u_fname);
				feed.setUser_lname(feedjson.u_lname);
				feed.setUser_slug(feedjson.u_slug);
				feed.setCategory_name(feedjson.cat_name);
				feed.setCategory_slug(feedjson.cat_slug);
				feed.setPost_score(feedjson.p_score);
				exibitionDao.insert(feed);
			}

			else {
				feed.setExibition_id(exibition_id);
				feed.setIs_followed(feedjson.if_follow);
				feed.setIs_favourite(feedjson.if_favourite);
				feed.setNum_comments(feedjson.num_comments);
				feed.setCategory_id(feedjson.cat_id);
				feed.setPost_description(feedjson.p_description);
				feed.setPost_id(feedjson.p_id);
				feed.setIs_file(feedjson.isfile);
				feed.setPost_mediatype(feedjson.p_mediatype);
				feed.setPost_slug(feedjson.p_slug);
				feed.setPost_title(feedjson.p_title);
				feed.setPost_views(feedjson.p_views);
				feed.setPost_vlink(feedjson.p_vlink);
				feed.setUser_id(feedjson.u_id);

				feed.setPost_timestamp(feedjson.p_timestamp);
				feed.setUser_fname(feedjson.u_fname);
				feed.setUser_lname(feedjson.u_lname);
				feed.setUser_slug(feedjson.u_slug);

				feed.setCategory_name(feedjson.cat_name);
				feed.setCategory_slug(feedjson.cat_slug);
				feed.setPost_score(feedjson.p_score);
				// exibitionDao.(feed);
			}
			// DatabaseManager.storeComments(feedjson.comments,
			// feedjson.cat_id);
		}
		return status;
	}

	public static List<Exibition> getExibition(long exibition_id) {
		return exibitionDao.queryBuilder().where(ExibitionDao.Properties.Exibition_id.eq(exibition_id)).list();
	}

	public static String getLastPostIdEXibition(long exibitionId) {
		List<Exibition> query = exibitionDao.queryBuilder().where(ExibitionDao.Properties.Exibition_id.eq(exibitionId)).list();
		if (query != null && query.size() > 0) {
			Exibition feed = query.get(query.size() - 1);
			return "" + feed.getPost_id();
		}
		return null;
	}

	public static String getLastScoreExibition(long userID) {
		List<Exibition> query = exibitionDao.queryBuilder().where(ExibitionDao.Properties.Exibition_id.eq(userID)).list();
		if (query != null && query.size() > 0) {
			Exibition feed = query.get(query.size() - 1);
			return "" + feed.getPost_score();
		}
		return null;
	}

	public static String getDisplayedExibition(long exibitionId) {
		List<Exibition> list = exibitionDao.queryBuilder().where(ExibitionDao.Properties.Exibition_id.eq(exibitionId)).list();
		if (list != null && list.size() > 0) {
			List<String> idList = new ArrayList<String>();
			for (Exibition feed : list) {
				idList.add("" + feed.getPost_id());
			}
			JSONArray jsonArray = new JSONArray(idList);
			return jsonArray.toString();
		}
		return null;
	}

	public static void updatefavourate(long post_id) {
		AppLog.i("post id recieved " + post_id);
		Feed feed = feedDao.queryBuilder().where(FeedDao.Properties.Post_id.eq(post_id)).unique();
		if (feed != null) {
			feed.setIs_favourite("1");
			feedDao.insertOrReplace(feed);
		}
	}

	public static List<Feed> getCategoryFeeds(int cat_id, int filter, int skip) {
		// if this method returns null... it means pref. are not saved ;
		try {
			List<Feed> categoryFeeds = null;
			if (skip > 0) {
				if (filter == Constant.FILTER_MOST_POPULAR && MyPref.getPopularLastScore() != MyPref.INVALIDATED_FLOAT) {
					categoryFeeds = feedDao.queryBuilder()
							.where(Properties.Post_score.ge(MyPref.getPopularLastScore()), Properties.Category_id.eq(cat_id))
							.orderDesc(FeedDao.Properties.Post_score).offset(skip).limit(30).list();
					if (categoryFeeds == null) {
						categoryFeeds = new ArrayList<Feed>();
					}
				} else if (filter == Constant.FILTER_MOST_RECENT && MyPref.getRecentPostId() != MyPref.INVALIDATE_PID) {
					categoryFeeds = feedDao.queryBuilder()
							.where(Properties.Post_id.ge(MyPref.getRecentPostId()), Properties.Category_id.eq(cat_id))
							.orderDesc(FeedDao.Properties.Post_id).offset(skip).limit(30).list();
					if (categoryFeeds == null) {
						categoryFeeds = new ArrayList<Feed>();
					}
				} else if (filter == Constant.FILTER_MOST_TRANDING && MyPref.INVALIDATED_FLOAT != MyPref.getTrendingLastScore()) {
					// List<Feed> tempList = feedDao
					// .queryBuilder()
					// .where(FeedDao.Properties.Post_score.ge(MyPref.getTrendingLastScore()),
					// FeedDao.Properties.Post_score.le(MyPref.getTrendingFirstScore()),
					// Properties.Category_id.eq(cat_id)).orderDesc(FeedDao.Properties.Post_score).list();

					categoryFeeds = feedDao
							.queryBuilder()
							.where(FeedDao.Properties.Post_score.ge(MyPref.getTrendingLastScore()),
									FeedDao.Properties.Post_score.le(MyPref.getTrendingFirstScore()),
									Properties.Category_id.eq(cat_id)).orderDesc(FeedDao.Properties.Post_id).offset(skip)
							.limit(30).list();

					if (categoryFeeds == null) {

						categoryFeeds = new ArrayList<Feed>();
					}
				}
				if (categoryFeeds != null) {
				}
				return categoryFeeds;
			} else {
				if (filter == Constant.FILTER_MOST_POPULAR) {
					categoryFeeds = feedDao.queryBuilder()
							.where(Properties.Post_score.ge(MyPref.getPopularLastScore()), Properties.Category_id.eq(cat_id))
							.offset(skip).limit(30).list();

				} else if (filter == Constant.FILTER_MOST_RECENT) {
					categoryFeeds = feedDao.queryBuilder().where(Properties.Category_id.eq(cat_id))
							.orderDesc(FeedDao.Properties.Post_id).offset(skip).limit(30).list();
				} else if (filter == Constant.FILTER_MOST_TRANDING) {
					categoryFeeds = feedDao
							.queryBuilder()
							.where(FeedDao.Properties.Post_score.ge(MyPref.getTrendingLastScore()),
									FeedDao.Properties.Post_score.le(MyPref.getTrendingFirstScore()),
									Properties.Category_id.eq(cat_id)).orderDesc(FeedDao.Properties.Post_id).offset(skip)
							.limit(30).list();
				}
				if (categoryFeeds.size() == 0) {
					categoryFeeds = null;
				}
				return categoryFeeds;
			}
		} catch (Exception e) {
			e.printStackTrace();
			AppLog.i("db-debug", " Exception DbMAnger GetHomeFeeds" + e);
			return null;
		}

	}

}
