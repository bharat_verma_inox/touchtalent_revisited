package com.app.touchtalent.utils;

public class AppLog {
	public final static String TAG = "touch_revisited";

	public final static boolean LOG_ENABLED = true;

	public static void i(String message) {
		if (LOG_ENABLED)
			android.util.Log.i(TAG, "" + message);
	}

	public static void i(String tag, String message) {
		if (LOG_ENABLED)
			android.util.Log.i(tag, "" + message);
	}

	public static void e(String string, String string2) {
		// TODO Auto-generated method stub
		if (LOG_ENABLED)
			android.util.Log.e(string, "" + string2);

	}
}
