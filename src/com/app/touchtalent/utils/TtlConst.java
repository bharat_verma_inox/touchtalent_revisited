package com.app.touchtalent.utils;

public class TtlConst {

	// My account
	public final static int PICK_FROM_CAM = 7887;
	public final static int PICK_FROM_GALLARY = 758098;
	public final static int CROP_FROM_CAM = 7867;
	public final static int FILTER_IMAGE = 988;

	public final static String GOOGLE_CLIENT_ID = "858559313929-hc9p1fmnon51k878n2rqb96lu76dgglg.apps.googleusercontent.com"; // "165559845302-0ed6vtqq0j6sofa6hmput1bq5gpbs71l.apps.googleusercontent.com";

	// STAGE
	// public static final String MENU_URL = "http://m.touchtalent.biz";
	// public static final String API_URL = "http://www.touchtalent.biz/api/v1";

	// PRODUCTION
	public static final String MENU_URL = "http://m.touchtalent.com";
	public static final String API_URL = "http://www.touchtalent.com/api/v1";

	public static final String device_id = "0";

	public static final String TT_PINTEREST_CLIENT_ID = "1434782";
	public static final String TT_FB_APP_ID = "316237385059852";
	public static final String TT_FB_APP_NAMESPACE = "touchtalent";
	public static final String GCM_SENDER_ID = "858559313929";// "165559845302";

	public static final String URL_FORGOT_PSED = MENU_URL + "/#/forgotpassword";
	public static final String URL_TERMS_CONDITIONS = "http:www.google.com";

	public static final int REQUEST_CODE_PLUS_CLIENT_FRAGMENT = 0;

	public static final int RESULT_SUCCESS = 21;
	public static final int RESULT_LOGOUT = 21221;
	public static final int REQUEST_CODE = 213;

	public static final String KEY_DATA_CATOGORY = "CATOGORY";
	public static final String KEY_UPLOAD_TYPE = "UPLOAD_TYPE";
	public static final String KEY_UPLOAD_DATA = "UPLOAD_DATA";
	public static final String KEY_URL = "UPLOAD_DATA";
	public static final String APP_NAME = "Touchtalent";

	public static final int UPLOAD_TYPE_IMAGE = 111;
	public static final int UPLOAD_TYPE_CAM = 132;
	public static final int UPLOAD_TYPE_TEXT = 112;
	public static final int UPLOAD_TYPE_VIDEO = 113;

	// URls hosts
	public static final String HOME_URL = MENU_URL + "/#/feed";
	public static final String FEEDBACK = MENU_URL + "/#/feedback";
	public static final String MENU_CAT_URL = MENU_URL + "/#/category/{0}";
	public static final String SEARCH = MENU_URL + "/#/search?name={0}";
	public static final String MAILBOX = MENU_URL + "/#/mailbox";
	public static final String NOTIFICATIONS = MENU_URL + "/#/notifications";
	public static final String ACTIVITY = MENU_URL + "/#/activitytimeline";
	public static final String SETTINGS = MENU_URL + "/#/settings";
	public static final String ME = MENU_URL + "/#/me";
	public static final String ARTGALLARY = MENU_URL + "/#/artgallery";
	public static final String AUCTIONHOUSE = MENU_URL + "/#/auctionhouse";
	public static final String INVITE_FRIENDS = MENU_URL + "/#/feed";
	public static final String EXIBITIONS = MENU_URL + "/#/exhibitions";
	public static final String GOPRO = MENU_URL + "/#/upgrade";

	public static final String UPLOAD_REDIRECT = MENU_URL + "/#/post/{0}";

	public static final String BROADCAST = "com.app.touchtalent.local.service.updater";
	public static final String PACKAGE_NAME = "com.app.touchtalent";

}
