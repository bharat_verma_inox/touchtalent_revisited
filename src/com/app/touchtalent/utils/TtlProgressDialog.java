package com.app.touchtalent.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class TtlProgressDialog extends ProgressDialog {

	public TtlProgressDialog(Context context) {
		super(context);
		this.setCancelable(false);
		this.setMessage("please wait...");
	}

	public void updateTextandshow(int resId) {
		this.show();
	}

	public void updateText(int resId) {
		this.setMessage(getContext().getResources().getString(resId));
	}
}
